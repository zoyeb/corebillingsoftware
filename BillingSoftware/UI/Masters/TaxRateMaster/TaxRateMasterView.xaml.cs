﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BillingSoftware.UI.Masters.TaxRateMaster
{
    /// <summary>
    /// Interaction logic for TaxRateMasterView.xaml
    /// </summary>
    public partial class TaxRateMasterView : Window
    {
        public TaxRateMasterView()
        {
            InitializeComponent();
        }
    }
}
