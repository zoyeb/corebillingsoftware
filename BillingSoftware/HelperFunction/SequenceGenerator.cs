﻿using BillingSoftware.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BillingSoftware.HelperFunction
{
    public static class SequenceGenerator
    {

        public static int? LastGenratedNo { get; set; }

        public static string MakeIntoSequence(this int i, int totalLength, string prefix)
        {
            string output = i.ToString();
            int lengthMinusPrefix = totalLength - prefix.Length;
            while (output.Length < lengthMinusPrefix)
                output = "0" + output;
            return prefix + output;
        }
        public static int Seq(this int lastGenratedNo)
        {
            return Interlocked.Increment(ref lastGenratedNo);
        }

        public static Sequence GetLastGenratedSeq(string prefix, int SeqId, int SeqHeaderId)
        {
            try
            {
                using (var db = new EntityModel.InvoiceEntities())
                {
                    return db.Sequences.FirstOrDefault(x => x.SeqId == SeqId && x.SeqHeaderId == SeqHeaderId && x.prefix == prefix);
                }
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }

            return new Sequence();
        }

        public static string GetSequence(int lastGenNo, int totalLength, string preFix, int SeqId, int SeqHeaderId)
        {
            var sequence = GetLastGenratedSeq(preFix, SeqId, SeqHeaderId);
            return Seq(lastGenNo).MakeIntoSequence(totalLength, preFix);
        }

        public static string GenerateSequence(int SeqId, int SeqHeaderId, string SeqFormName, int totalLength, string prefix, out Sequence Sequence)
        {
            var sequence = GetLastGenratedSeq(prefix, SeqId, SeqHeaderId);            
            if (sequence != null)
            {
                string seq = Seq(sequence.LastGeneratedNo.Value).MakeIntoSequence(totalLength, prefix);
                if (!string.IsNullOrEmpty(seq))
                {
                    sequence.LastGeneratedNo++;
                    Sequence = sequence;                     
                    return seq;
                }
            }
            Sequence = null;
            return string.Empty;
        }
    }
}
