﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingSoftware.Manager
{
    public class FluentUnitOfWork : IDisposable
    {
        private DbContext Context { get; }
        private DbContextTransaction Transaction { get; set; }
        public FluentUnitOfWork(DbContext context)
        {
            Context = context;
        }

        public FluentUnitOfWork BeginTransaction()
        {
            Transaction = Context.Database.BeginTransaction();
            return this;
        }

        public FluentUnitOfWork DoInsert<TEntity>(TEntity entity) where TEntity : class
        {
            if (entity != null)
                Context.Set<TEntity>().Add(entity);
            return this;
        }

        public FluentUnitOfWork DoInsert<TEntity>(TEntity entity, out TEntity inserted) where TEntity : class
        {
            if (entity != null)
            {
                inserted = Context.Set<TEntity>().Add(entity);
                return this;
            }
            else
            {
                inserted = null;
                return this;
            }
        }
        public FluentUnitOfWork DoInsertColl<TEntity>(List<TEntity> entity, out List<TEntity> inserted) where TEntity : class
        {
            List<TEntity> temp = new List<TEntity>();
            if (entity.Count != 0)
            {
                temp = Context.Set<TEntity>().AddRange(entity).ToList();
            }
            inserted = temp;
            return this;
        }
        public FluentUnitOfWork DoUpdate<TEntity>(TEntity entity) where TEntity : class
        {
            if (entity != null)
            {
                Context.Entry(entity).State = EntityState.Detached;
                Context.Entry(entity).State = EntityState.Modified;
            }
            return this;
        }
        public FluentUnitOfWork DoUpdate<TEntity>(TEntity entity,out TEntity outEntity) where TEntity : class
        {
            if (entity != null)
            {
                Context.Entry(entity).State = EntityState.Detached;
                Context.Entry(entity).State = EntityState.Modified;
                outEntity = entity;
            }
            outEntity = entity;
            return this;
        }
        public FluentUnitOfWork DoInsertColl<TEntity>(List<TEntity> entity) where TEntity : class
        {
            if (entity.Count != 0)
            {
                Context.Set<TEntity>().AddRange(entity);
            }
            return this;
        }
        public FluentUnitOfWork DoUpdateColl<TEntity>(List<TEntity> entity) where TEntity : class
        {
            if (entity.Count != 0)
            {
                foreach (var p in entity)
                {
                    Context.Set<TEntity>().Attach(p);
                    Context.Entry(p).State = EntityState.Modified;
                }
            }
            return this;
        }

        public FluentUnitOfWork DoDeleteColl<TEntity>(List<TEntity> entity) where TEntity : class
        {
            if (entity.Count != 0)
            {               
                foreach (var p in entity)
                {                    
                    Context.Entry(p).State = EntityState.Deleted;
                }
            }
            return this;
        }

        public FluentUnitOfWork Delete<TEntity>(List<TEntity> entity) where TEntity : class
        {
            if (entity.Count != 0)
            {
                foreach (var p in entity)
                {
                    Context.Entry(p).State = EntityState.Deleted;                     
                }
            }
            return this;
        }
        public FluentUnitOfWork SaveAndContinue()
        {
            try
            {
                Context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {                
                List<string> errorMessages = new List<string>();
                foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                {
                    string entityName = validationResult.Entry.Entity.GetType().Name;
                    foreach (DbValidationError error in validationResult.ValidationErrors)
                    {
                        errorMessages.Add(entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                    }
                }
            }
            return this;
        }

        public bool EndTransaction()
        {
            try
            {
                var a = Context.SaveChanges();
                Transaction.Commit();
            }
            catch (DbEntityValidationException dbEx)
            {                
                List<string> errorMessages = new List<string>();
                foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                {
                    string entityName = validationResult.Entry.Entity.GetType().Name;
                    foreach (DbValidationError error in validationResult.ValidationErrors)
                    {
                        errorMessages.Add(entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                    }
                }
            }
            return true;
        }
        public void RollBack()
        {
            Transaction.Rollback();
            Dispose();
        }

        public void Dispose()
        {
            Transaction?.Dispose();
            Context?.Dispose();
        }
    }
}
