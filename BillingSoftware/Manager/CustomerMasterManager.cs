﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BillingSoftware.EntityModel;
using BillingSoftware.Model;
using ResourceFiles.Properties;

namespace BillingSoftware.Manager
{
    public class CustomerMasterManager
    {
        public static CustomerModel GetCustomerByCustomerId(int customerId)
        {
            var cust = new CustomerModel();
            try
            {
                using (var db = new InvoiceEntities())
                {
                    Customer objCustomer = db.Customers.FirstOrDefault(x => x.CompanyId == Global.Instance.CompanyId && x.CustId == customerId && x.Status == 1);

                    cust.CustId = objCustomer.CustId;
                    cust.CityId = objCustomer.CityId;
                    cust.CompanyId = objCustomer.CompanyId;
                    cust.StateId = objCustomer.StateId;
                    cust.CountryId = objCustomer.CountryId;                   
                    cust.CreatedBy = objCustomer.CreatedBy;
                    cust.PostalCode = objCustomer.PostalCode;
                    cust.Address = objCustomer.Address;
                    cust.OtherDetail = objCustomer.OtherDetail;
                    cust.CreatedDate = objCustomer.CreatedDate;
                    cust.CustCode = objCustomer.CustCode;
                    cust.CustFName = objCustomer.CustFName;
                    cust.CustLName = objCustomer.CustLName;
                    cust.CustMName = objCustomer.CustMName;
                    cust.EmailAddress = objCustomer.EmailAddress;
                    cust.Gender = objCustomer.Gender;
                    cust.MobileNo = objCustomer.MobileNo;
                    cust.ModifiedDate = objCustomer.ModifiedDate;
                    cust.ModifiedBy = objCustomer.ModifiedBy;
                    cust.Status = objCustomer.Status;

                    return cust;
                }
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }
            return cust;
        }

        public static ObservableCollection<Customer> GetCustomerCollection()
        {
            try
            {
                var db = new InvoiceEntities();
                List<Customer> cust = db.Customers.Where(x => x.CompanyId == Global.Instance.CompanyId && x.Status == 1 && x.CustId > 0).ToList();
                return new ObservableCollection<Customer>(cust);

            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }
            return new ObservableCollection<Customer>();

        }

        internal static bool DeleteRecord(Customer obj)
        {
            //throw new NotImplementedException();
            return true;
        }

        public static ObservableCollection<Sequence> GetSequence(string masterModule, string customerSeqForm)
        {
            try
            {
                using (var db = new InvoiceEntities())
                {
                    var seqHdr = db.SequenceHeaders.Where(x => x.ModuleName.Equals(masterModule, StringComparison.OrdinalIgnoreCase) && x.SeqFormName.Equals(customerSeqForm, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    var cust = new ObservableCollection<Sequence>();
                    if(seqHdr!=null)
                    cust.Add(seqHdr.Sequences.FirstOrDefault(x => x.IsActive == true));
                    return cust;
                }
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }
            return new ObservableCollection<Sequence>();


        }

        public static bool SaveData(CustomerModel objCustomer, Sequence seq, out Customer customer, ref StringBuilder message)
        {
            if (objCustomer != null)
            {
                var unitOfWork = new FluentUnitOfWork(new InvoiceEntities());
                var cust = new EntityModel.Customer();


                cust.CustId = objCustomer.CustId;
                cust.CityId = objCustomer.CityId;
                cust.CompanyId = objCustomer.CompanyId;
                cust.StateId = objCustomer.StateId;
                cust.CountryId = objCustomer.CountryId;
                cust.CreatedBy = objCustomer.CreatedBy;
                cust.PostalCode = objCustomer.PostalCode;
                cust.Address = objCustomer.Address;
                cust.OtherDetail = objCustomer.OtherDetail;
                cust.CreatedDate = objCustomer.CreatedDate;
                cust.CustCode = objCustomer.CustCode;
                cust.CustFName = objCustomer.CustFName;
                cust.CustLName = objCustomer.CustLName;
                cust.CustMName = objCustomer.CustMName;
                cust.EmailAddress = objCustomer.EmailAddress;
                cust.Gender = objCustomer.Gender;
                cust.MobileNo = objCustomer.MobileNo;
                cust.ModifiedDate = DateTime.Now;
                cust.ModifiedBy = Global.Instance.LoginUserName;
                cust.Status = objCustomer.Status;

                if (objCustomer.CustId > 0)
                {
                    cust.CustId = objCustomer.CustId;

                    try
                    {
                        bool status = unitOfWork.
                            BeginTransaction()
                            .DoUpdate(cust)
                            .EndTransaction();
                        if (status)
                        {
                            customer = cust;
                            message.Append(MasterMessages.Updated.ToString());
                            return true;
                        }
                        else
                        {
                            customer = null;
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        unitOfWork.RollBack();
                        new ErrorLogger.ErrLogger(ex);
                    }
                }
                else
                {
                    cust.CustId = 0;
                    Sequence updtSequence = new Sequence();
                    cust.CustCode = HelperFunction.SequenceGenerator.GenerateSequence(seq.SeqId, seq.SequenceHeader.SeqHeaderId, Global.CustomerSeqForm, (int)seq.TotalLength, seq.prefix, out updtSequence);

                    try
                    {
                        bool status = unitOfWork.
                            BeginTransaction()
                            .DoInsert(cust, out cust)
                            .DoUpdate(updtSequence)
                            .EndTransaction();
                        if (status)
                        {

                            customer = cust;
                            message.Append(MasterMessages.Inserted.ToString());
                            return true;

                        }
                        else
                        {
                            customer = null;
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        unitOfWork.RollBack();
                        new ErrorLogger.ErrLogger(ex);
                    }
                }
            }
            customer = null;
            return false;
        }
    }
}
