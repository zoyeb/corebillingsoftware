﻿using BillingSoftware.EntityModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BillingSoftware.Model;
using ResourceFiles.Properties;

namespace BillingSoftware.Manager
{
    public class SequenceManager
    {
        public static ObservableCollection<SequenceHeader> GetSeqHeader()
        {
            try
            {
                var db = new InvoiceEntities();
                var seqHeader = db.SequenceHeaders.Where(x => x.SeqHeaderId > 0 && x.Status == 1 && x.CompanyId == Global.Instance.CompanyId).ToList();
                return new ObservableCollection<SequenceHeader>(seqHeader);

            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }
            return new ObservableCollection<SequenceHeader>();
        }

        public static SequenceHeader getSeqHeaderById(int seqHeaderId)
        {
            try
            {
                var db = new InvoiceEntities();
                return db.SequenceHeaders.FirstOrDefault(x =>                 
                x.SeqHeaderId == seqHeaderId && x.Status == 1 && x.CompanyId == Global.Instance.CompanyId);
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }

            return new SequenceHeader();
        }

        public static bool SaveData(ObservableCollection<Sequence> sequenceColl, out Sequence sequenceHdr, ref StringBuilder message)
        {
            List<Sequence> addSequence = new List<Sequence>();
            List<Sequence> updateSequence = new List<Sequence>();
            try
            {
                if (sequenceColl != null)
                {                   
                    foreach (Sequence _obj in sequenceColl)
                    {
                        var objSeq = new EntityModel.Sequence();

                        objSeq.SeqId = _obj.SeqId;
                        objSeq.prefix = _obj.prefix;
                        objSeq.IsActive = _obj.IsActive;
                        objSeq.CompanyId = _obj.CompanyId;
                        objSeq.TotalLength = _obj.TotalLength;
                        objSeq.LastGeneratedNo = _obj.LastGeneratedNo ?? 0;
                        objSeq.SeqHeaderId = _obj.SeqHeaderId;
                        objSeq.StartRangeNo = _obj.StartRangeNo;
                        objSeq.Status = _obj.Status;
                        objSeq.StartDate = _obj.StartDate;
                        objSeq.EndDate = _obj.EndDate;
                        objSeq.CreatedDate = _obj.CreatedDate;
                        objSeq.CreatedBy = _obj.CreatedBy;
                        objSeq.ModifiedBy = Global.Instance.LoginUserName;
                        objSeq.ModifiedDate = DateTime.Now;

                        if (objSeq.SeqId > 0)
                        {
                            updateSequence.Add(objSeq);
                            objSeq.Status = _obj.Status;
                        }
                        else
                        {
                            addSequence.Add(objSeq);
                        }

                    }

                    #region DbTransaction
                    var unitOfWork = new FluentUnitOfWork(new InvoiceEntities());
                    try
                    {
                        bool status = unitOfWork.
                            BeginTransaction()
                         .DoUpdateColl(updateSequence)
                         .DoInsertColl(addSequence, out addSequence)
                            .EndTransaction();
                        if (status)
                        {
                            updateSequence.AddRange(addSequence);
                            sequenceHdr = updateSequence.FirstOrDefault();
                            message.Append(MasterMessages.Inserted.ToString());
                            return true;
                        }
                        else
                        {
                            updateSequence.AddRange(addSequence);
                            sequenceHdr = updateSequence.FirstOrDefault();
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        unitOfWork.RollBack();
                        new ErrorLogger.ErrLogger(ex);
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {

                new ErrorLogger.ErrLogger(ex);
            }
            sequenceHdr = addSequence.FirstOrDefault();
            return false;
        }
    }
}
