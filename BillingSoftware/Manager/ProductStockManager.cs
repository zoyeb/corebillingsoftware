﻿using BillingSoftware.EntityModel;
using BillingSoftware.Model;
using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingSoftware.Manager
{
    public class ProductStockManager
    {
        public static ObservableCollection<ProductStock> GetProductStockCollection()
        {
            var data = new ObservableCollection<ProductStock>();
            try
            {
                var db = new InvoiceEntities();
                data = new ObservableCollection<ProductStock>(db.ProductStocks.Where(x => x.Status == 1 && x.CompanyId == Global.Instance.CompanyId).ToList());
                return data;
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }
            return data;
        }

        internal static bool SaveData(ProductStockModel objStock, ref StringBuilder message, out ProductStock tempCity)
        {
            if (objStock != null)
            {
                ProductStock productStk = new ProductStock();

                #region Update case
                if (objStock.ProductStockId > 0)
                {
                    productStk.ProductStockId = objStock.ProductStockId;
                    productStk.ProductId = objStock.ProductId;
                    productStk.StockQty = objStock.StockQty;
                    productStk.IssuedQty = 0;
                    productStk.BalanceQty = objStock.StockQty;
                    productStk.StockDate = objStock.StockDate;
                    productStk.CreatedBy = objStock.CreatedBy;
                    productStk.CreatedDate = objStock.CreatedDate;
                    productStk.ModifiedBy = Global.Instance.LoginUserName;
                    productStk.ModifiedDate = DateTime.Now;
                    productStk.Status = objStock.Status;
                    productStk.CompanyId = objStock.CompanyId;

                    #region DbTransaction
                    var uof = new FluentUnitOfWork(new InvoiceEntities());
                    try
                    {
                        bool Status =
                             uof.BeginTransaction()
                            .DoUpdate(productStk)
                            .SaveAndContinue()
                            .EndTransaction();
                        if (Status)
                        {
                            tempCity = productStk;
                            message.Append(MasterMessages.Updated);
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        uof.RollBack();
                        new ErrorLogger.ErrLogger(ex);
                        message.Append(GeneralMessages.ErrorLog);
                    }

                    #endregion
                }
                #endregion

                #region insert case
                else
                {
                    productStk.ProductStockId = 0;
                    productStk.ProductId = objStock.ProductId;
                    productStk.StockQty = objStock.StockQty;

                    if (objStock.StockDate.HasValue)
                        productStk.StockDate = objStock.StockDate;
                    else
                        productStk.StockDate = DateTime.Now;

                    productStk.IssuedQty = 0;
                    productStk.BalanceQty = objStock.StockQty;
                    productStk.CreatedBy = objStock.CreatedBy;
                    productStk.CreatedDate = objStock.CreatedDate;
                    productStk.ModifiedBy = Global.Instance.LoginUserName;
                    productStk.ModifiedDate = DateTime.Now;
                    productStk.Status = objStock.Status;
                    productStk.CompanyId = objStock.CompanyId;


                    #region DbTransaction

                    var uof = new FluentUnitOfWork(new InvoiceEntities());
                    try
                    {

                        bool Status = uof.BeginTransaction()
                            .DoInsert(productStk, out productStk)
                            .SaveAndContinue()
                            .EndTransaction();

                        if (Status)
                        {
                            tempCity = productStk;
                            message.Append(MasterMessages.Inserted);
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        uof.RollBack();
                        new ErrorLogger.ErrLogger(ex);
                        message.Append(GeneralMessages.ErrorLog);
                    }
                    #endregion
                }
                #endregion
            }
            tempCity = null;
            return false;
        }

        internal static ProductStockModel GetProductStockById(int productStockId)
        {
            var productStk = new ProductStockModel();
            try
            {
                var db = new InvoiceEntities();

                var productStock = db.ProductStocks.FirstOrDefault(x => x.ProductStockId == productStockId && x.Status == 1 && x.CompanyId == Global.Instance.CompanyId);

                productStk.ProductStockId = productStock.ProductStockId;
                productStk.ProductId = productStock.ProductId;
                productStk.StockQty = productStock.StockQty;
                productStk.StockDate = productStock.StockDate;
                productStk.CreatedBy = productStock.CreatedBy;
                productStk.CreatedDate = productStock.CreatedDate;
                productStk.ModifiedBy = productStock.ModifiedBy;
                productStk.ModifiedDate = productStock.ModifiedDate;
                productStk.Status = productStock.Status;
                productStk.CompanyId = productStock.CompanyId;
                return productStk;
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }
            return productStk;
        }
        public static decimal? GetProductTotalStockAvailable(int? ProductId)
        {
            decimal? AvailableQty = null;
            try
            {
                var db = new InvoiceEntities();
                AvailableQty = db.ProductStocks.Where(x => x.ProductId == ProductId && x.Status == 1 && x.CompanyId == Global.Instance.CompanyId).Sum(x => x.BalanceQty);
                return AvailableQty ?? 0;
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }
            return AvailableQty ?? 0;
        }

        public static ObservableCollection<Product> GetProducts()
        {
            var data = new ObservableCollection<Product>();
            try
            {
                var db = new InvoiceEntities();
                data = new ObservableCollection<Product>(db.Products.Where(x => x.Status == 1 && x.CompanyId == Global.Instance.CompanyId).ToList());
                return data;
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }
            return data;
        }
    }
}
