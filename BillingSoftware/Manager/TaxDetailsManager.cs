﻿using BillingSoftware.EntityModel;
using BillingSoftware.Model;
using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingSoftware.Manager
{
    public class TaxDetailsManager
    {
        public static ObservableCollection<EntityModel.TaxTypeMaster> GetTaxTypes()
        {
            try
            {
                using (var db = new InvoiceEntities())
                {
                    var TaxTypes = db.TaxTypeMasters.Where(x => x.TaxTypeId > 0 && x.Status == 1).ToList();
                    return new ObservableCollection<TaxTypeMaster>(TaxTypes);
                }
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }
            return new ObservableCollection<TaxTypeMaster>();
        }
        public static ObservableCollection<EntityModel.TaxRateMaster> GetTaxRates()
        {
            try
            {
                using (var db = new InvoiceEntities())
                {
                    var taxRates = db.TaxRateMasters.Where(x => x.TaxRateId > 0 && x.Status == 1).ToList();
                    return new ObservableCollection<TaxRateMaster>(taxRates);
                }
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }
            return new ObservableCollection<TaxRateMaster>();
        }
        public static ObservableCollection<EntityModel.TaxHeader> GetTaxHeaders()
        {
            try
            {
                var db = new InvoiceEntities();
                var taxHeader = db.TaxHeaders.Where(x => x.TaxHeaderId > 0 && x.Status == 1).ToList();
                return new ObservableCollection<TaxHeader>(taxHeader);

            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }
            return new ObservableCollection<TaxHeader>();
        }
        public static Model.TaxHeaderModel GetTaxHeaderById(int TaxHeaderId)
        {
            try
            {
                var db = new InvoiceEntities();
                var taxHeader = db.TaxHeaders.FirstOrDefault(x => x.TaxHeaderId == TaxHeaderId && x.Status == 1);
                return ParseTaxHeaderDetails(taxHeader);
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }
            return new Model.TaxHeaderModel();
        }


        private static Model.TaxHeaderModel ParseTaxHeaderDetails(EntityModel.TaxHeader taxHeader)
        {

            var taxHdr = new Model.TaxHeaderModel();
            taxHdr.TaxHeaderId = taxHeader.TaxHeaderId;
            taxHdr.TaxName = taxHeader.TaxName;
            taxHdr.CreatedBy = taxHeader.CreatedBy;
            taxHdr.CreatedDate = taxHeader.CreatedDate;
            taxHdr.ModifiedBy = taxHeader.ModifiedBy;
            taxHdr.ModifiedDate = taxHeader.ModifiedDate;
            taxHdr.Status = taxHeader.Status;
            taxHdr.CompanyId = taxHeader.CompanyId;

            foreach (var _obj in taxHeader.TaxDetails)
            {
                var objTaxDetail = new Model.TaxDetailModel
                {
                    TaxDetailsId = _obj.TaxDetailsId,
                    Status = _obj.Status,
                    CreatedDate = _obj.CreatedDate,
                    CreatedBy = _obj.CreatedBy,
                    CompanyId = _obj.CompanyId,
                    TaxRateId = _obj.TaxRateId,
                    TaxHeaderId = _obj.TaxHeaderId,
                    ModifiedBy = _obj.ModifiedBy,
                    ModifiedDate = _obj.ModifiedDate,
                    TaxTypeId = _obj.TaxTypeId,
                    TaxHeader = _obj.TaxHeader,
                    TaxTypeMaster = _obj.TaxTypeMaster,
                    TaxRate = _obj.TaxRateMaster.TaxRate,
                    GstType = _obj.TaxTypeMaster.GstType,
                    TaxRateMaster = _obj.TaxRateMaster,
                };
                if (objTaxDetail.Status > 0)
                    taxHdr.TaxDetails.Add(objTaxDetail);
            }

            return taxHdr;
        }


        public static ObservableCollection<Model.TaxDetailModel> ParseDetails(ObservableCollection<EntityModel.TaxDetail> taxdetails)
        {
            var taxDetails = new ObservableCollection<Model.TaxDetailModel>();
            foreach (var _obj in taxdetails)
            {
                var objTaxDetail = new Model.TaxDetailModel
                {
                    TaxDetailsId = _obj.TaxDetailsId,
                    Status = _obj.Status,
                    CreatedDate = _obj.CreatedDate,
                    CreatedBy = _obj.CreatedBy,
                    CompanyId = _obj.CompanyId,
                    TaxRateId = _obj.TaxRateId,
                    TaxHeaderId = _obj.TaxHeaderId,
                    ModifiedBy = _obj.ModifiedBy,
                    ModifiedDate = _obj.ModifiedDate,
                    TaxTypeId = _obj.TaxTypeId,
                    TaxHeader = _obj.TaxHeader,
                    TaxTypeMaster = _obj.TaxTypeMaster,
                    TaxRateMaster = _obj.TaxRateMaster,
                };
                taxDetails.Add(objTaxDetail);
            }
            return taxDetails;
        }
        public static EntityModel.TaxHeader ParseTaxHeader(TaxHeaderModel objTaxHeader)
        {
            var taxHdr = new EntityModel.TaxHeader();
            taxHdr.TaxHeaderId = objTaxHeader.TaxHeaderId;
            taxHdr.TaxName = objTaxHeader.TaxName;
            taxHdr.CreatedBy = objTaxHeader.CreatedBy;
            taxHdr.CreatedDate = objTaxHeader.CreatedDate;
            taxHdr.ModifiedBy = Global.Instance.LoginUserName;
            taxHdr.ModifiedDate = DateTime.Now;
            taxHdr.Status = objTaxHeader.Status;
            taxHdr.CompanyId = Global.Instance.CompanyId;
            return taxHdr;
        }
        public static bool SaveData(TaxHeader objTaxHeader, ObservableCollection<Model.TaxDetailModel> taxDetailsColl, out TaxHeader tempTaxHeader, ref StringBuilder message)
        {
            var updtTaxDetils = new List<TaxDetail>();
            var addTaxDetils = new List<TaxDetail>();

            if (objTaxHeader.TaxHeaderId > 0)
            {
                try
                {
                    objTaxHeader.ModifiedBy = Global.Instance.LoginUserName;
                    objTaxHeader.ModifiedDate = DateTime.Now;

                    foreach (var taxDtl in taxDetailsColl.ToList())
                    {
                        var taxDetail = new EntityModel.TaxDetail();

                        if (taxDtl.TaxDetailsId > 0)
                        {
                            taxDetail.TaxDetailsId = taxDtl.TaxDetailsId;
                            taxDetail.TaxHeaderId = objTaxHeader.TaxHeaderId;
                            taxDetail.TaxTypeId = taxDtl.TaxTypeId;
                            taxDetail.TaxRateId = taxDtl.TaxRateId;
                            taxDetail.CreatedBy = taxDtl.CreatedBy;
                            taxDetail.CreatedDate = taxDtl.CreatedDate;
                            taxDetail.ModifiedBy = taxDtl.ModifiedBy;
                            taxDetail.ModifiedDate = taxDtl.ModifiedDate;
                            taxDetail.CompanyId = taxDtl.CompanyId;
                            taxDetail.Status = taxDtl.Status;

                            updtTaxDetils.Add(taxDetail);
                        }
                        else
                        {
                            taxDetail.TaxDetailsId = 0;
                            taxDetail.TaxHeaderId = objTaxHeader.TaxHeaderId;
                            taxDetail.TaxTypeId = taxDtl.TaxTypeId;
                            taxDetail.TaxRateId = taxDtl.TaxRateId;
                            taxDetail.CreatedBy = taxDtl.CreatedBy;
                            taxDetail.CreatedDate = taxDtl.CreatedDate;
                            taxDetail.ModifiedBy = taxDtl.ModifiedBy;
                            taxDetail.ModifiedDate = taxDtl.ModifiedDate;
                            taxDetail.CompanyId = taxDtl.CompanyId;
                            taxDetail.Status = taxDtl.Status;
                            addTaxDetils.Add(taxDetail);
                        }

                    }
                    #region DbTransaction
                    var uof = new FluentUnitOfWork(new InvoiceEntities());
                    try
                    {
                        bool Status = uof.BeginTransaction()
                            .DoUpdate(objTaxHeader)
                            .DoInsertColl(addTaxDetils)
                            .DoUpdateColl(updtTaxDetils)
                            .SaveAndContinue()
                            .EndTransaction();

                        if (Status)
                        {
                            tempTaxHeader = objTaxHeader;
                            message.Append(MasterMessages.Inserted);
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        uof.RollBack();
                        new ErrorLogger.ErrLogger(ex);
                        message.Append(GeneralMessages.ErrorLog);
                    }
                }
                catch (Exception ex)
                {

                    new ErrorLogger.ErrLogger(ex);
                    message.Append(GeneralMessages.ErrorLog);
                }
                #endregion
            }
            else
            {
                objTaxHeader.TaxHeaderId = 0;
                objTaxHeader.ModifiedBy = Global.Instance.LoginUserName;
                objTaxHeader.ModifiedDate = DateTime.Now;
                foreach (var taxDtl in taxDetailsColl.ToList())
                {
                    var taxDetail = new EntityModel.TaxDetail();

                    taxDetail.TaxDetailsId = 0;
                    taxDetail.TaxHeaderId = objTaxHeader.TaxHeaderId;
                    taxDetail.TaxTypeId = taxDtl.TaxTypeId;
                    taxDetail.TaxRateId = taxDtl.TaxRateId;
                    taxDetail.CreatedBy = taxDtl.CreatedBy;
                    taxDetail.CreatedDate = taxDtl.CreatedDate;
                    taxDetail.ModifiedBy = Global.Instance.LoginUserName;
                    taxDetail.ModifiedDate = DateTime.Now;
                    taxDetail.CompanyId = taxDtl.CompanyId;
                    taxDetail.Status = taxDtl.Status;

                    addTaxDetils.Add(taxDetail);


                }

                #region DbTransaction        
                var uof = new FluentUnitOfWork(new InvoiceEntities());
                try
                {

                    bool Status = uof.BeginTransaction()
                        .DoInsert(objTaxHeader)
                        .DoInsertColl(addTaxDetils)
                        .SaveAndContinue()
                        .EndTransaction();

                    if (Status)
                    {
                        tempTaxHeader = objTaxHeader;
                        message.Append(MasterMessages.Inserted);
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    uof.RollBack();
                    new ErrorLogger.ErrLogger(ex);
                    message.Append(GeneralMessages.ErrorLog);
                }
                #endregion
            }
            tempTaxHeader = null;
            return false;
        }
    }
}
