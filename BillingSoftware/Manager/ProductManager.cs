﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BillingSoftware.EntityModel;
using BillingSoftware.Model;
using ResourceFiles.Properties;

namespace BillingSoftware.Manager
{
    class ProductManager
    {
        public static ProductModel GetProductById(int productTypeId)
        {
            var objProduct = new ProductModel();
            try
            {
                using (var db = new InvoiceEntities())
                {
                    var product = db.Products.FirstOrDefault(x => x.ProductId == productTypeId && x.Status == 1);
                    if (product != null)
                    {
                        objProduct.ProductId = product.ProductId;
                        objProduct.ProductionTypeId = product.ProductionTypeId;                  
                        objProduct.ProductColor = product.ProductColor;
                        objProduct.ProductSize = product.ProductSize;
                        objProduct.ProductDetails = product.ProductDetails;
                        objProduct.IsGstApplicable = product.IsGstApplicable;
                        objProduct.ProductName = product.ProductName;
                        objProduct.CompanyId = product.CompanyId;
                        objProduct.Status = product.Status;
                        objProduct.CreatedBy = product.CreatedBy;
                        objProduct.ModifiedBy = product.ModifiedBy;
                        objProduct.ModifiedDate = product.ModifiedDate;
                        objProduct.CreatedDate = product.CreatedDate;
                        return objProduct;
                    }
                }
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }

            return objProduct;
        }
        public static ObservableCollection<Product> GetProductCollection()
        {
            var data = new ObservableCollection<Product>();
            try
            {
                var db = new InvoiceEntities();
                return data = new ObservableCollection<Product>(db.Products.Where(x => x.Status == 1 && x.ProductId > 0).ToList());

            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }
            return data;
        }

        public static bool DeleteRecord(EntityModel.Product ObjProduct)
        {
            var product = new Product();
            if (ObjProduct.ProductId > 0)
            {
                product.ProductId = ObjProduct.ProductId;
                product.IsGstApplicable = ObjProduct.IsGstApplicable;
                product.ProductName = ObjProduct.ProductName;
                product.ProductionTypeId = ObjProduct.ProductionTypeId;
                product.ProductColor = ObjProduct.ProductColor;
                product.ProductSize = ObjProduct.ProductSize;
                product.ProductDetails = ObjProduct.ProductDetails;
                product.CreatedBy = ObjProduct.CreatedBy;
                product.ModifiedBy = Global.Instance.LoginUserName;
                product.CreatedBy = ObjProduct.CreatedBy;
                product.CreatedDate = ObjProduct.CreatedDate;
                product.ModifiedDate = DateTime.Now;
                product.CompanyId = ObjProduct.CompanyId;
                product.Status = 0;

                #region DbTransaction
                var uof = new FluentUnitOfWork(new InvoiceEntities());
                try
                {
                    bool status =
                        uof.BeginTransaction()
                            .DoUpdate(product)
                            .SaveAndContinue()
                            .EndTransaction();
                    if (status)
                    {
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    uof.RollBack();
                    new ErrorLogger.ErrLogger(ex);
                }

                #endregion
            }

            return false;

        }

        public static ObservableCollection<ProductType> GetProductCategoires(int companyId)
        {
            try
            {
                using (var db = new InvoiceEntities())
                {
                    var countries = db.ProductTypes.Where(x => x.ProductTypeId > 0 && x.Status == 1 && x.CompanyId == companyId).ToList();
                    return new ObservableCollection<ProductType>(countries);
                }
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }
            return new ObservableCollection<ProductType>();
        }

        public static bool SaveData(ProductModel objProduct, out Product productModel, ref StringBuilder message)
        {
            #region Db Transaction
            var unitOfWork = new FluentUnitOfWork(new InvoiceEntities());

            #endregion
            if (objProduct != null)
            {
                var product = new Product();

                if (objProduct.ProductId > 0)
                {
                    product.ProductId = objProduct.ProductId;
                    product.ProductName = objProduct.ProductName;
                    product.IsGstApplicable = objProduct.IsGstApplicable ?? false;
                    product.ProductionTypeId = objProduct.ProductionTypeId;
                    product.ProductColor = objProduct.ProductColor;
                    product.ProductSize = objProduct.ProductSize;
                    product.ProductDetails = objProduct.ProductDetails;
                    //
                    product.CompanyId = objProduct.CompanyId;
                    product.Status = objProduct.Status;
                    product.CreatedBy = objProduct.CreatedBy;
                    product.ModifiedBy = Global.Instance.LoginUserName;
                    product.ModifiedDate = DateTime.Now;
                    product.CreatedDate = objProduct.CreatedDate;

                    try
                    {
                        bool status = unitOfWork.
                            BeginTransaction()
                            .DoUpdate(product)
                            .EndTransaction();
                        if (status)
                        {
                            productModel = product;
                            message.Append(MasterMessages.Updated);
                            return true;
                        }

                        productModel = null;
                        return false;
                    }
                    catch (Exception ex)
                    {
                        unitOfWork.RollBack();
                        new ErrorLogger.ErrLogger(ex);
                    }

                }
                else
                {
                    product.ProductId = objProduct.ProductId;
                    product.ProductName = objProduct.ProductName;
                    product.IsGstApplicable = objProduct.IsGstApplicable ?? false;
                    product.ProductionTypeId = objProduct.ProductionTypeId;
                    product.ProductColor = objProduct.ProductColor;
                    product.ProductSize = objProduct.ProductSize;
                    product.ProductDetails = objProduct.ProductDetails;
                    //
                    product.CompanyId = objProduct.CompanyId;
                    product.Status = objProduct.Status;
                    product.CreatedBy = objProduct.CreatedBy;
                    product.ModifiedBy = Global.Instance.LoginUserName;
                    product.ModifiedDate = DateTime.Now;
                    product.CreatedDate = objProduct.CreatedDate;

                    try
                    {
                        bool status = unitOfWork.
                            BeginTransaction()
                            .DoInsert(product, out product)
                            .EndTransaction();
                        if (status)
                        {
                            productModel = product;
                            message.Append(MasterMessages.Inserted);
                            return true;
                        }

                        productModel = null;
                        return false;
                    }
                    catch (Exception ex)
                    {
                        unitOfWork.RollBack();
                        new ErrorLogger.ErrLogger(ex);
                    }
                }
            }
            productModel = null;
            return false;
        }

        public static bool CheckDuplicate(string productName, string oldProductName)
        {
            using (var db = new InvoiceEntities())
            {
                return db.Products.Count(x => x.ProductName == productName && x.ProductName != oldProductName) > 0;
            }
        }
    }
}
