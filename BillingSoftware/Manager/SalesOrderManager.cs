﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BillingSoftware.Model;
using BillingSoftware.EntityModel;
using ResourceFiles.Properties;
using BillingSoftware.HelperFunction;
using BillingSoftware.Properties;

namespace BillingSoftware.Manager
{
    public class SalesOrderManager
    {
        public static bool SaveData(Model.SalesOrderModel objOrder, ObservableCollection<SalesOrderItemModel> SOLines, EntityModel.Sequence Seq, ref StringBuilder message, out EntityModel.SalesOrder tempOrder)
        {
            EntityModel.SalesOrder order = new EntityModel.SalesOrder();
            List<EntityModel.SalesOrderItem> UpdateSOLines = new List<EntityModel.SalesOrderItem>();
            List<EntityModel.SalesOrderItem> AddSOLines = new List<EntityModel.SalesOrderItem>();


            List<EntityModel.SalesOrderTax> UpdateSOLineTaxes = new List<EntityModel.SalesOrderTax>();
            List<EntityModel.SalesOrderTax> AddSOLineTaxes = new List<EntityModel.SalesOrderTax>();

            Sequence Sequence = new Sequence();

            if (objOrder != null)
            {
                if (SOLines.Count > 0)
                {
                    if (objOrder.OrderId > 0)
                    {
                        order.OrderId = objOrder.OrderId;
                        order.OrderNo = objOrder.OrderNo;
                        order.DateOrderPlaced = objOrder.DateOrderPlaced;
                        order.OrderDescription = objOrder.OrderDescription;
                        order.CustId = objOrder.CustId;
                        order.CreatedBy = objOrder.CreatedBy;
                        order.CreatedDate = objOrder.CreatedDate;
                        order.ModifiedBy = objOrder.ModifiedBy;
                        order.ModifiedDate = objOrder.ModifiedDate;
                        order.CompanyId = objOrder.CompanyId;
                        order.Status = objOrder.Status;
                        order.NetAmount = objOrder.NetAmount;
                        order.TaxAmount = objOrder.TaxAmount;
                        order.GrossAmont = objOrder.GrossAmont;
                        order.SOHeaderStatus = objOrder.SOHeaderStatus;

                        foreach (var item in SOLines)
                        {
                            EntityModel.SalesOrderItem ordItms = new EntityModel.SalesOrderItem();

                            if (item.OrderItemId > 0)
                            {
                                ordItms.OrderItemId = item.OrderItemId;
                                ordItms.OrderId = item.OrderId;
                                ordItms.TaxHeaderId = item.TaxHeaderId;
                                ordItms.ProductId = item.ProductId;
                                ordItms.Qty = item.Qty;
                                ordItms.Rate = item.Rate;
                                ordItms.DiscPer = item.DiscPer;
                                ordItms.DiscAmt = item.DiscAmt;
                                ordItms.FreightPer = item.FreightPer;
                                ordItms.FreightAmt = item.FreightAmt;
                                ordItms.GrossAmount = item.GrossAmount;
                                ordItms.NetAmount = item.NetAmount;
                                ordItms.TaxAmount = item.TaxAmount;
                                ordItms.OtherProductItemDetails = item.OtherProductItemDetails;
                                ordItms.CreatedBy = item.CreatedBy;
                                ordItms.CreatedDate = item.CreatedDate;
                                ordItms.ModifiedBy = item.ModifiedBy;
                                ordItms.ModifiedDate = item.ModifiedDate;
                                ordItms.CompanyId = item.CompanyId;
                                ordItms.LineStatus = item.LineStatus;
                                ordItms.Status = item.Status;

                                foreach (var SoTax in item.SalesOrderTaxes)
                                {
                                    SalesOrderTax orderTax = new SalesOrderTax();

                                    orderTax.SOTaxId = SoTax.SOTaxId;
                                    orderTax.OrderItemId = ordItms.OrderItemId;
                                    orderTax.TaxDetailsId = SoTax.TaxDetailsId;
                                    orderTax.CompanyId = SoTax.CompanyId;
                                    orderTax.TaxAmount = SoTax.TaxAmount;
                                    orderTax.TaxPercent = SoTax.TaxPercent;
                                    orderTax.CalculatedOn = SoTax.CalculatedOn;
                                    orderTax.TaxTypeName = SoTax.TaxTypeName;
                                    orderTax.TaxValue = SoTax.TaxValue;
                                    orderTax.CreatedBy = SoTax.CreatedBy;
                                    orderTax.CreatedDate = SoTax.CreatedDate;
                                    orderTax.ModifiedBy = SoTax.ModifiedBy;
                                    orderTax.ModifiedDate = SoTax.ModifiedDate;
                                    orderTax.Status = SoTax.Status;

                                    if (orderTax.SOTaxId > 0)
                                    {
                                        if (ordItms.Status == 0)
                                            orderTax.Status = 0;

                                        UpdateSOLineTaxes.Add(orderTax);
                                    }
                                    else
                                    {
                                        if (ordItms.Status == 0)
                                            orderTax.Status = 0;

                                        AddSOLineTaxes.Add(orderTax);
                                    }

                                }

                                UpdateSOLines.Add(ordItms);

                            }
                            else
                            {

                                ordItms.OrderItemId = item.OrderItemId;
                                ordItms.OrderId = item.OrderId;
                                ordItms.TaxHeaderId = item.TaxHeaderId;
                                ordItms.ProductId = item.ProductId;
                                ordItms.Qty = item.Qty;
                                ordItms.Rate = item.Rate;
                                ordItms.DiscPer = item.DiscPer;
                                ordItms.DiscAmt = item.DiscAmt;
                                ordItms.FreightPer = item.FreightPer;
                                ordItms.FreightAmt = item.FreightAmt;
                                ordItms.GrossAmount = item.GrossAmount;
                                ordItms.NetAmount = item.NetAmount;
                                ordItms.TaxAmount = item.TaxAmount;
                                ordItms.OtherProductItemDetails = item.OtherProductItemDetails;
                                ordItms.CreatedBy = item.CreatedBy;
                                ordItms.CreatedDate = item.CreatedDate;
                                ordItms.ModifiedBy = item.ModifiedBy;
                                ordItms.ModifiedDate = item.ModifiedDate;
                                ordItms.CompanyId = item.CompanyId;
                                ordItms.LineStatus = item.LineStatus;
                                ordItms.Status = item.Status;

                                foreach (var SoTax in item.SalesOrderTaxes)
                                {
                                    SalesOrderTax orderTax = new SalesOrderTax();
                                    orderTax.SOTaxId = 0;
                                    orderTax.OrderItemId = ordItms.OrderItemId;
                                    orderTax.TaxDetailsId = SoTax.TaxDetailsId;
                                    orderTax.CompanyId = SoTax.CompanyId;
                                    orderTax.TaxAmount = SoTax.TaxAmount;
                                    orderTax.TaxPercent = SoTax.TaxPercent;
                                    orderTax.CalculatedOn = SoTax.CalculatedOn;
                                    orderTax.TaxTypeName = SoTax.TaxTypeName;
                                    orderTax.TaxValue = SoTax.TaxValue;
                                    orderTax.CreatedBy = SoTax.CreatedBy;
                                    orderTax.CreatedDate = SoTax.CreatedDate;
                                    orderTax.ModifiedBy = SoTax.ModifiedBy;
                                    orderTax.ModifiedDate = SoTax.ModifiedDate;
                                    orderTax.Status = SoTax.Status;

                                    if (orderTax.SOTaxId > 0)
                                    {
                                        UpdateSOLineTaxes.Add(orderTax);
                                    }
                                    else
                                    {
                                        AddSOLineTaxes.Add(orderTax);
                                    }
                                }

                                AddSOLines.Add(ordItms);
                            }
                        }
                        #region DbTransaction

                        var uof = new FluentUnitOfWork(new InvoiceEntities());
                        try
                        {

                            bool Status = uof.BeginTransaction()
                                .DoUpdate(order)
                                .DoInsertColl(AddSOLines)
                                .DoUpdateColl(UpdateSOLines)
                                .DoUpdateColl(UpdateSOLineTaxes)
                                .DoInsertColl(AddSOLineTaxes)
                                .SaveAndContinue()
                                .EndTransaction();

                            if (Status)
                            {
                                tempOrder = order;
                                message.Append(MasterMessages.Inserted);
                                return true;
                            }
                        }
                        catch (Exception ex)
                        {
                            uof.RollBack();
                            new ErrorLogger.ErrLogger(ex);
                            message.Append(GeneralMessages.ErrorLog);
                        }
                        #endregion

                    }
                    else
                    {
                        order.OrderId = 0;
                        if (Seq != null)
                            order.OrderNo = SequenceGenerator.GenerateSequence(Seq.SeqId, Seq.SeqHeaderId.Value, Global.OrderSeqForm, Seq.TotalLength.Value, Seq.prefix, out Sequence);
                        else
                            order.OrderNo = string.Empty;
                        if (objOrder.DateOrderPlaced != null)

                            order.DateOrderPlaced = objOrder.DateOrderPlaced;
                        else
                            order.DateOrderPlaced = DateTime.Now;


                        order.OrderDescription = objOrder.OrderDescription;
                        order.CustId = objOrder.CustId;
                        order.CreatedBy = objOrder.CreatedBy;
                        order.CreatedDate = objOrder.CreatedDate;
                        order.ModifiedBy = Global.Instance.LoginUserName;
                        order.ModifiedDate = DateTime.Now;
                        order.CompanyId = objOrder.CompanyId;
                        order.Status = objOrder.Status;
                        order.NetAmount = objOrder.NetAmount;
                        order.TaxAmount = objOrder.TaxAmount;
                        order.GrossAmont = objOrder.GrossAmont;
                        order.SOHeaderStatus = objOrder.SOHeaderStatus;


                        foreach (var item in SOLines)
                        {
                            EntityModel.SalesOrderItem ordItms = new EntityModel.SalesOrderItem();

                            ordItms.OrderItemId = item.OrderItemId;
                            ordItms.OrderId = item.OrderId;
                            ordItms.TaxHeaderId = item.TaxHeaderId;
                            ordItms.ProductId = item.ProductId;
                            ordItms.Qty = item.Qty;
                            ordItms.Rate = item.Rate;
                            ordItms.DiscPer = item.DiscPer;
                            ordItms.DiscAmt = item.DiscAmt;
                            ordItms.FreightPer = item.FreightPer;
                            ordItms.FreightAmt = item.FreightAmt;
                            ordItms.GrossAmount = item.GrossAmount;
                            ordItms.NetAmount = item.NetAmount;
                            ordItms.TaxAmount = item.TaxAmount;
                            ordItms.OtherProductItemDetails = item.OtherProductItemDetails;
                            ordItms.CreatedBy = item.CreatedBy;
                            ordItms.CreatedDate = item.CreatedDate;
                            ordItms.ModifiedBy = item.ModifiedBy;
                            ordItms.ModifiedDate = item.ModifiedDate;
                            ordItms.CompanyId = item.CompanyId;
                            ordItms.LineStatus = item.LineStatus;
                            ordItms.Status = item.Status;

                            foreach (var SoTax in item.SalesOrderTaxes)
                            {
                                SalesOrderTax orderTax = new SalesOrderTax();
                                orderTax.SOTaxId = 0;
                                orderTax.OrderItemId = ordItms.OrderItemId;
                                orderTax.TaxDetailsId = SoTax.TaxDetailsId;
                                orderTax.CompanyId = SoTax.CompanyId;
                                orderTax.TaxAmount = SoTax.TaxAmount;
                                orderTax.TaxPercent = SoTax.TaxPercent;
                                orderTax.CalculatedOn = SoTax.CalculatedOn;
                                orderTax.TaxTypeName = SoTax.TaxTypeName;
                                orderTax.TaxValue = SoTax.TaxValue;
                                orderTax.CreatedBy = SoTax.CreatedBy;
                                orderTax.CreatedDate = SoTax.CreatedDate;
                                orderTax.ModifiedBy = SoTax.ModifiedBy;
                                orderTax.ModifiedDate = SoTax.ModifiedDate;
                                orderTax.Status = SoTax.Status;

                                ordItms.SalesOrderTaxes.Add(orderTax);

                            }

                            order.SalesOrderItems.Add(ordItms);
                        }

                        #region DbTransaction

                        var uof = new FluentUnitOfWork(new InvoiceEntities());
                        try
                        {

                            bool Status = uof.BeginTransaction()
                                .DoInsert(order, out order)
                                .DoUpdate(Sequence)
                                .SaveAndContinue()
                                .EndTransaction();

                            if (Status)
                            {
                                tempOrder = order;
                                message.Append(MasterMessages.Inserted);
                                return true;
                            }
                        }
                        catch (Exception ex)
                        {
                            uof.RollBack();
                            new ErrorLogger.ErrLogger(ex);
                            message.Append(GeneralMessages.ErrorLog);
                        }
                        #endregion
                    }
                }
            }

            tempOrder = null;
            return false;
        }

        public static void PrintSalesOrder(SalesOrder obj)
        {
            
        }

        public static bool SalesOrderProcess(SalesOrder obj, ref StringBuilder message)
        {
            bool IsProcessed = false;
            List<ProductStock> updateStock = new List<ProductStock>();
            List<ProductSale> productSales = new List<ProductSale>();

            List<EntityModel.SalesOrderItem> UpdateSOLines = new List<EntityModel.SalesOrderItem>();
            List<EntityModel.SalesOrderTax> UpdateSOLineTaxes = new List<EntityModel.SalesOrderTax>();


            var db = new InvoiceEntities();
            var SOHeader = db.SalesOrders.AsNoTracking().FirstOrDefault(x => x.OrderId == obj.OrderId);
            Dictionary<int?, decimal?> TotalIssuedQty = new Dictionary<int?, decimal?>();

            if (SOHeader != null)
            {
                #region Update SO Header
                EntityModel.SalesOrder order = new EntityModel.SalesOrder();

                order.OrderId = SOHeader.OrderId;
                order.OrderNo = SOHeader.OrderNo;
                order.DateOrderPlaced = SOHeader.DateOrderPlaced;
                order.OrderDescription = SOHeader.OrderDescription;
                order.CustId = SOHeader.CustId;
                order.CreatedBy = SOHeader.CreatedBy;
                order.CreatedDate = SOHeader.CreatedDate;
                order.ModifiedBy = SOHeader.ModifiedBy;
                order.ModifiedDate = SOHeader.ModifiedDate;
                order.CompanyId = SOHeader.CompanyId;
                order.Status = SOHeader.Status;
                order.NetAmount = SOHeader.NetAmount;
                order.TaxAmount = SOHeader.TaxAmount;
                order.GrossAmont = SOHeader.GrossAmont;

                //    --Update SO heade Completed--
                order.SOHeaderStatus = Global.Completed;

                #endregion

                #region Update SO Line & TAX

                foreach (var orderItem in SOHeader.SalesOrderItems)
                {

                    EntityModel.SalesOrderItem ordItms = new EntityModel.SalesOrderItem();

                    ordItms.OrderItemId = orderItem.OrderItemId;
                    ordItms.OrderId = orderItem.OrderId;
                    ordItms.TaxHeaderId = orderItem.TaxHeaderId;
                    ordItms.ProductId = orderItem.ProductId;
                    ordItms.Qty = orderItem.Qty;
                    ordItms.Rate = orderItem.Rate;
                    ordItms.DiscPer = orderItem.DiscPer;
                    ordItms.DiscAmt = orderItem.DiscAmt;
                    ordItms.FreightPer = orderItem.FreightPer;
                    ordItms.FreightAmt = orderItem.FreightAmt;
                    ordItms.GrossAmount = orderItem.GrossAmount;
                    ordItms.NetAmount = orderItem.NetAmount;
                    ordItms.TaxAmount = orderItem.TaxAmount;
                    ordItms.OtherProductItemDetails = orderItem.OtherProductItemDetails;
                    ordItms.CreatedBy = orderItem.CreatedBy;
                    ordItms.CreatedDate = orderItem.CreatedDate;
                    ordItms.ModifiedBy = orderItem.ModifiedBy;
                    ordItms.ModifiedDate = orderItem.ModifiedDate;
                    ordItms.CompanyId = orderItem.CompanyId;
                    ordItms.Status = orderItem.Status;

                    //  --Update SO Line Status to Completed--
                    ordItms.LineStatus = Global.Completed;


                    foreach (var SoTax in orderItem.SalesOrderTaxes)
                    {
                        SalesOrderTax orderTax = new SalesOrderTax();

                        orderTax.SOTaxId = SoTax.SOTaxId;
                        orderTax.OrderItemId = ordItms.OrderItemId;
                        orderTax.TaxDetailsId = SoTax.TaxDetailsId;
                        orderTax.CompanyId = SoTax.CompanyId;
                        orderTax.TaxAmount = SoTax.TaxAmount;
                        orderTax.TaxPercent = SoTax.TaxPercent;
                        orderTax.CalculatedOn = SoTax.CalculatedOn;
                        orderTax.TaxTypeName = SoTax.TaxTypeName;
                        orderTax.TaxValue = SoTax.TaxValue;
                        orderTax.CreatedBy = SoTax.CreatedBy;
                        orderTax.CreatedDate = SoTax.CreatedDate;
                        orderTax.ModifiedBy = SoTax.ModifiedBy;
                        orderTax.ModifiedDate = SoTax.ModifiedDate;
                        orderTax.Status = SoTax.Status;

                        UpdateSOLineTaxes.Add(orderTax);

                    }

                    UpdateSOLines.Add(ordItms);
                    #endregion

                #region update Stock

                    var StockOfProduct = db.ProductStocks.AsNoTracking().Where(x => x.ProductId == orderItem.ProductId && x.BalanceQty > 0).ToList();
                    var QtyToBeIssued = orderItem.Qty;

                    var sumOfBalanceQty = StockOfProduct.Sum(x => x.BalanceQty);
                    if (QtyToBeIssued > sumOfBalanceQty)
                    {
                        message.Append(string.Format(TransactionMessages.StockNotAvailable, StockOfProduct.FirstOrDefault().Product.ProductName));
                        return false;
                    }
                    decimal? Issued = 0;
                    foreach (var stock in StockOfProduct)
                    {
                        if (QtyToBeIssued <= 0)
                            break;

                        decimal? IssuedQty = 0;

                        //in warehouse we have 15 , 60 qty and customer customer need 50 then 15 will issue from first row
                        //and 35 in second row 
                        if (QtyToBeIssued > stock.BalanceQty)
                        {
                            IssuedQty = stock.BalanceQty;
                        }
                        else
                        {
                            IssuedQty = QtyToBeIssued;
                        }

                        QtyToBeIssued = QtyToBeIssued - IssuedQty;

                        stock.IssuedQty = IssuedQty + stock.IssuedQty;
                        stock.BalanceQty = stock.StockQty - stock.IssuedQty;
                        updateStock.Add(stock);
                        Issued = Issued + IssuedQty;
                    }
                    TotalIssuedQty.Add(StockOfProduct.First().ProductId, Issued);

                }

                #endregion

                #region insert product Sales
                productSales = updateStock.GroupBy(x => x.ProductId)
                    .Select(x => new ProductSale
                    {
                        SalesOrderId = SOHeader.OrderId,
                        SalesDate = DateTime.Now,
                        CreatedBy = Global.Instance.LoginUserName,
                        CreatedDate = DateTime.Now,
                        CompanyId = Global.Instance.CompanyId,
                        Status = 1,
                        ModifiedDate = DateTime.Now,
                        ModifiedBy = Global.Instance.LoginUserName,
                        QtySold = TotalIssuedQty.FirstOrDefault(y => y.Key == x.Key).Value,
                        ProductId = x.Select(g => g.Product).FirstOrDefault().ProductId

                    }).ToList();


                #endregion

                #region DbTransaction

                var uof = new FluentUnitOfWork(new InvoiceEntities());
                try
                {

                    bool Status = uof.BeginTransaction()

                        .DoUpdate(order)
                        .DoUpdateColl(UpdateSOLines)
                        .DoUpdateColl(UpdateSOLineTaxes)
                        .DoUpdateColl(updateStock)
                        .DoInsertColl(productSales)
                        .SaveAndContinue()
                        .EndTransaction();

                    if (Status)
                    {
                        IsProcessed = true;
                        message.Append(MasterMessages.Inserted);
                        return IsProcessed;
                    }
                }
                catch (Exception ex)
                {
                    IsProcessed = false;
                    uof.RollBack();
                    new ErrorLogger.ErrLogger(ex);
                    message.Append(GeneralMessages.ErrorLog);
                }
                #endregion
            }             

            return IsProcessed;
        }

        public static SalesOrderModel GetOrdersById(int orderId)
        {
            var orderModel = new Model.SalesOrderModel();
            if (orderId > 0)
            {
                var db = new InvoiceEntities();
                var order = db.SalesOrders.FirstOrDefault(x => x.OrderId == orderId && x.CompanyId == Global.Instance.CompanyId && x.Status == 1);
                if (order != null)
                {
                    orderModel = PraseOrderDetails(order);
                }
            }
            return orderModel;
        }

        private static SalesOrderModel PraseOrderDetails(EntityModel.SalesOrder objOrder)
        {
            var order = new Model.SalesOrderModel();

            order.OrderId = objOrder.OrderId;
            order.OrderNo = objOrder.OrderNo;
            order.DateOrderPlaced = objOrder.DateOrderPlaced;
            order.OrderDescription = objOrder.OrderDescription;
            order.CustId = objOrder.CustId;
            order.CreatedBy = objOrder.CreatedBy;
            order.CreatedDate = objOrder.CreatedDate;
            order.ModifiedBy = objOrder.ModifiedBy;
            order.ModifiedDate = objOrder.ModifiedDate;
            order.CompanyId = objOrder.CompanyId;
            order.Status = objOrder.Status;
            order.NetAmount = objOrder.NetAmount;
            order.TaxAmount = objOrder.TaxAmount;
            order.GrossAmont = objOrder.GrossAmont;
            order.SOHeaderStatus = objOrder.SOHeaderStatus;



            foreach (var item in objOrder.SalesOrderItems.Where(x => x.Status == 1))
            {
                SalesOrderItemModel ordItms = new SalesOrderItemModel();

                ordItms.OrderItemId = item.OrderItemId;
                ordItms.OrderId = item.OrderId;
                ordItms.TaxHeaderId = item.TaxHeaderId;
                ordItms.TaxHeader = item.TaxHeader;
                ordItms.ProductId = item.ProductId;
                ordItms.Qty = item.Qty;
                ordItms.Rate = item.Rate;
                ordItms.DiscPer = item.DiscPer;
                ordItms.DiscAmt = item.DiscAmt;
                ordItms.FreightPer = item.FreightPer;
                ordItms.FreightAmt = item.FreightAmt;
                ordItms.GrossAmount = item.GrossAmount;
                ordItms.NetAmount = item.NetAmount;
                ordItms.TaxAmount = item.TaxAmount;
                ordItms.OtherProductItemDetails = item.OtherProductItemDetails;
                ordItms.CreatedBy = item.CreatedBy;
                ordItms.CreatedDate = item.CreatedDate;
                ordItms.ModifiedBy = item.ModifiedBy;
                ordItms.ModifiedDate = item.ModifiedDate;
                ordItms.CompanyId = item.CompanyId;
                ordItms.LineStatus = item.LineStatus;
                ordItms.Status = item.Status;

                if (order.Customer != null)
                {

                    order.Customer.CustId = objOrder.Customer.CustId;
                    order.Customer.CityId = objOrder.Customer.CityId;
                    order.Customer.CompanyId = objOrder.Customer.CompanyId;
                    order.Customer.StateId = objOrder.Customer.StateId;
                    order.Customer.CountryId = objOrder.Customer.CountryId;
                    order.Customer.CreatedBy = objOrder.Customer.CreatedBy;
                    order.Customer.PostalCode = objOrder.Customer.PostalCode;
                    order.Customer.Address = objOrder.Customer.Address;
                    order.Customer.OtherDetail = objOrder.Customer.OtherDetail;
                    order.Customer.CreatedDate = objOrder.Customer.CreatedDate;
                    order.Customer.CustCode = objOrder.Customer.CustCode;
                    order.Customer.CustFName = objOrder.Customer.CustFName;
                    order.Customer.CustLName = objOrder.Customer.CustLName;
                    order.Customer.CustMName = objOrder.Customer.CustMName;
                    order.Customer.EmailAddress = objOrder.Customer.EmailAddress;
                    order.Customer.Gender = objOrder.Customer.Gender;
                    order.Customer.MobileNo = objOrder.Customer.MobileNo;
                    order.Customer.ModifiedDate = objOrder.Customer.ModifiedDate;
                    order.Customer.ModifiedBy = objOrder.Customer.ModifiedBy;
                    order.Customer.Status = objOrder.Customer.Status;

                    if (objOrder.Customer.City != null)
                    {
                        order.Customer.City.CityName = objOrder.Customer.City.CityName;
                    }
                }
                ordItms.Product = new ProductModel();
                ordItms.Product.ProductId = item.Product.ProductId;
                ordItms.Product.ProductionTypeId = item.Product.ProductId;
                ordItms.Product.ProductName = item.Product.ProductName;
                ordItms.Product.ProductColor = item.Product.ProductColor;
                ordItms.Product.ProductSize = item.Product.ProductSize;
                ordItms.Product.ProductDetails = item.Product.ProductDetails;
                ordItms.Product.CreatedBy = item.Product.CreatedBy;
                ordItms.Product.CreatedDate = item.Product.CreatedDate;
                ordItms.Product.ModifiedBy = item.Product.ModifiedBy;
                ordItms.Product.ModifiedDate = item.Product.ModifiedDate;
                ordItms.Product.CompanyId = item.Product.CompanyId;
                ordItms.Product.IsGstApplicable = item.Product.IsGstApplicable;
                ordItms.Product.Status = item.Product.Status;
                ordItms.Product.ProductType = item.Product.ProductType;
                ordItms.Product.ProductStocks = item.Product.ProductStocks;
                ordItms.Product.SalesOrderItems = item.Product.SalesOrderItems;
                ordItms.Product.ProductSales = item.Product.ProductSales;

                foreach (var SoTax in item.SalesOrderTaxes.Where(x => x.Status == 1))
                {
                    SalesOrderTaxModel orderTax = new SalesOrderTaxModel();
                    orderTax.SOTaxId = SoTax.SOTaxId;
                    orderTax.OrderItemId = ordItms.OrderItemId;
                    orderTax.TaxDetailsId = SoTax.TaxDetailsId;
                    orderTax.CompanyId = SoTax.CompanyId;
                    orderTax.TaxAmount = SoTax.TaxAmount;
                    orderTax.TaxPercent = SoTax.TaxPercent;
                    orderTax.CalculatedOn = SoTax.CalculatedOn;
                    orderTax.TaxTypeName = SoTax.TaxTypeName;
                    orderTax.TaxValue = SoTax.TaxValue;
                    orderTax.CreatedBy = SoTax.CreatedBy;
                    orderTax.CreatedDate = SoTax.CreatedDate;
                    orderTax.ModifiedBy = SoTax.ModifiedBy;
                    orderTax.ModifiedDate = SoTax.ModifiedDate;
                    orderTax.Status = SoTax.Status;
                    orderTax.TaxDetail = SoTax.TaxDetail;



                    ordItms.SalesOrderTaxes.Add(orderTax);

                }
                order.OrderItems.Add(ordItms);
            }

            return order;
        }

        public static ObservableCollection<EntityModel.SalesOrder> GetOrders()
        {
            try
            {
                var db = new InvoiceEntities();
                var orders = db.SalesOrders.Where(x => x.OrderId > 0 && x.Status == 1 && x.SOHeaderStatus != Global.Processed).ToList();
                return new ObservableCollection<EntityModel.SalesOrder>(orders);

            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }
            return new ObservableCollection<EntityModel.SalesOrder>();
        }
    }
}
