﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BillingSoftware.EntityModel;
using BillingSoftware.Model;
using ResourceFiles.Properties;

namespace BillingSoftware.Manager
{
    public class ProductCategoryManager
    {
        public static ProductTypeModel GetProductCatById(int productTypeId)
        {
            var proType = new ProductTypeModel();
            try
            {
                using (var db = new InvoiceEntities())
                {
                    var prdoucType = db.ProductTypes.FirstOrDefault(x => x.ProductTypeId == productTypeId && x.Status == 1);
                    if (prdoucType != null)
                    {
                        proType.ProductTypeId = prdoucType.ProductTypeId;
                        proType.CompanyId = prdoucType.CompanyId;
                        proType.Status = prdoucType.Status;
                        proType.ParentProductTypeName = prdoucType.ParentProductTypeName;
                        proType.ProductTypeDescription = prdoucType.ProductTypeDescription;
                        proType.CreatedBy = prdoucType.CreatedBy;
                        proType.ModifiedBy = prdoucType.ModifiedBy;
                        proType.ModifiedDate = prdoucType.ModifiedDate;
                        proType.CreatedDate = prdoucType.CreatedDate;
                        return proType;
                    }
                }
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }

            return proType;
        }

        public static ObservableCollection<ProductType> GetProductCatCollection()
        {
            var data = new ObservableCollection<ProductType>();
            try
            {
                using (var db = new InvoiceEntities())
                {
                    return data = new ObservableCollection<ProductType>(db.ProductTypes.Where(x => x.Status == 1 && x.ProductTypeId > 0).ToList());
                }
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }
            return data;
        }

        public static bool DeleteRecord(EntityModel.ProductType objProductType)
        {
            ProductType product = new ProductType();
            if (objProductType.ProductTypeId > 0)
            {
                product.ProductTypeId = objProductType.ProductTypeId;
                product.ParentProductTypeName = objProductType.ParentProductTypeName;
                product.ProductTypeDescription = objProductType.ProductTypeDescription;
                product.CreatedBy = objProductType.CreatedBy;
                product.ModifiedBy = Global.Instance.LoginUserName;
                product.CreatedBy = objProductType.CreatedBy;
                product.CreatedDate = objProductType.CreatedDate;
                product.ModifiedDate = DateTime.Now;
                product.CompanyId = objProductType.CompanyId;
                product.Status = 0;

                #region DbTransaction
                var uof = new FluentUnitOfWork(new InvoiceEntities());
                try
                {
                    bool status =
                        uof.BeginTransaction()
                            .DoUpdate(product)
                            .SaveAndContinue()
                            .EndTransaction();
                    if (status)
                    {
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    uof.RollBack();
                    var errLogger = new ErrorLogger.ErrLogger(ex);
                }

                #endregion
            }

            return false;

        }
        public static bool SaveData(ProductTypeModel objProductType, out ProductType productTypeModel, ref StringBuilder message)
        {
            #region Db Transaction
            var unitOfWork = new FluentUnitOfWork(new InvoiceEntities());

            #endregion
            if (objProductType != null)
            {
                var productType = new ProductType();

                if (objProductType.ProductTypeId > 0)
                {
                    productType.ProductTypeId = objProductType.ProductTypeId;
                    productType.ParentProductTypeName = objProductType.ParentProductTypeName;
                    productType.ProductTypeDescription = objProductType.ProductTypeDescription;
                    productType.Status = objProductType.Status;
                    productType.CreatedBy = objProductType.CreatedBy;
                    productType.ModifiedBy = Global.Instance.LoginUserName;
                    productType.CreatedBy = objProductType.CreatedBy;
                    productType.CreatedDate = objProductType.CreatedDate;
                    productType.ModifiedDate = DateTime.Now;
                    productType.CompanyId = objProductType.CompanyId;

                    try
                    {
                        bool status = unitOfWork.
                            BeginTransaction()
                            .DoUpdate(productType)
                            .EndTransaction();
                        if (status)
                        {
                            productTypeModel = productType;
                            message.Append(MasterMessages.Updated.ToString());
                            return true;
                        }
                        else
                        {
                            productTypeModel = null;
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        unitOfWork.RollBack();
                        new ErrorLogger.ErrLogger(ex);
                    }

                }
                else
                {
                    productType.ProductTypeId = 0;
                    productType.ParentProductTypeName = objProductType.ParentProductTypeName;
                    productType.ProductTypeDescription = objProductType.ProductTypeDescription;
                    productType.Status = objProductType.Status;
                    productType.CreatedBy = objProductType.CreatedBy;
                    productType.ModifiedBy = Global.Instance.LoginUserName;
                    productType.CreatedBy = objProductType.CreatedBy;
                    productType.CreatedDate = objProductType.CreatedDate;
                    productType.ModifiedDate = DateTime.Now;
                    productType.CompanyId = objProductType.CompanyId;

                    try
                    {
                        bool status = unitOfWork.
                            BeginTransaction()
                            .DoInsert(productType, out productType)
                            .EndTransaction();
                        if (status)
                        {
                            productTypeModel = productType;
                            message.Append(MasterMessages.Inserted.ToString());
                            return true;
                        }
                        else
                        {
                            productTypeModel = null;
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        unitOfWork.RollBack();
                        new ErrorLogger.ErrLogger(ex);
                    }
                }
            }
            productTypeModel = null;
            return false;
        }

        public static bool CheckDuplicate(string parentProductTypeName, string oldProductTypeName)
        {
            using (var db = new InvoiceEntities())
            {
                return db.ProductTypes.Count(x => x.ParentProductTypeName == parentProductTypeName && x.ParentProductTypeName != oldProductTypeName) > 0;
            }
        }
    }
}
