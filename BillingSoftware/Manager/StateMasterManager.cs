﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using BillingSoftware.EntityModel;
using BillingSoftware.Model;
using ResourceFiles.Properties;

namespace BillingSoftware.Manager
{
    public class StateMasterManager
    {
        public static ObservableCollection<Country> GetCountires()
        {
            try
            {
                using (var db = new InvoiceEntities())
                {
                    var countries = db.Countries.Where(x => x.CountryId > 0 && x.Status == 1).ToList();
                    return new ObservableCollection<Country>(countries);
                }
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }
            return new ObservableCollection<Country>();
        }

        public static bool CheckDuplicate(string stateName, string oldStateName)
        {
            try
            {
                using (var db = new InvoiceEntities())
                {
                    return db.States.Count(x => x.StateName == stateName && stateName != oldStateName) > 0;
                }
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
                MessageBox.Show(GeneralMessages.ErrorLog);
                return false;
            }
        }

        public static StateModel GetStateById(int stateId)
        {
            var stateModel = new StateModel();
            using(var db = new InvoiceEntities())
            {
                var state =  db.States.FirstOrDefault(x => x.StateId == stateId && x.CompanyId == Global.Instance.CompanyId && x.Status == 1);
                if(state != null)
                {
                    stateModel.StateId = state.StateId;
                    stateModel.StateName = state.StateName;
                    stateModel.CompanyId = state.CompanyId;
                    stateModel.CountryId = state.CountryId;
                    stateModel.ModifiedBy = state.ModifiedBy;
                    stateModel.ModifiedDate = state.ModifiedDate;
                    stateModel.CreatedBy = state.CreatedBy;
                    stateModel.CreatedDate = state.CreatedDate;
                    stateModel.StateId = state.StateId;
                    stateModel.Status = state.Status;


                }

            }
            return stateModel;
        }

        public static bool DeleteRecord(EntityModel.State objState)
        {
            try
            {
                State state = new State();
                if (objState.CountryId > 0)
                {
                    state.StateId = objState.StateId;
                    state.StateName = objState.StateName;
                    state.CountryId = objState.CountryId;
                    state.CreatedBy = objState.CreatedBy;
                    state.CreatedDate = objState.CreatedDate;
                    state.ModifiedBy = Global.Instance.LoginUserName;
                    state.ModifiedDate = DateTime.Now;                    
                    state.CompanyId = objState.CompanyId;
                    //delete state
                    state.Status = 0;

                    #region DbTransaction
                    var uof = new FluentUnitOfWork(new InvoiceEntities());
                    try
                    {
                        bool Status =
                             uof.BeginTransaction()
                            .DoUpdate(state)
                            .SaveAndContinue()
                            .EndTransaction();
                        if (Status)
                        {
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        uof.RollBack();
                        new ErrorLogger.ErrLogger(ex);
                    }

                    #endregion
                }

            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);                
            }
            return false;

        }

        public static List<State> GetStates()
        {
            try
            {
                var db = new InvoiceEntities();
                return db.States.Where(x => x.Status == 1 && x.CompanyId == Global.Instance.CompanyId).ToList();                                 
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
                MessageBox.Show(GeneralMessages.ErrorLog);
            }
            return null;
        }

        public static bool SaveData(StateModel objState, ref StringBuilder message, out State tempState)
        {
            if (objState != null)
            {
                State state = new State();

                #region Update case
                if (objState.StateId > 0)
                {
                    state.StateId = objState.StateId;
                    state.StateName = objState.StateName;
                    state.CountryId = objState.CountryId;
                    state.CreatedBy = objState.CreatedBy;
                    state.CreatedDate = objState.CreatedDate;
                    state.ModifiedBy = Global.Instance.LoginUserName;
                    state.ModifiedDate = DateTime.Now;
                    state.Status = objState.Status;
                    state.CompanyId = objState.CompanyId;

                    #region DbTransaction
                    var uof = new FluentUnitOfWork(new InvoiceEntities());
                    try
                    {
                        bool Status =
                             uof.BeginTransaction()
                            .DoUpdate(state)
                            .SaveAndContinue()
                            .EndTransaction();
                        if (Status)
                        {
                            tempState = state;
                            message.Append(MasterMessages.Updated);
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        uof.RollBack();
                        new ErrorLogger.ErrLogger(ex);
                        message.Append(GeneralMessages.ErrorLog);
                    }

                    #endregion
                }
                #endregion

                #region insert case
                else
                {
                    state.StateId = 0;
                    state.StateName = objState.StateName;
                    state.CountryId = objState.CountryId;
                    state.CreatedBy = objState.CreatedBy;
                    state.CreatedDate = objState.CreatedDate;
                    state.ModifiedBy = Global.Instance.LoginUserName;
                    state.ModifiedDate = DateTime.Now;
                    state.Status = objState.Status;
                    state.CompanyId = objState.CompanyId;


                    #region DbTransaction

                    var uof = new FluentUnitOfWork(new InvoiceEntities());
                    try
                    {

                        bool Status = uof.BeginTransaction()
                            .DoInsert(state, out state)
                            .SaveAndContinue()
                            .EndTransaction();

                        if (Status)
                        {
                            tempState = state;
                            message.Append(MasterMessages.Inserted);
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        uof.RollBack();
                        new ErrorLogger.ErrLogger(ex);
                        message.Append(GeneralMessages.ErrorLog);
                    }



                    #endregion
                }
                #endregion
            }
            tempState = null;
            return false;
        }

    }
}
