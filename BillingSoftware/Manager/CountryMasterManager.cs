﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BillingSoftware.EntityModel;
using BillingSoftware.Model;
using ResourceFiles.Properties;
using Utilities;

namespace BillingSoftware.Manager
{
    class CountryMasterManager
    {

        public static bool CheckDuplicate(string CountryCode)
        {
            using (var db = new InvoiceEntities())
            {
                return db.Countries.Count(x => x.CountryCode == CountryCode) > 0;
            }
        }
        public static bool CheckDuplicate(string CountryCode, string OldCountryCode)
        {
            using (var db = new InvoiceEntities())
            {
                return db.Countries.Count(x => x.CountryCode == CountryCode && x.CountryCode != OldCountryCode) > 0;
            }
        }


        public static bool SaveData(CountryModel objCountry, out Country countryModel,ref StringBuilder message)
        {
            #region Db Transaction
            var unitOfWork = new FluentUnitOfWork(new InvoiceEntities());

            #endregion
            if (objCountry != null)
            {
                var country = new Country();

                if (objCountry.CountryId > 0)
                {
                    country.CountryId = objCountry.CountryId;
                    country.CountryCode = objCountry.CountryCode;
                    country.CountryName = objCountry.CountryName;
                    country.Status = objCountry.Status;
                    country.CreatedBy = objCountry.CreatedBy;
                    country.ModifiedBy = Global.Instance.LoginUserName;
                    country.CreatedBy = objCountry.CreatedBy;
                    country.CreatedDate = objCountry.CreatedDate;
                    country.ModifiedDate = DateTime.Now;
                    country.CompanyId = objCountry.CompanyId;

                    try
                    {
                        bool status = unitOfWork.
                            BeginTransaction()
                            .DoUpdate(country)
                            .EndTransaction();
                        if (status)
                        {
                            countryModel = country;
                            message.Append(MasterMessages.Updated.ToString());
                            return true;
                        }
                        else
                        {
                            countryModel = null;
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        unitOfWork.RollBack();
                        new ErrorLogger.ErrLogger(ex);
                    }

                }
                else
                {
                    country.CountryId = 0;
                    country.CountryCode = objCountry.CountryCode;
                    country.CountryName = objCountry.CountryName;
                    country.Status = objCountry.Status;
                    country.CreatedBy = objCountry.CreatedBy;
                    country.ModifiedBy = Global.Instance.LoginUserName;
                    country.CreatedBy = objCountry.CreatedBy;
                    country.CreatedDate = objCountry.CreatedDate;
                    country.ModifiedDate = DateTime.Now;
                    country.CompanyId = objCountry.CompanyId;

                    try
                    {
                        bool status = unitOfWork.
                            BeginTransaction()
                            .DoInsert(country, out country)
                            .EndTransaction();
                        if (status)
                        {
                            countryModel = country;
                            message.Append(MasterMessages.Inserted.ToString());
                            return true;
                        }
                        else
                        {
                            countryModel = null;
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        unitOfWork.RollBack();
                        new ErrorLogger.ErrLogger(ex);
                    }
                }
            }
            countryModel = null;
            return false;
        }

         

        public static bool DeleteRecord(EntityModel.Country objCountry)
        {
            try
            {
                Country country = new Country();
                if (objCountry.CountryId > 0)
                {
                    country.CountryId = objCountry.CountryId;
                    country.CompanyId = objCountry.CompanyId;                   
                    country.CountryCode = objCountry.CountryCode;
                    country.CountryName = objCountry.CountryName;
                    country.CreatedBy = objCountry.CreatedBy;
                    country.ModifiedBy = objCountry.ModifiedBy;
                    country.ModifiedDate = objCountry.ModifiedDate;
                    country.CreatedDate = objCountry.CreatedDate;
                    //delete country
                    country.Status = 0;

                    #region DbTransaction
                    var uof = new FluentUnitOfWork(new InvoiceEntities());
                    try
                    {
                        bool Status =
                             uof.BeginTransaction()
                            .DoUpdate(country)
                            .SaveAndContinue()
                            .EndTransaction();
                        if (Status)
                        {
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        uof.RollBack();
                        new ErrorLogger.ErrLogger(ex);
                    }

                    #endregion
                }

            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }

            return false;

        }

        public static CountryModel GetCountryByCountryId(int countryId)
        {
            CountryModel countryModel = new CountryModel();
            try
            {
                using(var db = new InvoiceEntities())
                {
                    var country =  db.Countries.FirstOrDefault(x => x.CountryId == countryId && x.Status == 1);
                    if(country != null)
                    {
                        countryModel.CountryId = country.CountryId;
                        countryModel.CompanyId = country.CompanyId;
                        countryModel.Status = country.Status;
                        countryModel.CountryCode = country.CountryCode;
                        countryModel.CountryName = country.CountryName;
                        countryModel.CreatedBy = country.CreatedBy;
                        countryModel.ModifiedBy = country.ModifiedBy;
                        countryModel.ModifiedDate = country.ModifiedDate;
                        countryModel.CreatedDate = country.CreatedDate;
                        return countryModel;
                    }
                }
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }

            return countryModel;
        }

        public static ObservableCollection<Country> GetCountryCollection(int start, int itemCount, bool ascending, out int totalItems)
        {
            ObservableCollection<Country> filteredCountries = new ObservableCollection<Country>();
            totalItems = 0;
            try
            {
                using (var db = new InvoiceEntities())
                {
                    var countries = new ObservableCollection<Country>(db.Countries.Where(x => x.Status == 1 && x.CountryId > 0).ToList());
                    totalItems = countries.Count;
                    countries = ascending ? countries : new ObservableCollection<Country>(countries.Reverse());                    
                    for (int i = start; i < start + itemCount && i < totalItems; i++)
                    {
                        filteredCountries.Add(countries[i]);
                    }
                }
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }

                              
            return filteredCountries;
        }
    }
}
