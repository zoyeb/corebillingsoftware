﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using BillingSoftware.EntityModel;
using BillingSoftware.Model;
using ResourceFiles.Properties;

namespace BillingSoftware.Manager
{
    public class CityMasterManager
    {

        public static ObservableCollection<State> GetStates()
        {
            try
            {
                var db = new InvoiceEntities();
                var States = db.States.Where(x => x.StateId > 0 && x.Status == 1).ToList();
                return new ObservableCollection<State>(States);

            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }
            return new ObservableCollection<State>();
        }
        public static ObservableCollection<Country> GetCountires()
        {
            try
            {
                using (var db = new InvoiceEntities())
                {
                    var countries = db.Countries.Where(x => x.CountryId > 0 && x.Status == 1).ToList();
                    return new ObservableCollection<Country>(countries);
                }
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }
            return new ObservableCollection<Country>();
        }

        public static bool CheckDuplicate(string CityName, string oldCityName)
        {
            try
            {
                using (var db = new InvoiceEntities())
                {
                    return db.Cities.Count(x => x.CityName == CityName && CityName != oldCityName) > 0;
                }
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
                MessageBox.Show(GeneralMessages.ErrorLog);
                return false;
            }
        }

        public static CityModel GetCityById(int CityId)
        {
            var CityModel = new CityModel();
            using (var db = new InvoiceEntities())
            {
                var City = db.Cities.FirstOrDefault(x => x.CityId == CityId && x.CompanyId == Global.Instance.CompanyId && x.Status == 1);
                if (City != null)
                {
                    CityModel.CityId = City.CityId;
                    CityModel.CityName = City.CityName;
                    CityModel.CompanyId = City.CompanyId;
                    CityModel.StateId = City.StateId;
                    CityModel.ModifiedBy = City.ModifiedBy;
                    CityModel.ModifiedDate = City.ModifiedDate;
                    CityModel.CreatedBy = City.CreatedBy;
                    CityModel.CreatedDate = City.CreatedDate;
                    CityModel.StateId = City.StateId;
                    CityModel.Status = City.Status;


                }

            }
            return CityModel;
        }

        public static bool DeleteRecord(EntityModel.City objCity)
        {

            try
            {
                City city = new City();
                if (objCity.CityId > 0)
                {
                    city.CityId = objCity.CityId;
                    city.CityName = objCity.CityName;
                    city.StateId = objCity.StateId;
                    city.CreatedBy = objCity.CreatedBy;
                    city.CreatedDate = objCity.CreatedDate;
                    city.ModifiedBy = Global.Instance.LoginUserName;
                    city.ModifiedDate = DateTime.Now;
                    city.CompanyId = objCity.CompanyId;
                    //delete record
                    city.Status = 0;

                    #region DbTransaction
                    var uof = new FluentUnitOfWork(new InvoiceEntities());
                    try
                    {
                        bool Status =
                             uof.BeginTransaction()
                            .DoUpdate(city)
                            .SaveAndContinue()
                            .EndTransaction();
                        if (Status)
                        {
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        uof.RollBack();
                        new ErrorLogger.ErrLogger(ex);
                    }

                    #endregion
                }

            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }

            return false;

        }

        public static List<City> GetCities()
        {
            try
            {
                var db = new InvoiceEntities();
                return db.Cities.Where(x => x.Status == 1 && x.CompanyId == Global.Instance.CompanyId).ToList();
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
                MessageBox.Show(GeneralMessages.ErrorLog);
            }
            return null;
        }

        public static bool SaveData(CityModel objCity, ref StringBuilder message, out City tempCity)
        {
            if (objCity != null)
            {
                City city = new City();

                #region Update case
                if (objCity.CityId > 0)
                {
                    city.CityId = objCity.CityId;
                    city.CityName = objCity.CityName;
                    city.StateId = objCity.StateId;
                    city.CreatedBy = objCity.CreatedBy;
                    city.CreatedDate = objCity.CreatedDate;
                    city.ModifiedBy = Global.Instance.LoginUserName;
                    city.ModifiedDate = DateTime.Now;
                    city.Status = objCity.Status;
                    city.CompanyId = objCity.CompanyId;

                    #region DbTransaction
                    var uof = new FluentUnitOfWork(new InvoiceEntities());
                    try
                    {
                        bool Status =
                             uof.BeginTransaction()
                            .DoUpdate(city)
                            .SaveAndContinue()
                            .EndTransaction();
                        if (Status)
                        {
                            tempCity = city;
                            message.Append(MasterMessages.Updated);
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        uof.RollBack();
                        new ErrorLogger.ErrLogger(ex);
                        message.Append(GeneralMessages.ErrorLog);
                    }

                    #endregion
                }
                #endregion

                #region insert case
                else
                {
                    city.CityId = 0;
                    city.CityName = objCity.CityName;
                    city.StateId = objCity.StateId;
                    city.CreatedBy = objCity.CreatedBy;
                    city.CreatedDate = objCity.CreatedDate;
                    city.ModifiedBy = Global.Instance.LoginUserName;
                    city.ModifiedDate = DateTime.Now;
                    city.Status = objCity.Status;
                    city.CompanyId = objCity.CompanyId;


                    #region DbTransaction

                    var uof = new FluentUnitOfWork(new InvoiceEntities());
                    try
                    {

                        bool Status = uof.BeginTransaction()
                            .DoInsert(city, out city)
                            .SaveAndContinue()
                            .EndTransaction();

                        if (Status)
                        {
                            tempCity = city;
                            message.Append(MasterMessages.Inserted);
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        uof.RollBack();
                        new ErrorLogger.ErrLogger(ex);
                        message.Append(GeneralMessages.ErrorLog);
                    }
                    #endregion
                }
                #endregion
            }
            tempCity = null;
            return false;
        }

    }
}
