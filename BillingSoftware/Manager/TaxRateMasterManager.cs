﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using BillingSoftware.EntityModel;
using BillingSoftware.Model;
using ResourceFiles.Properties;


namespace BillingSoftware.Manager
{
    public class TaxRateMasterManager
    {
        public static ObservableCollection<TaxRateMaster> GetTaxRates()
        {
            ObservableCollection<TaxRateMaster> taxRateMasters = new ObservableCollection<TaxRateMaster>();
            try
            {
                using (var db = new InvoiceEntities())
                {
                    var taxRates = db.TaxRateMasters.Where(x => x.Status == 1 && x.CompanyId == Global.Instance.CompanyId).ToList();
                    foreach (var taxRate in taxRates)
                    {
                        TaxRateMaster taxRateMaster = new TaxRateMaster();

                        taxRateMaster.TaxRateId = taxRate.TaxRateId;
                        taxRateMaster.Active = taxRate.Active ?? false;
                        taxRateMaster.TaxRate = taxRate.TaxRate;
                        taxRateMaster.CompanyId = taxRate.CompanyId;
                        taxRateMaster.CreatedBy = taxRate.CreatedBy;
                        taxRateMaster.CreatedDate = taxRate.CreatedDate;
                        taxRateMaster.ModifiedBy = taxRate.ModifiedBy;
                        taxRateMaster.ModifiedDate = taxRate.ModifiedDate;
                        taxRateMaster.Status = taxRate.Status;

                        taxRateMasters.Add(taxRateMaster);

                        
                    }

                    return taxRateMasters;
                }
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }
            return new ObservableCollection<TaxRateMaster>();
        }
        
        public static TaxRateMasterModel GetTaxRate(int taxRateId)
        {
            TaxRateMasterModel taxRateMaster = new TaxRateMasterModel();
            try
            {
                using (var db = new InvoiceEntities())
                {
                    var taxRate = db.TaxRateMasters.FirstOrDefault(x => x.TaxRateId == taxRateId && x.Status == 1 && x.CompanyId == Global.Instance.CompanyId);
                    taxRateMaster.TaxRateId = taxRate.TaxRateId;
                    taxRateMaster.Active = taxRate.Active;
                    taxRateMaster.TaxRate = taxRate.TaxRate;
                    taxRateMaster.CompanyId = taxRate.CompanyId;
                    taxRateMaster.CreatedBy = taxRate.CreatedBy;
                    taxRateMaster.CreatedDate = taxRate.CreatedDate;
                    taxRateMaster.ModifiedBy = taxRate.ModifiedBy;
                    taxRateMaster.ModifiedDate = taxRate.ModifiedDate;
                    taxRateMaster.Status = taxRate.Status;

                    return taxRateMaster;
                }
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
            }
            return taxRateMaster;
        }

        internal static bool CheckDuplicate(double? taxRate, double? oldTaxRate)
        {

            try
            {
                using (var db = new InvoiceEntities())
                {
                    return db.TaxRateMasters.Count(x => x.TaxRate == taxRate && x.TaxRate != oldTaxRate) > 0;
                }
            }
            catch (Exception ex)
            {
                new ErrorLogger.ErrLogger(ex);
                MessageBox.Show(GeneralMessages.ErrorLog);
                return false;
            }
        }

        internal static bool SaveData(TaxRateMasterModel objTaxRate, ref StringBuilder message, out EntityModel.TaxRateMaster temp)
        {
            if (objTaxRate != null)
            {
                TaxRateMaster taxRate = new TaxRateMaster();

                #region Update case
                if (objTaxRate.TaxRateId > 0)
                {
                    taxRate.TaxRateId = objTaxRate.TaxRateId;
                    taxRate.Active = objTaxRate.Active;
                    taxRate.TaxRate = objTaxRate.TaxRate;
                    taxRate.CompanyId = objTaxRate.CompanyId;
                    taxRate.CreatedBy = objTaxRate.CreatedBy;
                    taxRate.CreatedDate = objTaxRate.CreatedDate;
                    taxRate.ModifiedBy = objTaxRate.ModifiedBy;
                    taxRate.ModifiedDate = objTaxRate.ModifiedDate;
                    taxRate.Status = objTaxRate.Status;

                    #region DbTransaction
                    var uof = new FluentUnitOfWork(new InvoiceEntities());
                    try
                    {
                        bool Status =
                             uof.BeginTransaction()
                            .DoUpdate(taxRate)
                            .SaveAndContinue()
                            .EndTransaction();
                        if (Status)
                        {
                            temp = taxRate;
                            message.Append(MasterMessages.Updated);
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        uof.RollBack();
                        new ErrorLogger.ErrLogger(ex);
                        message.Append(GeneralMessages.ErrorLog);
                    }

                    #endregion
                }
                #endregion

                #region insert case
                else
                {
                    taxRate.TaxRateId = 0;
                    taxRate.Active = objTaxRate.Active;
                    taxRate.TaxRate = objTaxRate.TaxRate;
                    taxRate.CompanyId = objTaxRate.CompanyId;
                    taxRate.CreatedBy = objTaxRate.CreatedBy;
                    taxRate.CreatedDate = objTaxRate.CreatedDate;
                    taxRate.ModifiedBy = objTaxRate.ModifiedBy;
                    taxRate.ModifiedDate = objTaxRate.ModifiedDate;
                    taxRate.Status = objTaxRate.Status;


                    #region DbTransaction

                    var uof = new FluentUnitOfWork(new InvoiceEntities());
                    try
                    {

                        bool Status = uof.BeginTransaction()
                            .DoInsert(taxRate, out taxRate)
                            .SaveAndContinue()
                            .EndTransaction();

                        if (Status)
                        {
                            temp = taxRate;
                            message.Append(MasterMessages.Inserted);
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        uof.RollBack();
                        new ErrorLogger.ErrLogger(ex);
                        message.Append(GeneralMessages.ErrorLog);
                    }
                    #endregion
                }
                #endregion
            }
            temp = null;
            return false;
        }
    }
}
