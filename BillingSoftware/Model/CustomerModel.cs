﻿using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BillingSoftware.Model
{
    public class CustomerModel : ViewModelBase
    {
       
        public CustomerModel()
        {
            this.Orders = new HashSet<SalesOrderModel>();
            this.City = new CityModel();
        }


        private int _CustId;
        public int CustId
        {
            get { return _CustId; }
            set { _CustId = value; }
        }

        private string _CustCode;
        
        public string CustCode
        {
            get { return _CustCode; }
            set { _CustCode = value; }
        }

        private string _CustFName;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.CustFNameIsRequried))]
        public string CustFName
        {
            get { return _CustFName; }
            set { _CustFName = value; }
        }

        private string _CustMName;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.CustMNameIsRequried))]
        public string CustMName
        {
            get { return _CustMName; }
            set { _CustMName = value; }
        }

        private string _CustLName;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.CustLNameIsRequried))]
        public string CustLName
        {
            get { return _CustLName; }
            set { _CustLName = value; }
        }

        private bool? _Gender;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.CustGenderIsRequired))]
        public bool? Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }

        private string _EmailAddress;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.CustEmaiIsRequired))]
        public string EmailAddress
        {
            get { return _EmailAddress; }
            set { _EmailAddress = value; }
        }

        private string _MobileNo;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.CustPhoneRequired))]
        public string MobileNo
        {
            get { return _MobileNo; }
            set { _MobileNo = value; }
        }

        private string _Address;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.CustAddressRequired))]
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }

        private int? _CityId;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.CityNameIsRequired))]
        public int? CityId
        {
            get { return _CityId; }
            set { _CityId = value; }
        }


        private int? _StateId;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.StateNameIsRequired))]
        public int? StateId
        {
            get { return _StateId; }
            set { _StateId = value; }
        }

        private int? _CountryId;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.CountryNameIsRequired))]
        public int? CountryId
        {
            get { return _CountryId; }
            set { _CountryId = value; }
        }

        private int? _PostalCode;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.CustZipRequired))]
        public int? PostalCode
        {
            get { return _PostalCode; }
            set { _PostalCode = value; }
        }

        private string _OtherDetail;

        public string OtherDetail
        {
            get { return _OtherDetail; }
            set { _OtherDetail = value; }
        }
        
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? CompanyId { get; set; }
        public int? Status { get; set; }



        public virtual CityModel City { get; set; } 
        public virtual ICollection<SalesOrderModel> Orders { get; set; }
    }
}
