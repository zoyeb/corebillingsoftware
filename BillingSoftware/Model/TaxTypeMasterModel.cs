﻿using BillingSoftware.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingSoftware.Model
{
    public class TaxTypeMasterModel : ViewModelBase
    {
         
        public TaxTypeMasterModel()
        {
            this.TaxDetails = new HashSet<TaxDetail>();
        }

        private int _TaxTypeId;
        public int TaxTypeId
        {
            get { return _TaxTypeId; }
            set { _TaxTypeId = value; OnPropertyChanged("TaxTypeId"); }
        }

        private string _GstType;
        public string GstType
        {
            get { return _GstType; }
            set { _GstType = value; OnPropertyChanged("GstType"); }
        }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> CompanyId { get; set; }
        public Nullable<short> Status { get; set; }

      
        public virtual ICollection<TaxDetail> TaxDetails { get; set; }
    }
}
