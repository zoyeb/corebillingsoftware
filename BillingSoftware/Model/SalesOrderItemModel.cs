﻿using BillingSoftware.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingSoftware.Model
{
    public class SalesOrderItemModel : ViewModelBase
    {
        public SalesOrderItemModel()
        {
            this.SalesOrderTaxes = new HashSet<SalesOrderTaxModel>();
        }
        private bool _IsSelected;
        public bool IsSelected
        {
            get { return _IsSelected; }
            set { _IsSelected = value; OnPropertyChanged(nameof(IsSelected)); }
        }

        public int OrderItemId { get; set; }
        public Nullable<int> OrderId { get; set; }
        public Nullable<int> TaxHeaderId { get; set; }
        public Nullable<int> ProductId { get; set; }
        public Nullable<decimal> Qty { get; set; }
        public Nullable<decimal> Rate { get; set; }
        public Nullable<decimal> DiscPer { get; set; }
        public Nullable<decimal> DiscAmt { get; set; }
        public Nullable<decimal> FreightPer { get; set; }
        public Nullable<decimal> FreightAmt { get; set; }
        public Nullable<decimal> GrossAmount { get; set; }
        public Nullable<decimal> NetAmount { get; set; }
        public Nullable<decimal> TaxAmount { get; set; }
        public string OtherProductItemDetails { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> CompanyId { get; set; }
        public string LineStatus { get; set; }
        public Nullable<int> Status { get; set; }


     

        public virtual ProductModel Product { get; set; }
        public virtual TaxHeader TaxHeader { get; set; }
        public virtual SalesOrderItemModel SalesOrder { get; set; }        
        public virtual ICollection<SalesOrderTaxModel> SalesOrderTaxes { get; set; }
    }
}
