﻿using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingSoftware.Model
{
    public class SequenceModel : ViewModelBase
    {
        public int SeqId { get; set; }


        private Nullable<int> seqHeaderId;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.SequenceForRequried))]
        public Nullable<int> SeqHeaderId
        {
            get { return seqHeaderId; }
            set { seqHeaderId = value; OnPropertyChanged(nameof(SeqHeaderId)); }
        }

        public string Prefix;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.prefixRequried))]
        public string prefix
        {
            get { return Prefix; }
            set { Prefix = value; OnPropertyChanged(nameof(Prefix)); }
        }

        
        public Nullable<System.DateTime> _startDate;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.startDateNeeded))]
        public Nullable<System.DateTime> StartDate
        {
            get { return _startDate; }
            set { _startDate = value; OnPropertyChanged(nameof(StartDate)); }
        }

      
        public Nullable<System.DateTime> _endDate;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.EndDateIsNeeded))]
        public Nullable<System.DateTime> EndDate
        {
            get { return _endDate; }
            set { _endDate = value; OnPropertyChanged(nameof(EndDate)); }
        }

      
        public Nullable<int> startRangeNo;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.StartRangeNo))]
        public Nullable<int> StartRangeNo
        {
            get { return startRangeNo; }
            set { startRangeNo = value; OnPropertyChanged(nameof(StartRangeNo)); }
        }

        public Nullable<int> lastGeneratedNo;       
        public Nullable<int> LastGeneratedNo
        {
            get { return startRangeNo; }
            set { startRangeNo = value; OnPropertyChanged(nameof(LastGeneratedNo)); }
        }


        private Nullable<int> totalLength;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.TotalLengthIsRequried))]
        public Nullable<int> TotalLength
        {
            get { return totalLength; }
            set { totalLength = value; OnPropertyChanged(nameof(TotalLength)); }
        }

        private Nullable<bool> isActive;
        public Nullable<bool> IsActive
        {
            get { return isActive; }
            set { isActive = value; OnPropertyChanged(nameof(IsActive)); }
        }

        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> CompanyId { get; set; }
        public Nullable<short> Status { get; set; }

        public virtual SequenceHeaderModel SequenceHeader { get; set; }
    }
}
