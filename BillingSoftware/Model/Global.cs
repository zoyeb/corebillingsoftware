﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingSoftware.Model
{
    public class Global
    {
        public bool IsModified { get; set; }
        public static Global SInstance;
        public static readonly object SInstanceLock = new object();


        private int? _companyId;
        public int? CompanyId
        {
            get => _companyId ?? (_companyId = 1);
            set => _companyId = value;
        }


        private string _loginUserName;

        public string LoginUserName
        {
            get => _loginUserName ?? (_loginUserName = "zoyeb");
            set => _loginUserName = value;
        }

        public const string MasterModule = "Master";
        public const string TransactionModule = "Transaction";

        public const string CustomerSeqForm = "Customer";
        public const string OrderSeqForm = "Order";
        public const string InvoiceSeqForm = "Invoice";


        public const string Completed = "Completed";
        public const string confirmed = "confirmed";
        public const string Open = "Open";
        public const string Closed = "Closed";
        public const string Processed = "Processed";

        public enum Gender : short
        {
            Male=0,
            FeMale=1
        
        }

        public static Global Instance
        {
            get
            {
                lock (SInstanceLock)
                {
                    if (SInstance == null)
                    {
                        SInstance = new Global();
                    }
                }
                return SInstance;
            }
        }
    }
}
