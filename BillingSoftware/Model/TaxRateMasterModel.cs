﻿using BillingSoftware.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingSoftware.Model
{
    public class TaxRateMasterModel : ViewModelBase
    {
        
        public TaxRateMasterModel()
        {
            this.TaxDetails = new HashSet<TaxDetail>();
        }

        private int _TaxRateId;
        public int TaxRateId
        {
            get { return _TaxRateId; }
            set { _TaxRateId = value; OnPropertyChanged("TaxRateId"); }
        }
        public Nullable<double> _TaxRate;
        public Nullable<double> TaxRate
        {
            get { return _TaxRate; }
            set { _TaxRate = value; OnPropertyChanged("TaxRate"); }
        }
        
        private Nullable<bool> _Active;
        public Nullable<bool> Active
        {
            get { return _Active; }
            set { _Active = value; OnPropertyChanged(nameof(Active)); }
        }

        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> CompanyId { get; set; }
        public Nullable<short> Status { get; set; }
        
        public virtual ICollection<TaxDetail> TaxDetails { get; set; }
    }
}
