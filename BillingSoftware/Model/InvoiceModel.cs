﻿using BillingSoftware.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingSoftware.Model
{
    public class InvoiceModel : ViewModelBase
    {
         
        public InvoiceModel()
        {
            this.FinancialTransactions = new HashSet<FinancialTransaction>();
            this.InvoiceLineItems = new HashSet<InvoiceLineItem>();
        }

        private int _InvoiceId;
        public int InvoiceId
        {
            get { return _InvoiceId; }
            set { _InvoiceId = value; OnPropertyChanged("InvoiceId"); }
        }
        private string _InvoiceNumber;
        public string InvoiceNumber
        {
            get { return _InvoiceNumber; }
            set { _InvoiceNumber = value; OnPropertyChanged("InvoiceNumber"); }
        }
        private Nullable<System.DateTime> _InvoiceDate;
        public Nullable<System.DateTime> InvoiceDate
        {
            get { return _InvoiceDate; }
            set { _InvoiceDate = value; OnPropertyChanged("InvoiceDate"); }
        }

        private string _InvoiceDetails;
        public string InvoiceDetails
        {
            get { return _InvoiceDetails; }
            set { _InvoiceDetails = value; OnPropertyChanged("InvoiceDetails"); }
        }
        private Nullable<int> _OrderId;
        public Nullable<int> OrderId
        {
            get { return _OrderId; }
            set { _OrderId = value; OnPropertyChanged("OrderId"); }
        }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> CompanyId { get; set; }
        public Nullable<int> Status { get; set; }

        
        public virtual ICollection<FinancialTransaction> FinancialTransactions { get; set; }
         
        public virtual ICollection<InvoiceLineItem> InvoiceLineItems { get; set; }
    }
}
