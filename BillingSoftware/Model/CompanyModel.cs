﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingSoftware.Model
{
    class CompanyModel
    {
        
        public CompanyModel()
        {
            this.SalesOrderTaxes = new HashSet<SalesOrderTaxModel>();
        }

        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCode { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<short> Status { get; set; }

 
        public virtual ICollection<SalesOrderTaxModel> SalesOrderTaxes { get; set; }
    }
}
