﻿using BillingSoftware.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingSoftware.Model
{
    public class ProductStockModel : ViewModelBase
    {
        public int ProductStockId { get; set; }
        public Nullable<int> ProductId { get; set; }
        public Nullable<decimal> StockQty { get; set; }
        public Nullable<decimal> BalanceQty { get; set; }
        public Nullable<decimal> IssuedQty { get; set; }
        public Nullable<System.DateTime> StockDate { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<int> CompanyId { get; set; }
        public Nullable<int> Status { get; set; }

        
        public virtual Company Company { get; set; }
        public virtual Product Product { get; set; }
    }
}
