﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ResourceFiles.Properties;
using BillingSoftware.EntityModel;

namespace BillingSoftware.Model
{
    public class ProductModel : ViewModelBase
    {
        public ProductModel()
        {
            this.ProductSales = new HashSet<ProductSale>();
            this.ProductStocks = new HashSet<ProductStock>();
            this.SalesOrderItems = new HashSet<SalesOrderItem>();
        }
        
        private bool _IsSelected;
        public bool IsSelected
        {
            get { return _IsSelected; }
            set { _IsSelected = value; OnPropertyChanged(nameof(IsSelected)); }
        }


        public int ProductId { get; set; }

        private Nullable<int> _ProductionTypeId;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.ParentProductTypeName))]
        public Nullable<int> ProductionTypeId
        {
            get { return _ProductionTypeId; }
            set
            {
                _ProductionTypeId = value;
                OnPropertyChanged(nameof(ProductionTypeId));
            }
        }

        private string _ProductName;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.ProductCategoryNameReqired))]
        [StringLength(50)]
        public string ProductName
        {
            get { return _ProductName; }
            set { _ProductName = value; OnPropertyChanged(nameof(ProductName)); }
        }

        private Nullable<decimal> _ProductPrice;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.ProductPriceReqired))]
        public Nullable<decimal> ProductPrice
        {
            get { return _ProductPrice; }
            set { _ProductPrice = value; OnPropertyChanged(nameof(ProductPrice)); }
        }

        private Nullable<decimal> _ProductQuantity;
        //[Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.ProductPriceReqired))]
        public Nullable<decimal> ProductQuantity
        {
            get { return _ProductQuantity; }
            set { _ProductQuantity = value; OnPropertyChanged(nameof(ProductQuantity)); }
        }
        private Nullable<int> _OrderItemId;
       // [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.ParentProductTypeName))]
        public Nullable<int> OrderItemId
        {
            get { return _OrderItemId; }
            set
            {
                _OrderItemId = value;
                OnPropertyChanged(nameof(OrderItemId));
            }
        }


        #region Display Filed
        public decimal? AvailableStockQty { get; set; }
        #endregion

        public string ProductColor { get; set; }
        public string ProductSize { get; set; }
        public string ProductDetails { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> CompanyId { get; set; }

        private Nullable<bool> isGstApplicable;
        public Nullable<bool> IsGstApplicable
        {
            get { return isGstApplicable; }
            set { isGstApplicable = value; OnPropertyChanged(nameof(IsGstApplicable)); }
        }
        public Nullable<int> Status { get; set; }

        public virtual ProductType ProductType { get; set; }
 
        public virtual ICollection<ProductSale> ProductSales { get; set; }
 
        public virtual ICollection<ProductStock> ProductStocks { get; set; }
 
        public virtual ICollection<SalesOrderItem> SalesOrderItems { get; set; }
    }
}
