﻿using System;
using System.Collections.Generic;

namespace BillingSoftware.Model
{
    public class SequenceHeaderModel
    {
         
        public SequenceHeaderModel()
        {
            this.Sequences = new HashSet<SequenceModel>();
        }

        public int SeqHeaderId { get; set; }
        public string ModuleName { get; set; }
        public string SeqFormName { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> CompanyId { get; set; }
        public Nullable<short> Status { get; set; }
        
        public virtual ICollection<SequenceModel> Sequences { get; set; }
    }
}