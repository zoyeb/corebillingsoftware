﻿using BillingSoftware.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingSoftware.Model
{
   public class SalesOrderTaxModel
    {
        public int SOTaxId { get; set; }
        public Nullable<int> OrderItemId { get; set; }
        public Nullable<int> TaxDetailsId { get; set; }
        public Nullable<int> CompanyId { get; set; }
        public Nullable<decimal> TaxAmount { get; set; }
        public Nullable<decimal> TaxPercent { get; set; }
        public Nullable<decimal> CalculatedOn { get; set; }
        public string TaxTypeName { get; set; }
        public Nullable<decimal> TaxValue { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> Status { get; set; }

        public virtual Company Company { get; set; }
        public virtual SalesOrderItem SalesOrderItem { get; set; }
        public virtual TaxDetail TaxDetail { get; set; }
    }
}
