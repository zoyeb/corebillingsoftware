﻿using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BillingSoftware.Model
{
 
    public class CountryModel : ViewModelBase
    {
        private int _CountryId;
        public int CountryId
        {
            get { return _CountryId; }
            set { _CountryId = value; OnPropertyChanged("CountryId"); }
        }

        private string _CountryName;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.CountryNameIsRequired))]
        [StringLength(30)]        
        public string CountryName
        {
            get { return _CountryName; }
            set { _CountryName = value; OnPropertyChanged("CountryName"); }
        }

        private string _CountryCode;

        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.CountryCodeIsRequired))]
        public string CountryCode
        {
            get { return _CountryCode; }
            set { _CountryCode = value; OnPropertyChanged("CountryCode"); }
        }
        public Nullable<short> Status { get; set; }
        public Nullable<int> CompanyId { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
         

    }
}
