﻿using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingSoftware.Model
{
    public class StateModel : ViewModelBase
    {         
        public StateModel()
        {
            this.Cities = new HashSet<CityModel>();
        }

        public int StateId { get; set; }

        private string _StateName;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.StateNameIsRequired))]
        [StringLength(15)]
        public string StateName
        {
            get { return _StateName; }
            set { _StateName = value; OnPropertyChanged("StateName"); }
        }

        private Nullable<int> _CountryId;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.CountryNameIsRequired))]       
        public Nullable<int> CountryId
        {
            get { return _CountryId; }
            set { _CountryId = value; OnPropertyChanged("CountryId"); }
        }
      
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> CompanyId { get; set; }
        public Nullable<int> Status { get; set; }
       
        public virtual ICollection<CityModel> Cities { get; set; }
        public virtual CountryModel Country { get; set; }

    }
}
