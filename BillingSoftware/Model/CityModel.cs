﻿using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingSoftware.Model
{
    public class CityModel : ViewModelBase
    {
        public CityModel()
        {
            this.Cities = new HashSet<CityModel>();
        }

        public int CityId { get; set; }

        private string _CityName;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.CityNameIsRequired))]
        [StringLength(15)]
        public string CityName
        {
            get { return _CityName; }
            set { _CityName = value; OnPropertyChanged("CityName"); }
        }

        private Nullable<int> _StateId;
        [Required(ErrorMessageResourceType = typeof(MasterMessages), ErrorMessageResourceName = nameof(MasterMessages.StateNameIsRequired))]
        public Nullable<int> StateId
        {
            get { return _StateId; }
            set { _StateId = value;  }
        }


        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> CompanyId { get; set; }
        public Nullable<short> Status { get; set; }

        public virtual ICollection<CityModel> Cities { get; set; }
        public virtual StateModel State { get; set; }
        public virtual CountryModel Country { get; set; }
    }
}
