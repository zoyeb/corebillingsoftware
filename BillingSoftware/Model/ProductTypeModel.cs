﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ResourceFiles.Properties;

namespace BillingSoftware.Model
{
    public class ProductTypeModel : ViewModelBase
    {         
        public ProductTypeModel()
        {
            this.Products = new HashSet<ProductModel>();
        }

        public int ProductTypeId { get; set; }


        private string _ParentProductTypeName;
        [Required(ErrorMessageResourceType = typeof(MasterMessages),ErrorMessageResourceName = nameof(MasterMessages.ParentProductTypeName)), StringLength(50)]
        public string ParentProductTypeName
        {
            get => _ParentProductTypeName;
            set { _ParentProductTypeName = value; OnPropertyChanged("ParentProductTypeName"); }
        }

        private string _ProductTypeDescription; 
        [Required(ErrorMessageResourceType = typeof(MasterMessages),
             ErrorMessageResourceName = nameof(MasterMessages.ProductTypeDescription)), StringLength(maximumLength:500)]
        public string ProductTypeDescription
        {
            get => _ProductTypeDescription;
            set { _ProductTypeDescription = value; OnPropertyChanged("ProductTypeDescription"); }
        }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> CompanyId { get; set; }
        public Nullable<short> Status { get; set; }

        
        public virtual ICollection<ProductModel> Products { get; set; }
    }
}
