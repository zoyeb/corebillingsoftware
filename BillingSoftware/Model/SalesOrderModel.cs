﻿using System;
using System.Collections.Generic;

namespace BillingSoftware.Model
{
    public class SalesOrderModel : ViewModelBase
    {
        public SalesOrderModel()
        {
            this.OrderItems = new HashSet<SalesOrderItemModel>();
            this.Customer = new CustomerModel();
        }
        public int OrderId { get; set; }
        public string OrderNo { get; set; }
        public Nullable<System.DateTime> DateOrderPlaced { get; set; }
        public string OrderDescription { get; set; }
        public Nullable<decimal> NetAmount { get; set; }
        public Nullable<decimal> TaxAmount { get; set; }
        public Nullable<decimal> GrossAmont { get; set; }
        public Nullable<int> CustId { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> CompanyId { get; set; }
        public Nullable<int> Status { get; set; }
        public string SOHeaderStatus { get; set; }

        public virtual CustomerModel Customer { get; set; }
        public virtual ICollection<SalesOrderItemModel> OrderItems { get; set; }
    }
}