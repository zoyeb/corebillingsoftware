﻿using BillingSoftware.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingSoftware.Model
{
    public class TaxDetailModel : ViewModelBase
    {
        private int _TaxDetailsId;
        public int TaxDetailsId
        {
            get { return _TaxDetailsId; }
            set { _TaxDetailsId = value; OnPropertyChanged(nameof(TaxDetailsId)); }
        }

        private Nullable<int> _TaxHeaderId;
        public Nullable<int> TaxHeaderId
        {
            get { return _TaxHeaderId; }
            set { _TaxHeaderId = value; OnPropertyChanged(nameof(TaxHeaderId)); }
        }
        private Nullable<int> _TaxTypeId;
        public Nullable<int> TaxTypeId
        {
            get { return _TaxTypeId; }
            set { _TaxTypeId = value; OnPropertyChanged(nameof(TaxTypeId)); }
        }
        private Nullable<int> _TaxRateId;
        public Nullable<int> TaxRateId
        {
            get { return _TaxRateId; }
            set { _TaxRateId = value; OnPropertyChanged(nameof(TaxRateId)); }
        }


        private string _GstType;
        public string GstType
        {
            get { return _GstType; }
            set { _GstType = value; OnPropertyChanged(nameof(GstType)); }
        }


        public Nullable<double> _TaxRate;
        public Nullable<double> TaxRate
        {
            get { return _TaxRate; }
            set { _TaxRate = value; OnPropertyChanged(nameof(TaxRate)); }
        }


        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> CompanyId { get; set; }
        public Nullable<short> Status { get; set; }

        public virtual TaxHeader TaxHeader { get; set; }
        public virtual TaxRateMaster TaxRateMaster { get; set; }
        public virtual TaxTypeMaster TaxTypeMaster { get; set; }         
        public virtual ICollection<SalesOrderTax> SalesOrderTaxes { get; set; }
    }
}
