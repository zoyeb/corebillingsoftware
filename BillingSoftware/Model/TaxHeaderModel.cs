﻿using BillingSoftware.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingSoftware.Model
{
    public class TaxHeaderModel : ViewModelBase
    {
        public TaxHeaderModel()
        {
            this.TaxDetails = new HashSet<TaxDetailModel>();
        }

        private int _TaxHeaderId;
        public int TaxHeaderId
        {
            get { return _TaxHeaderId; }
            set { _TaxHeaderId = value; OnPropertyChanged("TaxHeaderId"); }
        }
        private string _TaxName;
        public string TaxName
        {
            get { return _TaxName; }
            set { _TaxName = value; OnPropertyChanged("TaxName"); }
        }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> CompanyId { get; set; }

        
        public virtual ICollection<TaxDetailModel> TaxDetails { get; set; }
    }
}
