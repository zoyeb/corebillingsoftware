﻿using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Utilities;

namespace BillingSoftware.ErrorLogger
{
   public class ErrLogger : ExceptionLogger
    {                
        public ErrLogger(Exception ex)
        {
            var textFileLogger = new TextFileLogger();            
            this.AddLogger(textFileLogger);
            this.LogException(ex);
            textFileLogger.LogError(ex.ToString());
            MessageBoxResult result = MessageBox.Show(GeneralMessages.ErrorLog, "Confirmation",MessageBoxButton.OK,MessageBoxImage.Asterisk);
        }

    }
}
