﻿using BillingSoftware.Model;
using BillingSoftware.UI;
using BillingSoftware.UI.Masters;
using BillingSoftware.UI.SplashScreen;
using BillingSoftware.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using BillingSoftware.UI.Masters.Sequence;

namespace BillingSoftware
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static readonly object obj = new object();
        protected override void OnStartup(StartupEventArgs e)
        {            
            if (ConfigurationManager.AppSettings["ShowSplash"] == "1")
            {
                //create an main window object
                var window = new MainWindow();
                //hide main window
                window.Hide();                

                SplashWindow splash;
                object ThreadLocker = obj;
                //create thread and lock splash screen until it perform it's task
                Monitor.Enter(ThreadLocker);
                try
                {
                    splash = new SplashWindow();
                    splash.ShowDialog();
                }
                finally
                {
                    Monitor.Exit(ThreadLocker);
                }
                //set data context                
                var mainWindow = new MainWindowViewModel();
                window.DataContext = mainWindow;
                //show again main window
                window.Show();
                //close Spplas screen after finish it
                splash.Close();
                base.OnStartup(e);
            }
            else
            {
                //create an main window object
                var window = new MainWindow();
                //set data context                
                var mainWindow = new MainWindowViewModel();
                window.DataContext = mainWindow;
                //show main window
                window.Show();
                base.OnStartup(e);
            }
            
        }
    }
}
