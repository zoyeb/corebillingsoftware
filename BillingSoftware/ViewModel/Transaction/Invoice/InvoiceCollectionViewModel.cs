﻿using BillingSoftware.Command;
using BillingSoftware.HelperFunction;
using BillingSoftware.Manager;
using BillingSoftware.Model;
using BillingSoftware.UI.Transaction.Invoice;
using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BillingSoftware.ViewModel.Transaction.Invoice
{
    class InvoiceCollectionViewModel : NotifyPropertyChangedBase
    {
        #region Command Declaration
        private readonly DelegateCommand<object> newButtonCommand;
        public DelegateCommand<object> NewButtonCommand
        {
            get { return newButtonCommand; }
        }

        private DelegateCommand<object> closeButtonCommand;
        public DelegateCommand<object> CloseButtonCommand
        {
            get { return closeButtonCommand; }
        }

        private DelegateCommand<object> selectedOrder;
        public DelegateCommand<object> SelectedOrder
        {
            get { return selectedOrder; }
        }
        private DelegateCommand<object> deleteButtonCommand;
        public DelegateCommand<object> DeleteButtonCommand
        {
            get { return deleteButtonCommand; }
        }
        private DelegateCommand<object> reportButtonCommand;
        public DelegateCommand<object> ReportButtonCommand
        {
            get { return reportButtonCommand; }
        }
        #endregion Command Declaration

        #region Constructor
        public InvoiceCollectionViewModel()
        {
            Predicate<object> canEdit = delegate (object obj) { if ((EntityModel.SalesOrder)obj != null) return true; else return false; };

            newButtonCommand = new DelegateCommand<object>((e) => { OnAddNewButtonCommand(); }, (e) => { return true; });
        }
        #endregion Constructor

        private void OnAddNewButtonCommand()
        {
            if (!Helpers.IsWindowOpen<InvoiceView>())
            {
                var view = new InvoiceView();
                var viewModel = new InvoiceViewModel();
                view.DataContext = viewModel;
                view.Closed += RefreshGridOnClosed;
                view.Show();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        private void RefreshGridOnClosed(object sender, EventArgs e)
        {

        }

        private void SetDefaultValues()
        {
            Invoices = InvoiceManager.GetInvoices();
        }



        #region Properties
        private EntityModel.Invoice _Invoices;
        public EntityModel.Invoice Invoices
        {
            get { return _Invoices; }
            set { _Invoices = value; }
        }
        #endregion
    }

}

