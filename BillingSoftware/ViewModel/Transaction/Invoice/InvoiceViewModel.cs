﻿using BillingSoftware.Command;
using BillingSoftware.Manager;
using BillingSoftware.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BillingSoftware.ViewModel.Transaction.Invoice
{
    class InvoiceViewModel : NotifyPropertyChangedBase
    {

        private DelegateCommand<object> orderNumberSelection;
        public DelegateCommand<object> OrderNumberSelection
        {
            get { return orderNumberSelection; }
        }

        private DelegateCommand<object> orderItemsSelectionCommand;
        public DelegateCommand<object> OrderItemsSelectionCommand
        {
            get { return orderItemsSelectionCommand; }
        }


        private DelegateCommand<object> taxGroupSelection;
        public DelegateCommand<object> TaxGroupSelection
        {
            get { return taxGroupSelection; }
        }

        public InvoiceViewModel()
        {
            Init();
            SetDefaultvalue();
        }

        private void SetDefaultvalue()
        {
            OrderColl = SalesOrderManager.GetOrders();
            TaxHeaders = TaxDetailsManager.GetTaxHeaders();
        }

        private void Init()
        {
            objSelectedItem = new EntityModel.SalesOrderItem();
            objOrder = new EntityModel.SalesOrder();
            orderNumberSelection = new DelegateCommand<object>((obj) => { OnOrderNumberSelection(obj); });
            taxGroupSelection = new DelegateCommand<object>((obj) => OntaxGroupSelection(obj));
            orderItemsSelectionCommand = new DelegateCommand<object>((obj) => OnOrderItemsSelection(obj));
        }

        private void OnOrderItemsSelection(object obj)
        {
            var _obj = obj as EntityModel.SalesOrderItem;
            if(_obj != null)
            {
                objSelectedItem.Qty = _obj.Qty;
                objSelectedItem.OtherProductItemDetails = _obj.OtherProductItemDetails;
            }
            OnPropertyChanged(() => objSelectedItem);
        }

        private void OntaxGroupSelection(object obj)
        {
            var _obj = obj as EntityModel.TaxHeader;
            if (_obj != null)
            {

            }
        }

         
        private void OnOrderNumberSelection(object obj)
        {
            var _obj = obj as EntityModel.SalesOrder;
            if (_obj != null)
            {
                CustomerCode = _obj.Customer.CustCode;
                CustomerName = string.Format("{0}-({1})", _obj.Customer?.CustFName ?? "", _obj.Customer?.City?.CityName ?? "");
                objOrder.DateOrderPlaced = _obj.DateOrderPlaced;
                objOrder.OrderDescription = _obj.OrderDescription;
                objOrder.CreatedBy = _obj.CreatedBy;
                OnPropertyChanged(() => objOrder);
                if (_obj.SalesOrderItems.Count > 0)
                {
                    OrderItems = new ObservableCollection<EntityModel.SalesOrderItem>(_obj.SalesOrderItems);
                    OnPropertyChanged(() => OrderItems);
                }

            }
        }

        private ObservableCollection<EntityModel.SalesOrder> _OrderColl;
        public ObservableCollection<EntityModel.SalesOrder> OrderColl
        {
            get { return _OrderColl; }
            set { _OrderColl = value; OnPropertyChanged(() => OrderColl); }
        }

        private string _CustomerCode;
        public string CustomerCode
        {
            get { return _CustomerCode; }
            set { _CustomerCode = value; OnPropertyChanged(() => CustomerCode); }
        }

        private string _CustomerName;
        public string CustomerName
        {
            get { return _CustomerName; }
            set { _CustomerName = value; OnPropertyChanged(() => CustomerName); }
        }

        private EntityModel.SalesOrder _objOrder;
        public EntityModel.SalesOrder objOrder
        {
            get { return _objOrder; }
            set { _objOrder = value; OnPropertyChanged(() => objOrder); }
        }

        private ObservableCollection<EntityModel.TaxHeader> _TaxHeaders;
        public ObservableCollection<EntityModel.TaxHeader> TaxHeaders
        {
            get { return _TaxHeaders; }
            set { _TaxHeaders = value; OnPropertyChanged(() => TaxHeaders); }
        }


        private ObservableCollection<EntityModel.SalesOrderItem> _OrderItems;
        public ObservableCollection<EntityModel.SalesOrderItem> OrderItems
        {
            get { return _OrderItems; }
            set { _OrderItems = value; OnPropertyChanged(() => objOrder); }
        }

        private EntityModel.SalesOrderItem _objSelectedItem;
        public EntityModel.SalesOrderItem objSelectedItem
        {
            get { return _objSelectedItem; }
            set { _objSelectedItem = value; OnPropertyChanged(() => objSelectedItem); }
        }
    }
}
