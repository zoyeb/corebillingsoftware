﻿using BillingSoftware.Command;
using BillingSoftware.HelperFunction;
using BillingSoftware.Manager;
using BillingSoftware.Model;
using BillingSoftware.UI.Masters.Customer;
using BillingSoftware.UI.Transaction.SalesOrder;
using BillingSoftware.ViewModel.Masters.Customer;
using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BillingSoftware.ViewModel.Transaction.SalesOrder
{
    public class SalesOrderViewModel : NotifyPropertyChangedBase
    {
        #region Declartion

        #region  LineCommand
        public DelegateCommand<object> SaveLineCommand { get; private set; }
        public DelegateCommand<object> EditLineCommand { get; private set; }
        public DelegateCommand<object> CancelLineCommand { get; private set; }
        public DelegateCommand<object> RemoveLineCommand { get; private set; }
        #endregion

        public DelegateCommand<object> SaveCommand { get; private set; }
        public DelegateCommand<object> EditCommand { get; private set; }
        public DelegateCommand<object> SaveAndNewCommand { get; private set; }
        public DelegateCommand<object> CloseButtonCommand { get; private set; }
        public DelegateCommand<object> SaveAndCloseCommand { get; private set; }
        public DelegateCommand<object> SelectItemButtonCommand { get; private set; }
        public DelegateCommand<object> SelectSoLine { get; private set; }
        public DelegateCommand<object> SelectCustomerButtonCommand { get; private set; }
        public DelegateCommand<object> SelecteAllProduct { get; private set; }
        public DelegateCommand<object> CustomerSelectCommand { get; private set; }
        public DelegateCommand<object> ConfirmedButtonCommand { get; private set; }


        #endregion

        #region Constructor
        public SalesOrderViewModel()
        {
            Init();
            DisableEditMode();
        }

        public SalesOrderViewModel(int orderId, bool IsEditMode)
        {
            Init();
            this.IsEditMode = IsEditMode;
            GetOrderDetails(orderId);
            EnableEditMode();
        }

        private void GetOrderDetails(int orderId)
        {
            Init();
            objOrder = SalesOrderManager.GetOrdersById(orderId);
            if (objOrder.Customer?.City != null)
                CustomerName = string.Format("{0},{1} - ({2})", objOrder.Customer.CustFName, objOrder.Customer.CustLName, objOrder.Customer.City.CityName);
            else
                CustomerName = string.Empty;

            foreach (SalesOrderItemModel ordItm in objOrder.OrderItems)
            {
                SalesOrderLineColl.Add(ordItm);
                DataForSaveSoLine.Add(ordItm);
            }
            OnPropertyChanged(() => SalesOrderLineColl);
            OnPropertyChanged(() => DataForSaveSoLine);
        }
        #endregion

        #region Methods
        private void Init()
        {
            SelectMsg = "Select All";
            DataForSaveSoLine = new ObservableCollection<SalesOrderItemModel>();
            ObjSOLine = new SalesOrderItemModel { CompanyId = Global.Instance.CompanyId, CreatedBy = Global.Instance.LoginUserName, Status = 1, LineStatus = "Open" };
            objOrder = new SalesOrderModel { CompanyId = Global.Instance.CompanyId, Status = 1, CreatedBy = Global.Instance.LoginUserName, CreatedDate = DateTime.Now, SOHeaderStatus = "Open" };
            SalesOrderLineColl = new ObservableCollection<SalesOrderItemModel>();
            EditCommand = new DelegateCommand<object>((s) => { OnEditCommand(); }, (s) => { return true; });
            SaveCommand = new DelegateCommand<object>((a) => { OnSaveData(); });
            SaveAndNewCommand = new DelegateCommand<object>((a) => { OnSaveAndNewCommand(); });
            CloseButtonCommand = new DelegateCommand<object>((obj) => { OnClose(obj); });
            SaveAndCloseCommand = new DelegateCommand<object>((obj) => { OnsaveAndCloseCommand(obj); });
            SelectItemButtonCommand = new DelegateCommand<object>((obj) => { OnSelectItemButtonCommand(obj); });
            SelectCustomerButtonCommand = new DelegateCommand<object>((obj) => { OnSelectCustomerButtonCommand(obj); });
            SelectSoLine = new DelegateCommand<object>((obj) => { OnSelectSoLine(obj); });
            SelecteAllProduct = new DelegateCommand<object>((obj) => { OnSelectAll(obj); });
            CustomerSelectCommand = new DelegateCommand<object>((obj) => { OnCustomerSeletion(obj); });
            Predicate<object> canSelectOrder = delegate (object s) { if (((BillingSoftware.Model.SalesOrderItemModel)s) != null) { return true; } else { return false; } };
            SaveLineCommand = new DelegateCommand<object>((obj) => { OnSaveLineCommand(); }, (obj) => { return true; });
            EditLineCommand = new DelegateCommand<object>((obj) => { OnEditLineCommand(obj); }, canSelectOrder);
            CancelLineCommand = new DelegateCommand<object>((obj) => { OnCancelLineCommand(obj); }, canSelectOrder);
            RemoveLineCommand = new DelegateCommand<object>((obj) => { OnRemoveLineCommand(obj); });
            ConfirmedButtonCommand = new DelegateCommand<object>((obj) => { OnConfirmedSO(obj); });
            SOTaxes = new List<SalesOrderTaxModel>();
            SetDefaultValues();
        }



        private void OnCustomerSeletion(object obj)
        {
            if (obj != null)
            {
                if (obj is EntityModel.Customer)
                {
                    var _obj = obj as EntityModel.Customer;
                    CustomerName = string.Format("{0},{1} - ({2})", _obj.CustFName, _obj.CustLName, _obj.City.CityName);
                }
            }
        }


        private void CalculateLineTax()
        {
            List<SalesOrderTaxModel> SOTaxes = new List<SalesOrderTaxModel>();
            decimal TaxAmount = 0;

            if (SOLineQty.HasValue && SOLineRate.HasValue && SOLineNetAmount.HasValue)
            {
                if (ObjSOLine.TaxHeader != null)
                {
                    foreach (var item in ObjSOLine.TaxHeader.TaxDetails)
                    {
                        SalesOrderTaxModel SOTax = new SalesOrderTaxModel();

                        var taxRate = item.TaxRateMaster;
                        var TaxType = item.TaxTypeMaster;

                        SOTax.SOTaxId = 0;
                        SOTax.TaxDetail = item;
                        SOTax.TaxDetailsId = item.TaxDetailsId;
                        SOTax.OrderItemId = ObjSOLine.OrderItemId;
                        SOTax.CalculatedOn = SOLineNetAmount;
                        SOTax.TaxTypeName = TaxType.GstType;
                        SOTax.TaxAmount = SOLineNetAmount * Convert.ToDecimal(taxRate.TaxRate) / 100;
                        SOTax.TaxPercent = Convert.ToDecimal(taxRate.TaxRate);
                        SOTax.OrderItemId = ObjSOLine.OrderItemId;
                        SOTax.CreatedBy = Global.Instance.LoginUserName;
                        SOTax.CreatedDate = DateTime.Now;
                        SOTax.ModifiedBy = Global.Instance.LoginUserName;
                        SOTax.Status = 1;
                        SOTax.CompanyId = Global.Instance.CompanyId;

                        TaxAmount = TaxAmount + SOTax.TaxAmount.Value;

                        SOTaxes.Add(SOTax);
                    }

                    SOLineTaxAmount = TaxAmount;
                    ObjSOLine.TaxAmount = TaxAmount;
                    ObjSOLine.GrossAmount = ObjSOLine.NetAmount + TaxAmount;
                    SOLineGrossAmount = ObjSOLine.NetAmount + TaxAmount;

                    foreach (var item in ObjSOLine.SalesOrderTaxes.ToList())
                    {
                        if (item.SOTaxId > 0)
                        {
                            item.Status = 0;
                        }
                        else
                        {
                            ObjSOLine.SalesOrderTaxes.Remove(item);
                        }


                    }
                    foreach (var item in SOTaxes)
                    {
                        ObjSOLine.SalesOrderTaxes.Add(item);
                    }
                    //ObjSOLine.SalesOrderTaxes= SOTaxes;

                    this.SOTaxes = SOTaxes;
                }
            }
        }

        private void UpdateLineTax()
        {
            SalesOrderItemModel salesOrderItem;

            if (ObjSOLine.OrderItemId > 0)
            {
                salesOrderItem = DataForSaveSoLine.FirstOrDefault(x => x.OrderItemId == ObjSOLine.OrderItemId);
            }
            else
            {
                salesOrderItem = DataForSaveSoLine.FirstOrDefault(x => x.CreatedDate == ObjSOLine.CreatedDate);
            }

            List<SalesOrderTaxModel> SOTaxes = new List<SalesOrderTaxModel>();
            decimal TaxAmount = 0;

            if (salesOrderItem != null)
            {
                foreach (var item in salesOrderItem.SalesOrderTaxes)
                {
                    if (ObjSOLine.TaxHeader != null)
                    {
                        if (SOLineQty.HasValue && SOLineRate.HasValue && SOLineNetAmount.HasValue)
                        {
                            SalesOrderTaxModel SOTax = new SalesOrderTaxModel();

                            var taxRate = item.TaxDetail.TaxRateMaster;
                            var TaxType = item.TaxDetail.TaxTypeMaster;

                            SOTax.TaxDetail = item.TaxDetail;
                            SOTax.SOTaxId = item.SOTaxId;
                            SOTax.TaxDetailsId = item.TaxDetailsId;
                            SOTax.OrderItemId = ObjSOLine.OrderItemId;
                            SOTax.CalculatedOn = SOLineNetAmount;
                            SOTax.TaxTypeName = TaxType.GstType;
                            SOTax.TaxAmount = SOLineNetAmount * Convert.ToDecimal(taxRate.TaxRate) / 100;
                            SOTax.TaxPercent = Convert.ToDecimal(taxRate.TaxRate);
                            SOTax.OrderItemId = ObjSOLine.OrderItemId;
                            SOTax.CreatedBy = item.CreatedBy;
                            SOTax.CreatedDate = item.CreatedDate;
                            SOTax.ModifiedBy = item.ModifiedBy;
                            SOTax.Status = item.Status;
                            SOTax.CompanyId = item.CompanyId;

                            TaxAmount = TaxAmount + SOTax.TaxAmount.Value;

                            SOTaxes.Add(SOTax);
                        }
                    }


                }
                SOLineTaxAmount = TaxAmount;
                ObjSOLine.TaxAmount = TaxAmount;
                ObjSOLine.GrossAmount = ObjSOLine.NetAmount + TaxAmount;
                SOLineGrossAmount = ObjSOLine.NetAmount + TaxAmount;
                ObjSOLine.SalesOrderTaxes = SOTaxes;
                this.SOTaxes = SOTaxes;
            }

        }



        private void SetItemNetProperties()
        {
            if (IsEnableSaveLine)
            {
                if (SOLineQty.HasValue && SOLineRate.HasValue)
                {
                    decimal? itemNet = 0;
                    decimal? tempItemNet = 0;

                    if (SOLineDiscPer.HasValue && SOLineDiscPer.GetValueOrDefault(0) != 0)
                    {
                        itemNet = (SOLineQty.Value * SOLineRate.Value) - ((SOLineQty.Value * SOLineRate.Value) * SOLineDiscPer / 100);
                    }
                    else if (SOLineDiscAmt.HasValue && SOLineDiscAmt.GetValueOrDefault(0) != 0)
                    {
                        itemNet = (SOLineQty.Value * SOLineRate.Value) - SOLineDiscAmt.Value;
                    }
                    else
                    {
                        itemNet = (SOLineQty.Value * SOLineRate.Value);
                    }

                    tempItemNet = itemNet;



                    if (SOLineFreightPer.HasValue && SOLineFreightPer.GetValueOrDefault(0) != 0)
                    {
                        itemNet = itemNet - (tempItemNet * SOLineFreightPer.Value / 100);
                    }
                    else if (SOLineFreightAmt.HasValue && SOLineFreightAmt.GetValueOrDefault(0) != 0)
                    {
                        itemNet = tempItemNet - SOLineFreightAmt.Value;
                    }


                    SOLineNetAmount = itemNet;
                    tempItemNet = itemNet;
                }
            }
        }


        private void OnSelectCustomerButtonCommand(object obj)
        {
            if (obj != null)
            {
                if (obj is EntityModel.Customer)
                {
                    var _obj = obj as EntityModel.Customer;
                    if (!Helpers.IsWindowOpen<CustomerView>())
                    {
                        var view = new CustomerView();
                        var viewModel = new CustomerViewModel(_obj.CustId, IsEditMode: true, ViewOnlyMode: true);
                        view.DataContext = viewModel;
                        view.Show();
                    }
                    else
                    {
                        MessageBox.Show(GeneralMessages.WindowIsOpened);
                    }
                }
            }
            else
            {
                MessageBoxResult result = MessageBox.Show("Select customer to see Customer record", "", MessageBoxButton.OK, MessageBoxImage.Asterisk, MessageBoxResult.OK);

            }
        }


        #region line Commnad Function
        private void OnNewLineCommand()
        {
            //is not being used
        }
        private void OnSaveLineCommand()
        {
            if (ValidateBeforeLineSave())
            {

                SalesOrderItemModel temp = new SalesOrderItemModel();

                oldTaxHeaderId = null;
                temp.OrderId = ObjSOLine.OrderId;
                temp.OrderItemId = ObjSOLine.OrderItemId;
                temp.CompanyId = ObjSOLine.CompanyId;
                temp.Product = ObjSOLine.Product;
                temp.CreatedBy = ObjSOLine.CreatedBy;
                temp.CreatedDate = ObjSOLine.CreatedDate;
                temp.IsSelected = ObjSOLine.IsSelected;
                temp.ModifiedBy = ObjSOLine.ModifiedBy;
                temp.ModifiedDate = ObjSOLine.ModifiedDate;
                temp.ProductId = ObjSOLine.ProductId;
                temp.Status = ObjSOLine.Status;
                temp.DiscAmt = SOLineDiscAmt;
                temp.DiscPer = SOLineDiscPer;
                temp.FreightAmt = SOLineFreightAmt;
                temp.FreightPer = SOLineFreightPer;
                temp.Qty = SOLineQty;
                temp.Rate = SOLineRate;
                temp.LineStatus = ObjSOLine.LineStatus;
                temp.NetAmount = SOLineNetAmount;
                temp.TaxAmount = SOLineTaxAmount;
                temp.GrossAmount = SOLineGrossAmount;


                temp.TaxHeaderId = ObjSOLine.TaxHeaderId;
                temp.TaxHeader = ObjSOLine.TaxHeader;

                temp.SalesOrderTaxes = ObjSOLine.SalesOrderTaxes;



                if (temp.OrderItemId > 0)
                {
                    SalesOrderLineColl.Remove(SalesOrderLineColl.FirstOrDefault(x => x.OrderItemId == temp.OrderItemId));
                    DataForSaveSoLine.Remove(DataForSaveSoLine.FirstOrDefault(x => x.OrderItemId == temp.OrderItemId));

                    SalesOrderLineColl.Add(temp);
                    DataForSaveSoLine.Add(temp);
                }
                else
                {
                    SalesOrderLineColl.Remove(SalesOrderLineColl.FirstOrDefault(x => x.CreatedDate == temp.CreatedDate));
                    DataForSaveSoLine.Remove(DataForSaveSoLine.FirstOrDefault(x => x.CreatedDate == temp.CreatedDate));

                    SalesOrderLineColl.Add(temp);
                    DataForSaveSoLine.Add(temp);

                }

                CalculateHeaderAmount();
                OnPropertyChanged(() => SalesOrderLineColl);
                OnPropertyChanged(() => DataForSaveSoLine);
                IsEnableSaveLine = false;


                if (SalesOrderLineColl.Count > 0)
                    IsEnableEditLine = true;
                else
                    IsEnableEditLine = false;



                IsEnableEntryLine = false;
                IsEnableSOLineGrid = true;
                IsEnableEditableFileds = false;
                IsEnableCancelLine = false;
                IsEnableRemoveLine = true;
                IsEnableSelectAll = true;
            }

        }


        private void OnEditLineCommand(object obj)
        {

            if (obj != null)
            {
                var _obj = obj as BillingSoftware.Model.SalesOrderItemModel;
                if (_obj != null)
                {
                    ObjSOLine = new SalesOrderItemModel();

                    ObjSOLine.OrderItemId = _obj.OrderItemId;
                    ObjSOLine.CompanyId = _obj.CompanyId;
                    ObjSOLine.OrderId = _obj.OrderId;
                    ObjSOLine.Product = _obj.Product;
                    ObjSOLine.CreatedBy = _obj.CreatedBy;
                    ObjSOLine.CreatedDate = _obj.CreatedDate;
                    ObjSOLine.IsSelected = _obj.IsSelected;
                    ObjSOLine.ModifiedBy = _obj.ModifiedBy;
                    ObjSOLine.ModifiedDate = _obj.ModifiedDate;
                    ObjSOLine.ProductId = _obj.ProductId;
                    ObjSOLine.Status = _obj.Status;
                    ObjSOLine.TaxHeader = _obj.TaxHeader;
                    ObjSOLine.SalesOrderTaxes = _obj.SalesOrderTaxes;
                    ObjSOLine.TaxHeaderId = _obj.TaxHeaderId;
                    ObjSOLine.LineStatus = _obj.LineStatus;

                    SOLineDiscAmt = _obj.DiscAmt;
                    SOLineDiscPer = _obj.DiscPer;
                    SOLineFreightAmt = _obj.FreightAmt;
                    SOLineFreightPer = _obj.FreightPer;
                    SOLineQty = _obj.Qty;
                    SOLineRate = _obj.Rate;
                    SOLineNetAmount = _obj.NetAmount;
                    SOLineTaxAmount = _obj.TaxAmount;
                    SOLineGrossAmount = _obj.GrossAmount;



                    TaxHeaderId = _obj.TaxHeaderId;
                    IsEnabledTaxHeader = ObjSOLine.OrderItemId > 0 ? false : true;

                    IsEnableEntryLine = true;
                    IsEnableSOLineGrid = false;
                    IsEnableEditLine = false;
                    IsEnableCancelLine = true;
                    IsEnableSaveLine = true;
                    IsEnableRemoveLine = false;
                    IsEnableSelectAll = false;
                    IsEnableEditableFileds = true;
                }
                else
                {
                    ObjSOLine = new SalesOrderItemModel();
                }
                OnPropertyChanged(() => ObjSOLine);
            }
        }
        private void OnCancelLineCommand(object obj)
        {
            if (obj != null)
            {
                var _obj = obj as BillingSoftware.Model.SalesOrderItemModel;
                if (_obj != null)
                {
                    ObjSOLine = new SalesOrderItemModel();

                    ObjSOLine.OrderId = _obj.OrderId;
                    ObjSOLine.OrderItemId = _obj.OrderItemId;
                    ObjSOLine.CompanyId = _obj.CompanyId;
                    ObjSOLine.Product = _obj.Product;
                    ObjSOLine.CreatedBy = _obj.CreatedBy;
                    ObjSOLine.CreatedDate = _obj.CreatedDate;
                    ObjSOLine.IsSelected = _obj.IsSelected;
                    ObjSOLine.ModifiedBy = _obj.ModifiedBy;
                    ObjSOLine.ModifiedDate = _obj.ModifiedDate;
                    ObjSOLine.ProductId = _obj.ProductId;
                    ObjSOLine.Status = _obj.Status;
                    ObjSOLine.LineStatus = _obj.LineStatus;
                    SOLineDiscAmt = _obj.DiscAmt;
                    SOLineDiscPer = _obj.DiscPer;
                    SOLineFreightAmt = _obj.FreightAmt;
                    SOLineFreightPer = _obj.FreightPer;
                    SOLineQty = _obj.Qty;
                    SOLineRate = _obj.Rate;
                    SOLineNetAmount = _obj.NetAmount;
                    SOLineTaxAmount = _obj.TaxAmount;
                    SOLineGrossAmount = _obj.GrossAmount;

                    ObjSOLine.TaxHeader = _obj.TaxHeader;
                    ObjSOLine.SalesOrderTaxes = _obj.SalesOrderTaxes;
                    ObjSOLine.TaxHeaderId = _obj.TaxHeaderId;


                    IsEnableEntryLine = false;
                    IsEnableSOLineGrid = true;
                    IsEnableEditLine = SalesOrderLineColl.Count > 0;
                    IsEnableCancelLine = false;
                    IsEnableSaveLine = false;
                    IsEnableEditableFileds = false;
                    IsEnableRemoveLine = true;
                    IsEnableSelectAll = true;
                }
                else
                {
                    ObjSOLine = new SalesOrderItemModel();
                }
                OnPropertyChanged(() => ObjSOLine);
            }
        }

        private void CalculateHeaderAmount()
        {
            decimal GrossAmt = 0, TaxTamt = 0, NetAmt = 0;
            foreach (var item in SalesOrderLineColl)
            {
                if (item.GrossAmount.HasValue)
                    GrossAmt = GrossAmt + item.GrossAmount.Value;
                if (item.TaxAmount.HasValue)
                    TaxTamt = TaxTamt + item.TaxAmount.Value;
                if (item.NetAmount.HasValue)
                    NetAmt = NetAmt + item.NetAmount.Value;
            }

            objOrder.GrossAmont = GrossAmt;
            objOrder.TaxAmount = TaxTamt;
            objOrder.NetAmount = NetAmt;

            OnPropertyChanged(() => objOrder);

        }

        private void OnRemoveLineCommand(object obj)
        {
            if (SalesOrderLineColl.Count > 0)
            {
                if (SalesOrderLineColl.Any(x => x.IsSelected))
                {
                    MessageBoxResult result = MessageBox.Show(MasterMessages.RemoveRecords, "", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.OK);
                    if (result == MessageBoxResult.No) return;
                    foreach (var item in SalesOrderLineColl.Where(x => x.IsSelected == true).ToList())
                    {
                        var selectedProduct = SalesOrderLineColl.FirstOrDefault(x => x.IsSelected == true);

                        if (item.OrderItemId > 0)
                        {
                            selectedProduct.Status = 0;
                            SalesOrderLineColl.Remove(selectedProduct);
                            DataForSaveSoLine.FirstOrDefault(x => x.IsSelected == true && x.OrderItemId == selectedProduct.OrderItemId).Status = 0;
                        }
                        else
                        {
                            SalesOrderLineColl.Remove(selectedProduct);
                            DataForSaveSoLine.Remove(selectedProduct);
                        }

                    }
                }
                else
                {
                    MessageBox.Show(TransactionMessages.SelectAnyRecord, "", MessageBoxButton.OK, MessageBoxImage.Asterisk, MessageBoxResult.OK);
                }

                IsEnableRemoveLine = true;

            }
            CalculateHeaderAmount();
            IsEnableEntryLine = false;
            IsEnableSOLineGrid = true;
            IsEnableEditableFileds = false;
            IsEnableRemoveLine = !(SalesOrderLineColl.Count == 0);
            IsEnableSelectAll = SalesOrderLineColl.Count > 0;
        }
        #endregion


        private void OnSelectSoLine(object obj)
        {
            var IsModified = Global.Instance.IsModified;

            if (IsEditMode || IsEnableEditLine)
            {
                if (obj != null)
                {

                    var _obj = obj as BillingSoftware.Model.SalesOrderItemModel;
                    if (_obj != null)
                    {
                        TaxHeaderId = _obj.TaxHeaderId;
                        ObjSOLine = new SalesOrderItemModel();
                        ObjSOLine.OrderId = _obj.OrderId;
                        ObjSOLine.OrderItemId = _obj.OrderItemId;
                        ObjSOLine.CompanyId = _obj.CompanyId;
                        ObjSOLine.Product = _obj.Product;
                        ObjSOLine.CreatedBy = _obj.CreatedBy;
                        ObjSOLine.CreatedDate = _obj.CreatedDate;
                        ObjSOLine.IsSelected = _obj.IsSelected;
                        ObjSOLine.ModifiedBy = _obj.ModifiedBy;
                        ObjSOLine.ModifiedDate = _obj.ModifiedDate;
                        ObjSOLine.ProductId = _obj.ProductId;
                        ObjSOLine.Status = _obj.Status;
                        ObjSOLine.TaxHeader = _obj.TaxHeader;
                        ObjSOLine.SalesOrderTaxes = _obj.SalesOrderTaxes;
                        ObjSOLine.TaxHeaderId = _obj.TaxHeaderId;
                        ObjSOLine.LineStatus = _obj.LineStatus;
                        SOLineDiscAmt = _obj.DiscAmt;

                        SOLineDiscPer = _obj.DiscPer;
                        SOLineFreightAmt = _obj.FreightAmt;
                        SOLineFreightPer = _obj.FreightPer;
                        SOLineQty = _obj.Qty;
                        SOLineRate = _obj.Rate;
                        SOLineNetAmount = _obj.NetAmount;
                        SOLineTaxAmount = _obj.TaxAmount;
                        SOLineGrossAmount = _obj.GrossAmount;




                        ObjSOLine.SalesOrderTaxes = _obj.SalesOrderTaxes;
                        SOTaxes = new List<SalesOrderTaxModel>(_obj.SalesOrderTaxes.Where(x => x.Status == 1));


                        DisplayItemStock(ObjSOLine);

                    }

                    else
                    {
                        ObjSOLine = new SalesOrderItemModel();
                    }
                    OnPropertyChanged(() => ObjSOLine);
                }
            }
            Global.Instance.IsModified = IsModified;
        }

        private void DisplayItemStock(SalesOrderItemModel objSOLine)
        {
            if (ObjSOLine.Product?.ProductStocks != null)
            {

                //display current sotck if extis
                ItemSotck = ObjSOLine.Product.ProductStocks.GroupBy(x => x.ProductId == ObjSOLine.ProductId)
                    .Select(x => new ProductStockModel
                    {
                        StockQty = x.Sum(y => y.StockQty),
                        BalanceQty = x.Sum(y => y.BalanceQty),
                        IssuedQty = x.Sum(y => y.IssuedQty),
                        Product = x.Select(y => y.Product).FirstOrDefault(),
                    }).ToList();


                //display default stock if stock of item is not presenet
                if (ItemSotck.Count <= 0)
                {
                    ItemSotck = new List<ProductStockModel>
                    {
                                new ProductStockModel
                                {
                                    StockQty=0,
                                    BalanceQty = 0,
                                    IssuedQty = 0,
                                    Product = new EntityModel.Product
                                    {
                                        ProductName = ObjSOLine?.Product?.ProductName,
                                        ProductType = new EntityModel.ProductType
                                        {
                                            ParentProductTypeName = ObjSOLine?.Product?.ProductType?.ParentProductTypeName
                                        }
                                    }
                                } };
                }

            }
            else
            {
                ItemSotck = new List<ProductStockModel>();
            }
        }

        private void OnSelectItemButtonCommand(object obj)
        {
            if (!Helpers.IsWindowOpen<OrderItemsView>())
            {
                var view = new OrderItemsView();
                var vm = new SalesOrderItemsViewModel(IsEditMode, this);
                view.DataContext = vm;
                view.ShowDialog();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        private void SetDefaultValues()
        {
            SequenceColl = CustomerMasterManager.GetSequence(Global.TransactionModule, Global.OrderSeqForm);
            Customers = CustomerMasterManager.GetCustomerCollection();
            TaxHeaders = TaxDetailsManager.GetTaxHeaders();
            OnPropertyChanged(() => Customers);
        }

        private void OnsaveAndCloseCommand(object obj)
        {
            OnSaveData();
            if (SaveResult)
            {
                ((SalesOrderView)obj).Close();
            }
            Global.Instance.IsModified = false;
        }
        private bool ValidateBeforeSave()
        {
            int cnt = 1;
            StringBuilder messasge = new StringBuilder();


            if (SalesOrderLineColl.Count == 0 || DataForSaveSoLine.Count == 0)
            {
                cnt++;
                messasge.Append($"{cnt}. " + TransactionMessages.LineItemRequired + "\n");
            }

            if (DataForSaveSoLine.Any(x => (x.Qty == null || x.Qty <= 0) && x.Status == 1))
            {
                cnt++;
                messasge.Append($"{cnt}. " + TransactionMessages.LineItemQtyRequired + "\n");
            }

            objOrder.Validate();
            OnPropertyChanged(() => objOrder);
            foreach (var item in objOrder._errors)
            {
                foreach (var i in item.Value)
                {
                    messasge.Append($"{cnt}. " + i + "\n");
                    cnt++;
                }

            }

            if (messasge.Length > 0)
            {
                MessageBox.Show(messasge.ToString());
                return false;
            }
            return true;
        }
        private bool ValidateBeforeLineSave()
        {
            int cnt = 1;
            StringBuilder messasge = new StringBuilder();


            if (ObjSOLine.Product.ProductStocks.Sum(x => x.StockQty) == 0)
            {
                cnt++;
                messasge.Append($"{cnt}. " + "Stock Qty is zero!! please add some stock against this item" + "\n");
            }

            else if (ObjSOLine.Qty > ObjSOLine.Product.ProductStocks.Sum(x => x.StockQty))
            {
                cnt++;
                messasge.Append($"{cnt}. " + $"Not enough stock is availabe for this item , current stock is {ObjSOLine.Product.ProductStocks.Sum(x => x.StockQty)}" + "\n");
            }


            if (messasge.Length > 0)
            {
                MessageBox.Show(messasge.ToString());
                return false;
            }
            return true;
        }

        private void OnSelectAll(object obj)
        {
            SalesOrderLineColl.Where(x => x.Status == 1).ToList().ForEach(x => x.IsSelected = true);
            if (SalesOrderLineColl.Count > 0)
                IsEnableRemoveLine = true;
            else
                IsEnableRemoveLine = false;

            if (string.Equals(SelectMsg, "De-Select All", StringComparison.OrdinalIgnoreCase))
            {
                SalesOrderLineColl.Where(x => x.Status == 1).ToList().ForEach(x => x.IsSelected = false);
                SelectMsg = "Select All";

            }
            if (SalesOrderLineColl.All(x => x.IsSelected == true))
            {
                SelectMsg = "De-Select All";
            }
        }
        private void OnConfirmedSO(object obj)
        {
            MessageBoxResult result = MessageBox.Show(TransactionMessages.ConfirmedSO, "", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.OK);
            if (result == MessageBoxResult.No) return;
            objOrder.SOHeaderStatus = "confirmed";
            foreach (var salesOrderItem in DataForSaveSoLine)
            {
                salesOrderItem.LineStatus = "confirmed";
            }
            OnSaveData();
        }
        private void OnSaveData()
        {

            if (ValidateBeforeSave())
            {
                var message = new StringBuilder();
                bool isSave = SalesOrderManager.SaveData(objOrder, DataForSaveSoLine, SelectedSequence, ref message, out var tempOrder);
                if (isSave)
                {
                    if (!SaveAndNew)
                    {
                        objOrder = new SalesOrderModel();                    
                        GetOrderDetails(tempOrder.OrderId);
                        EnableEditMode();
                    }
                    MessageBox.Show(message.ToString());
                }
            }
        }

        private void OnClose(object obj)
        {
            if (obj != null)
            {
                if (Global.Instance.IsModified)
                {
                    MessageBoxResult result = MessageBox.Show(GeneralMessages.ChangesHaveMade, "", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.OK);

                    if (result == MessageBoxResult.Yes)
                    {
                        OnSaveData();
                        ((SalesOrderView)obj).Close();
                    }
                    else
                    {
                        ((SalesOrderView)obj).Close();
                    }
                }
                else
                {
                    MessageBoxResult result = MessageBox.Show(GeneralMessages.CloseWindow, "", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.OK);
                    if (result == MessageBoxResult.Yes)
                    {
                        ((SalesOrderView)obj).Close();
                    }
                }
            }
        }

        private void OnSaveAndNewCommand()
        {
            SaveAndNew = true;
            OnSaveData();
            Init();
            if (SaveResult)
            {
                IsEditMode = false;
                IsEnableEdit = false;
                IsEnableSave = true;
                IsEnableSaveClose = true;
                IsEnableSaveNew = true;
                SaveResult = false;
            }
        }

        private void OnEditCommand()
        {
            IsEnabledTaxHeader = false;
            IsReadOnly = false;
            IsEditMode = true;
            IsEnableEdit = false;
            IsEnableSave = true;
            IsEnableSaveClose = true;
            IsEnableSaveNew = true;
            IsEnableSelectAll = IsEnableRemoveLine = SalesOrderLineColl.Count > 0;
            IsEnableEditLine = SalesOrderLineColl.Count > 0;

            if (!string.IsNullOrEmpty(objOrder.SOHeaderStatus) && IsEditMode
                && string.Equals(objOrder.SOHeaderStatus, "Open"))
            {
                IsEnableConfirmed = false;
            }

        }
        private void DisableEditMode()
        {
            IsEnabledTaxHeader = true;
            IsEditMode = false;
            IsEnableEdit = false;
            IsEnableSave = true;
            IsEnableSaveClose = true;
            IsEnableSaveNew = true;
            IsEnableCustomerCode = true;
            IsEnableEntryLine = false;
            IsEnableSOLineGrid = true;
            VisibleSequence = Visibility.Visible;
        }
        private void EnableEditMode()
        {
            IsEnabledTaxHeader = false;
            IsReadOnly = true;
            IsEditMode = true;
            IsEnableConfirmed = false;
            IsEnableEdit = true;
            IsEnableSave = false;
            IsEnableSaveClose = false;
            IsEnableSaveNew = false;
            IsEnableCustomerCode = false;
            IsEnableSOLineGrid = true;
            IsEnableSelectAll = false;
            IsEnableRemoveLine = false;

            if (!string.IsNullOrEmpty(objOrder.SOHeaderStatus) && IsEditMode
                && string.Equals(objOrder.SOHeaderStatus, "Open"))
            {
                IsEnableConfirmed = true;
            }

            if (!string.IsNullOrEmpty(objOrder.SOHeaderStatus) && IsEditMode
                && string.Equals(objOrder.SOHeaderStatus, "Confirmed",StringComparison.OrdinalIgnoreCase))
            {
                IsEnableEdit = false;
                OnPropertyChanged(() => IsEnableEdit);

            }

            VisibleSequence = Visibility.Collapsed;
            Global.Instance.IsModified = false;

        }
        #endregion

        #region Header Properties
        private bool _isReadOnly;
        public bool IsReadOnly
        {
            get => _isReadOnly;
            set { _isReadOnly = value; OnPropertyChanged(() => IsReadOnly); }
        }

        private ObservableCollection<EntityModel.TaxHeader> _TaxHeaders;
        public ObservableCollection<EntityModel.TaxHeader> TaxHeaders
        {
            get { return _TaxHeaders; }
            set { _TaxHeaders = value; OnPropertyChanged(() => TaxHeaders); }
        }
        private bool _saveResult;
        public bool SaveResult
        {
            get => _saveResult;
            set { _saveResult = value; OnPropertyChanged(() => SaveResult); }
        }
        private bool _saveAndNew;
        public bool SaveAndNew
        {
            get { return _saveAndNew; }
            set { _saveAndNew = value; OnPropertyChanged(() => SaveAndNew); }
        }

        private string _oldCityName;
        public string OldCityName
        {
            get { return _oldCityName; }
            set { _oldCityName = value; OnPropertyChanged(() => OldCityName); }
        }

        private bool _isEditMode;
        public bool IsEditMode
        {
            get { return _isEditMode; }
            set { _isEditMode = value; OnPropertyChanged(() => IsEditMode); }
        }

        private bool _isEnableEdit;
        public bool IsEnableEdit
        {
            get => _isEnableEdit;
            set { _isEnableEdit = value; OnPropertyChanged(() => IsEnableEdit); }
        }

        private bool _isEnableSaveNew;
        public bool IsEnableSaveNew
        {
            get => _isEnableSaveNew;
            set { _isEnableSaveNew = value; OnPropertyChanged(() => IsEnableSaveNew); }
        }
        private bool _IsEnableCustomerCode;
        public bool IsEnableCustomerCode
        {
            get => _IsEnableCustomerCode;
            set { _IsEnableCustomerCode = value; OnPropertyChanged(() => IsEnableCustomerCode); }
        }

        private Visibility _VisibleSequence;
        public Visibility VisibleSequence
        {
            get { return _VisibleSequence; }
            set { _VisibleSequence = value; OnPropertyChanged(() => VisibleSequence); }
        }


        private bool _isEnableSaveClose;
        public bool IsEnableSaveClose
        {
            get => _isEnableSaveClose;
            set { _isEnableSaveClose = value; OnPropertyChanged(() => IsEnableSaveClose); }
        }


        private bool _isEnableSave;
        public bool IsEnableSave
        {
            get => _isEnableSave;
            set { _isEnableSave = value; OnPropertyChanged(() => IsEnableSave); }
        }


        private SalesOrderModel _objOrder;
        public SalesOrderModel objOrder
        {
            get { return _objOrder; }
            set { _objOrder = value; OnPropertyChanged(() => objOrder); }
        }

        private ObservableCollection<EntityModel.Customer> _Customers;
        public ObservableCollection<EntityModel.Customer> Customers
        {
            get { return _Customers; }
            set { _Customers = value; OnPropertyChanged(() => Customers); }
        }

        #region Properties
        private SalesOrderItemModel _objSOLine;
        public SalesOrderItemModel ObjSOLine
        {
            get { return _objSOLine; }
            set { _objSOLine = value; OnPropertyChanged(() => ObjSOLine); }
        }

        private ObservableCollection<SalesOrderItemModel> _SalesOrderLineColl;
        public ObservableCollection<SalesOrderItemModel> SalesOrderLineColl
        {
            get { return _SalesOrderLineColl; }
            set { _SalesOrderLineColl = value; OnPropertyChanged(() => SalesOrderLineColl); }
        }

        private ObservableCollection<SalesOrderItemModel> _DataForSaveSoLine;
        public ObservableCollection<SalesOrderItemModel> DataForSaveSoLine
        {
            get { return _DataForSaveSoLine; }
            set { _DataForSaveSoLine = value; OnPropertyChanged(() => DataForSaveSoLine); }
        }

        private ObservableCollection<EntityModel.Sequence> _SequenceColl;
        public ObservableCollection<EntityModel.Sequence> SequenceColl
        {
            get { return _SequenceColl; }
            set { _SequenceColl = value; OnPropertyChanged(() => SequenceColl); }
        }
        private EntityModel.Sequence _SelectedSequence;
        public EntityModel.Sequence SelectedSequence
        {
            get { return _SelectedSequence; }
            set { _SelectedSequence = value; OnPropertyChanged(() => SelectedSequence); }
        }

        private bool _IsEnabledTaxHeader;
        public bool IsEnabledTaxHeader
        {
            get { return _IsEnabledTaxHeader; }
            set { _IsEnabledTaxHeader = value; OnPropertyChanged(() => IsEnabledTaxHeader); }
        }

        #endregion Properties       

        #endregion

        #region Line Properties
        private bool _IsEnableNewLine;

        public bool IsEnableNewLine
        {
            get { return _IsEnableNewLine; }
            set { _IsEnableNewLine = value; OnPropertyChanged(() => IsEnableNewLine); }
        }

        private bool _IsEnableEditLine;

        public bool IsEnableEditLine
        {
            get { return _IsEnableEditLine; }
            set { _IsEnableEditLine = value; OnPropertyChanged(() => IsEnableEditLine); }
        }

        private bool _IsEnableSaveLine;

        public bool IsEnableSaveLine
        {
            get { return _IsEnableSaveLine; }
            set { _IsEnableSaveLine = value; OnPropertyChanged(() => IsEnableSaveLine); }
        }

        private bool _IsEnableRemoveLine;

        public bool IsEnableRemoveLine
        {
            get { return _IsEnableRemoveLine; }
            set { _IsEnableRemoveLine = value; OnPropertyChanged(() => IsEnableRemoveLine); }
        }

        private bool _IsEnableCancelLine;
        public bool IsEnableCancelLine
        {
            get { return _IsEnableCancelLine; }
            set { _IsEnableCancelLine = value; OnPropertyChanged(() => IsEnableCancelLine); }
        }

        private bool _IsEnableEditableFileds;
        public bool IsEnableEditableFileds
        {
            get { return _IsEnableEditableFileds; }
            set { _IsEnableEditableFileds = value; OnPropertyChanged(() => IsEnableEditableFileds); }
        }

        private string _SelectMsg;
        public string SelectMsg
        {
            get { return _SelectMsg; }
            set { _SelectMsg = value; OnPropertyChanged(() => SelectMsg); }
        }

        private string _CustomerName;
        public string CustomerName
        {
            get { return _CustomerName; }
            set { _CustomerName = value; OnPropertyChanged(() => CustomerName); }
        }

        private bool _IsEnableSelectAll;

        public bool IsEnableSelectAll
        {
            get { return _IsEnableSelectAll; }
            set { _IsEnableSelectAll = value; OnPropertyChanged(() => IsEnableSelectAll); }
        }


        public decimal? SOLineQty
        {
            get { return ObjSOLine.Qty; }
            set
            {
                ObjSOLine.Qty = value;

                OnPropertyChanged(() => SOLineQty);
                SetItemNetProperties();
            }
        }



        private int? oldTaxHeaderId;
        public int? TaxHeaderId
        {
            get
            {

                oldTaxHeaderId = ObjSOLine.TaxHeaderId;
                return ObjSOLine.TaxHeaderId;

            }
            set
            {
                if (value.HasValue)
                {
                    ObjSOLine.TaxHeader = TaxHeaders.FirstOrDefault(x => x.TaxHeaderId == value.Value && x.Status == 1);

                }
                else
                {

                    ObjSOLine.TaxHeader = null;
                }

                if (IsEnableSaveLine)
                {

                    if (ObjSOLine.TaxHeader != null)
                    {

                        if (oldTaxHeaderId != ObjSOLine.TaxHeader.TaxHeaderId)
                        {
                            CalculateLineTax();
                        }
                        else
                        {
                            UpdateLineTax();
                        }

                    }
                }
                ObjSOLine.TaxHeaderId = value;
                oldTaxHeaderId = ObjSOLine.TaxHeaderId;
                OnPropertyChanged(() => ObjSOLine.TaxHeaderId);
            }
        }


        public decimal? SOLineRate
        {
            get { return ObjSOLine.Rate; }
            set
            {
                ObjSOLine.Rate = value;

                OnPropertyChanged(() => SOLineRate);
                SetItemNetProperties();
            }
        }


        public decimal? SOLineDiscPer
        {
            get { return ObjSOLine.DiscPer; }
            set
            {
                ObjSOLine.DiscPer = value;
                if (SOLineDiscPer.HasValue && SOLineDiscAmt.HasValue && SOLineDiscAmt.GetValueOrDefault(0) != 0)
                {
                    MessageBox.Show("Discount Amount has value");
                    SOLineDiscAmt = null;
                    OnPropertyChanged(() => SOLineDiscAmt);
                }


                OnPropertyChanged(() => SOLineDiscPer);
                SetItemNetProperties();
            }
        }


        public decimal? SOLineDiscAmt
        {
            get { return ObjSOLine.DiscAmt; }
            set
            {
                ObjSOLine.DiscAmt = value;

                if (ObjSOLine.DiscAmt.HasValue && SOLineDiscPer.HasValue && SOLineDiscPer.GetValueOrDefault(0) != 0)
                {
                    MessageBox.Show("Discount Percentage has value");
                    SOLineDiscPer = null;
                    OnPropertyChanged(() => SOLineDiscPer);
                }
                OnPropertyChanged(() => SOLineDiscAmt);

                SetItemNetProperties();
            }
        }


        public decimal? SOLineFreightPer
        {
            get { return ObjSOLine.FreightPer; }
            set
            {
                ObjSOLine.FreightPer = value;
                if (ObjSOLine.FreightPer.HasValue && SOLineFreightAmt.HasValue && SOLineFreightAmt.GetValueOrDefault(0) != 0)
                {
                    MessageBox.Show("Freight Amount has value");
                    SOLineFreightAmt = null;
                    OnPropertyChanged(() => SOLineFreightAmt);
                }
                OnPropertyChanged(() => SOLineFreightPer);
                SetItemNetProperties();
            }
        }

        public decimal? SOLineFreightAmt
        {
            get { return ObjSOLine.FreightAmt; }
            set
            {
                ObjSOLine.FreightAmt = value;

                if (ObjSOLine.FreightAmt.HasValue && SOLineFreightPer.HasValue && SOLineFreightPer.GetValueOrDefault(0) != 0)
                {
                    MessageBox.Show("Freight Percentage has value");
                    SOLineFreightPer = null;
                    OnPropertyChanged(() => SOLineFreightPer);

                }
                OnPropertyChanged(() => SOLineFreightAmt);
                SetItemNetProperties();
            }
        }

        public decimal? SOLineNetAmount
        {
            get { return ObjSOLine.NetAmount; }
            set
            {
                ObjSOLine.NetAmount = value;
                if (ObjSOLine.TaxHeader != null)
                    UpdateLineTax();

                OnPropertyChanged(() => SOLineNetAmount);
            }
        }


        public decimal? SOLineGrossAmount
        {
            get { return ObjSOLine.GrossAmount; }
            set
            {
                ObjSOLine.GrossAmount = value;
                OnPropertyChanged(() => SOLineGrossAmount);

            }
        }



        public List<string> LineStatus
        {
            get { return new List<string> { Global.confirmed, Global.Open, Global.Closed }; }
           
        }
        


        public List<string> HeaderStatus
        {
            get { return new List<string> { Global.Open, Global.Closed, Global.confirmed, Global.Processed }; }

        }


        public decimal? SOLineTaxAmount
        {
            get { return ObjSOLine.TaxAmount; }
            set
            {
                ObjSOLine.TaxAmount = value;
                OnPropertyChanged(() => SOLineTaxAmount);

            }
        }


        private List<SalesOrderTaxModel> _SOTaxes;
        public List<SalesOrderTaxModel> SOTaxes
        {
            get { return _SOTaxes; }
            set { _SOTaxes = value; OnPropertyChanged(() => SOTaxes); }
        }


        private bool _IsEnableEntryLine;
        public bool IsEnableEntryLine
        {
            get { return _IsEnableEntryLine; }
            set { _IsEnableEntryLine = value; OnPropertyChanged(() => IsEnableEntryLine); }
        }

        private bool _IsEnableSOLineGrid;
        public bool IsEnableSOLineGrid
        {
            get { return _IsEnableSOLineGrid; }
            set { _IsEnableSOLineGrid = value; OnPropertyChanged(() => IsEnableSOLineGrid); }
        }


        private List<ProductStockModel> _ItemSotck;
        public List<ProductStockModel> ItemSotck
        {
            get { return _ItemSotck; }
            set { _ItemSotck = value; OnPropertyChanged(() => ItemSotck); }
        }


        private bool _IsEnableConfirmed;
        public bool IsEnableConfirmed
        {
            get { return _IsEnableConfirmed; }
            set { _IsEnableConfirmed = value; OnPropertyChanged(() => IsEnableConfirmed); }
        }

        #endregion
    }
}
