﻿using BillingSoftware.Command;
using BillingSoftware.Manager;
using BillingSoftware.Model;
using BillingSoftware.UI.Transaction.SalesOrder;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace BillingSoftware.ViewModel.Transaction.SalesOrder
{
    public class SalesOrderItemsViewModel : NotifyPropertyChangedBase
    {
        private bool isEditMode;
        private SalesOrderViewModel ParentVm { get; set; }
        public DelegateCommand<object> SelecteAllProduct { get; private set; }
        public DelegateCommand<object> AppendItemCommand { get; private set; }

        public SalesOrderItemsViewModel(bool isEditMode, SalesOrderViewModel orderViewModel)
        {
            this.isEditMode = isEditMode;
            this.ParentVm = orderViewModel;

            SetDefaultvalue();

            if (this.ParentVm.SalesOrderLineColl.Count > 0)
            {
                foreach (var item in this.ParentVm.SalesOrderLineColl.ToList())
                {
                    Products.Remove(products.FirstOrDefault(x => x.ProductId == item.ProductId));
                }
            }

            OnPropertyChanged(() => Products);
            AppendItemCommand = new DelegateCommand<object>((obj) => { OnAppendItems(obj); });
            SelecteAllProduct = new DelegateCommand<object>((obj) => { OnSelectAll(obj); });
        }
        private void OnAppendItems(object obj)
        {
            double seconds = 1;
            foreach (var item in Products.Where(x => x.IsSelected == true).ToList())
            {
                SalesOrderItemModel SOLineItem = new SalesOrderItemModel();
                SOLineItem.OrderItemId = 0;
                SOLineItem.IsSelected = item.IsSelected;
                SOLineItem.OrderId = ParentVm.objOrder?.OrderId;
                SOLineItem.ProductId = item.ProductId;
                SOLineItem.Qty = null;
                SOLineItem.Product = item;
                SOLineItem.Rate = null;
                SOLineItem.DiscPer = null;
                SOLineItem.DiscAmt = null;
                SOLineItem.FreightPer = null;
                SOLineItem.FreightAmt = null;
                SOLineItem.GrossAmount = null;
                SOLineItem.NetAmount = null;
                SOLineItem.TaxAmount = null;
                SOLineItem.OtherProductItemDetails = item.ProductDetails;
                SOLineItem.CreatedBy = Global.Instance.LoginUserName;
                SOLineItem.CreatedDate = DateTime.Now.AddSeconds(seconds);
                SOLineItem.ModifiedBy = null;
                SOLineItem.ModifiedDate = null;
                SOLineItem.CompanyId = Global.Instance.CompanyId;
                SOLineItem.LineStatus = "Open";
                SOLineItem.Status = 1;
                SOLineItem.Product = item;
                seconds++;

                ParentVm.SalesOrderLineColl.Add(SOLineItem);
                ParentVm.DataForSaveSoLine.Add(SOLineItem);
                Products.Remove(item);
            }

            ParentVm.IsEnableRemoveLine = ParentVm.SalesOrderLineColl.Count > 0;
            ParentVm.IsEnableEditLine = ParentVm.SalesOrderLineColl.Count > 0;
            ParentVm.IsEnableSelectAll = ParentVm.SalesOrderLineColl.Count > 0;

            if (obj is OrderItemsView)
                ((Window)obj).Close();

        }

        private void OnSelectAll(object obj)
        {
            Products.Where(x => x.Status == 1).ToList().ForEach(x => x.IsSelected = true);

            if (string.Equals(SelectMsg, "De-Select All", StringComparison.OrdinalIgnoreCase))
            {
                Products.Where(x => x.Status == 1).ToList().ForEach(x => x.IsSelected = false);
                SelectMsg = "Select All";
            }
            if (Products.All(x => x.IsSelected == true))
            {
                SelectMsg = "De-Select All";
            }
        }

        private void SetDefaultvalue()
        {
            SelectMsg = "Select All";
            var Items = ParseProduct(ProductManager.GetProductCollection())
            .Select(x => new ProductModel
            {
                ProductId = x.ProductId,
                ProductionTypeId = x.ProductionTypeId,
                ProductColor = x.ProductColor,
                ProductName = x.ProductName,
                ProductSize = x.ProductSize,
                ProductDetails = x.ProductDetails,
                IsGstApplicable = x.IsGstApplicable,
                CompanyId = x.CompanyId,
                Status = x.Status,
                CreatedBy = x.CreatedBy,
                ModifiedBy = x.ModifiedBy,
                ModifiedDate = x.ModifiedDate,
                CreatedDate = x.CreatedDate,
                ProductType = x.ProductType,
                ProductStocks = x.ProductStocks,
                ProductSales = x.ProductSales,
                SalesOrderItems = x.SalesOrderItems,
                AvailableStockQty = x.ProductStocks.Sum(y => y.StockQty)

            }).ToList();

            Products = new ObservableCollection<ProductModel>(Items);

            OnPropertyChanged(() => Products);
        }

        private ObservableCollection<ProductModel> ParseProduct(ObservableCollection<EntityModel.Product> Products)
        {
            ObservableCollection<ProductModel> ProductModel = new ObservableCollection<ProductModel>();
            foreach (var product in Products)
            {
                var objProduct = new ProductModel();
                objProduct.ProductId = product.ProductId;
                objProduct.ProductionTypeId = product.ProductionTypeId;
                objProduct.ProductColor = product.ProductColor;
                objProduct.ProductSize = product.ProductSize;
                objProduct.ProductDetails = product.ProductDetails;
                objProduct.IsGstApplicable = product.IsGstApplicable;
                objProduct.ProductName = product.ProductName;
                objProduct.CompanyId = product.CompanyId;
                objProduct.Status = product.Status;
                objProduct.CreatedBy = product.CreatedBy;
                objProduct.ModifiedBy = product.ModifiedBy;
                objProduct.ModifiedDate = product.ModifiedDate;
                objProduct.CreatedDate = product.CreatedDate;
                objProduct.ProductType = product.ProductType;

                objProduct.ProductStocks = product.ProductStocks;
                objProduct.ProductSales = product.ProductSales;
                objProduct.SalesOrderItems = product.SalesOrderItems;


                ProductModel.Add(objProduct);
            }
            return ProductModel;
        }

        #region Properties
        private string _SelectMsg;
        public string SelectMsg
        {
            get { return _SelectMsg; }
            set { _SelectMsg = value; OnPropertyChanged(() => SelectMsg); }
        }

        private ObservableCollection<ProductModel> products;
        public ObservableCollection<ProductModel> Products
        {
            get { return products; }
            set { products = value; OnPropertyChanged(() => Products); }
        }


        #endregion Properties
    }
}
