﻿using BillingSoftware.Command;
using BillingSoftware.HelperFunction;
using BillingSoftware.Manager;
using BillingSoftware.Model;
using BillingSoftware.UI.Transaction.SalesOrder;
using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BillingSoftware.ViewModel.Transaction.SalesOrder
{
    public class SalesOrderCollectionViewModel : ViewModelBase
    {
        #region command
        public DelegateCommand<object> NewButtonCommand { get; }
        public DelegateCommand<object> CloseButtonCommand { get; }
        public DelegateCommand<object> SelectedOrder { get; }
        public DelegateCommand<object> DeleteButtonCommand { get; }
        public DelegateCommand<object> ReportButtonCommand { get; }
        public DelegateCommand<object> ProcessButtonCommand { get; }
        public DelegateCommand<object> PrintButtonCommand { get; }
        #endregion Command

        public SalesOrderCollectionViewModel()
        {
            Predicate<object> canEdit = delegate (object obj) { if ((EntityModel.SalesOrder)obj != null) return true; else return false; };


            Predicate<object> canPrintSO =
                delegate (object obj)
                {
                    if (obj is EntityModel.SalesOrder _obj)
                    {
                        if (!string.IsNullOrEmpty(_obj.SOHeaderStatus) && string.Equals(_obj.SOHeaderStatus, Global.Completed))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                        return false;
                };


            Predicate<object> canProcessSO =
                delegate (object obj)
                {
                    if (obj is EntityModel.SalesOrder _obj)
                    {
                        if (!string.IsNullOrEmpty(_obj.SOHeaderStatus) && string.Equals(_obj.SOHeaderStatus, Global.confirmed))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                        return false;
                };


            NewButtonCommand = new DelegateCommand<object>((e) => { OnAddNewButtonCommand(); }, (e) => { return true; });
            SelectedOrder = new DelegateCommand<object>((obj) => { OnselectedOrder(obj); }, canEdit);

            //report command
            ReportButtonCommand = new DelegateCommand<object>((obj) => { OnReportPrintCommand(obj); }, canEdit);

            //processCommand
            ProcessButtonCommand = new DelegateCommand<object>((obj) => { OnProcessSO(obj); }, canProcessSO);

            // print
            PrintButtonCommand = new DelegateCommand<object>((obj) => { OnPrintSO(obj); }, canPrintSO);
            SetDefaultValues();
        }

        private void OnPrintSO(object obj)
        {
            if (obj is EntityModel.SalesOrder _obj)
            {
                SalesOrderManager.PrintSalesOrder(_obj);
            }

        }

        private void OnProcessSO(object obj)
        {
            if (obj is EntityModel.SalesOrder _obj)
            {
                //MessageBoxResult result = MessageBox.Show(TransactionMessages.ProcessSO, "", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.OK);
                //if (result == MessageBoxResult.No) return;
                StringBuilder msg = new StringBuilder();

                var IsProcessed = SalesOrderManager.SalesOrderProcess(_obj, ref msg);

                if (IsProcessed)
                {
                    MessageBox.Show(TransactionMessages.ProcessCompleted, "Process Done", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    if (msg.Length > 0)
                    {
                        MessageBox.Show(msg.ToString());
                    }
                }

                Orders = SalesOrderManager.GetOrders();
                OnPropertyChanged(nameof(Orders));
            }
        }

        private void OnReportPrintCommand(object obj)
        {
            if (obj != null)
            {
                var _obj = obj as EntityModel.SalesOrder;
                if (_obj != null)
                {
                    if (!Helpers.IsWindowOpen<SalesOrderView>())
                    {
                        var view = new SalesOrderView();
                        var viewModel = new SalesOrderViewModel(_obj.OrderId, true);
                        view.DataContext = viewModel;
                        view.Closed += RefreshGridOnClosed; ;
                        view.Show();
                    }
                    else
                    {
                        MessageBox.Show(GeneralMessages.WindowIsOpened);
                    }
                }
            }
        }

        private void OnselectedOrder(object obj)
        {
            if (obj != null)
            {
                var _obj = obj as EntityModel.SalesOrder;
                if (_obj != null)
                {
                    if (!Helpers.IsWindowOpen<SalesOrderView>())
                    {
                        var view = new SalesOrderView();
                        var viewModel = new SalesOrderViewModel(_obj.OrderId, true);
                        view.DataContext = viewModel;
                        view.Closed += RefreshGridOnClosed;
                        view.Show();
                    }
                    else
                    {
                        MessageBox.Show(GeneralMessages.WindowIsOpened);
                    }
                }
            }
        }

        private void OnAddNewButtonCommand()
        {
            if (!Helpers.IsWindowOpen<SalesOrderView>())
            {
                var view = new SalesOrderView();
                var viewModel = new SalesOrderViewModel();
                view.DataContext = viewModel;
                view.Closed += RefreshGridOnClosed; ;
                view.Show();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        private void RefreshGridOnClosed(object sender, EventArgs e)
        {
            Orders = SalesOrderManager.GetOrders();
            OnPropertyChanged(nameof(Orders));
        }

        private void SetDefaultValues()
        {
            Orders = SalesOrderManager.GetOrders();
        }



        #region Properties
        private ObservableCollection<EntityModel.SalesOrder> _Orders;
        public ObservableCollection<EntityModel.SalesOrder> Orders
        {
            get { return _Orders; }
            set { _Orders = value; OnErrorsChanged(nameof(Orders)); }
        }

        #endregion Properties
    }
}
