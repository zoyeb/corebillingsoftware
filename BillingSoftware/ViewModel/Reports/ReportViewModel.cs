﻿using BillingSoftware.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingSoftware.ViewModel.Reports
{
    public class ReportViewModel : NotifyPropertyChangedBase
    {
        public delegate void Print();        
        private ReprotType rpt { get; set; }
        private int Id { get; set; }

        public ReportViewModel(int Id, ReprotType rpt)
        {
            this.Id = Id;
            this.rpt = rpt;

        }
        public enum ReprotType
        {
            Order,
            Invoice
        }

        public static void PrintReport(ReprotType rpt)
        {
            switch (rpt)
            {
                case ReprotType.Invoice:                   
                    break;

                case ReprotType.Order:
                    Print p = new Print(PreviewOrderReport);
                    p();
                    break;

                default:
                    break;
            }
        }


        #region  Order Report
    
        private static void PreviewOrderReport()
        {

        }
        #endregion
    }
}
