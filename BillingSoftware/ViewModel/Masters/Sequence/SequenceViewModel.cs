﻿using BillingSoftware.Command;
using BillingSoftware.Manager;
using BillingSoftware.Model;
using BillingSoftware.UI.Masters.State;
using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using BillingSoftware.EntityModel;
using BillingSoftware.UI.Masters.Sequence;
using BillingSoftware.HelperFunction;

namespace BillingSoftware.ViewModel.Masters.Sequence
{
    public class SequenceViewModel : NotifyPropertyChangedBase
    {

        #region Declaration


        #region  LineCommand

        private DelegateCommand<object> newLineCommand;
        public DelegateCommand<object> NewLineCommand
        {
            get { return newLineCommand; }
        }

        private DelegateCommand<object> saveLineCommand;
        public DelegateCommand<object> SaveLineCommand
        {
            get { return saveLineCommand; }
        }

        private DelegateCommand<object> editLineCommand;
        public DelegateCommand<object> EditLineCommand
        {
            get { return editLineCommand; }
        }

        private DelegateCommand<object> cancelLineCommand;
        public DelegateCommand<object> CancelLineCommand
        {
            get { return cancelLineCommand; }
        }

        private DelegateCommand<object> removeLineCommand;
        public DelegateCommand<object> RemoveLineCommand
        {
            get { return removeLineCommand; }
        }
        #endregion


        private DelegateCommand<object> saveAndCloseCommand;
        public DelegateCommand<object> SaveAndCloseCommand
        {
            get { return saveAndCloseCommand; }
        }

        private DelegateCommand<object> _SaveCommand;
        public DelegateCommand<object> SaveCommand
        {
            get { return _SaveCommand; }
        }

        private DelegateCommand<object> _SaveAndNewCommand;
        public DelegateCommand<object> SaveAndNewCommand
        {
            get { return _SaveAndNewCommand; }
        }
        private DelegateCommand<object> _EditCommand;
        public DelegateCommand<object> EditCommand
        {
            get { return _EditCommand; }
        }

        private DelegateCommand<object> _CloseButtonCommand;
        public DelegateCommand<object> CloseButtonCommand
        {
            get { return _CloseButtonCommand; }
        }

        private DelegateCommand<object> _SeqHdrSelectionChanged;
        public DelegateCommand<object> SeqHdrSelectionChanged
        {
            get { return _SeqHdrSelectionChanged; }
        }

        private DelegateCommand<object> _SeqSelectionChanged;
        public DelegateCommand<object> SeqSelectionChanged
        {
            get { return _SeqSelectionChanged; }
        }

        #endregion

        #region Constructor

        public SequenceViewModel()
        {
            DisableEditMode();
            Init();
            IsEnableSequence = true;
            IsEnableNewLine = true;
        }

        public SequenceViewModel(int seqHeaderId, bool IsEditMode)
        {
            Init();
            this.IsEditMode = IsEditMode;
            var data = SequenceManager.getSeqHeaderById(seqHeaderId);
            if (data != null)
            {
                OnSeqHdrSelectionChanged(data);
            }
            EnableEditMode();
            IsEnableSequence = true;
        }

        #endregion


        #region Methods


        private void DisableEditMode()
        {
            IsEditMode = false;
            IsEnableEdit = false;
            IsEnableSave = true;
            IsEnableSaveClose = true;
            IsEnableSaveNew = true;
        }
        private void EnableEditMode()
        {
            IsReadOnly = true;
            IsEnableControls = false;
            IsEditMode = true;
            IsEnableEdit = true;
            IsEnableSave = false;
            IsEnableSaveClose = false;
            IsEnableSaveNew = false;

            IsEnableSaveLine = false;
            IsEnableNewLine = false;
            IsEnableCancelLine = false;
            IsEnableEditLine = false;
            IsEnableRemoveLine = false;
            Global.Instance.IsModified = false;
        }
        private void Init()
        {
            objSeq = new SequenceModel { CreatedDate = DateTime.Now, IsActive = false, CreatedBy = Global.Instance.LoginUserName, Status = 1, LastGeneratedNo = 0, StartDate = null, EndDate = null };
            objHdrSeq = new EntityModel.SequenceHeader();
            _SeqHdrSelectionChanged = new DelegateCommand<object>((obj) => { OnSeqHdrSelectionChanged(obj); }, (s) => true);
            _SeqSelectionChanged = new DelegateCommand<object>((obj) => { OnSeqSelectionChanged(obj); }, (s) => true);

             saveAndCloseCommand = new DelegateCommand<object>((obj) => { OnsaveAndCloseCommand(obj); }, (s) => true);
            _EditCommand = new DelegateCommand<object>((s) => { OnEditCommand(); }, (s) => { return true; });
            _SaveCommand = new DelegateCommand<object>((a) => { OnSaveData(); });
            _SaveAndNewCommand = new DelegateCommand<object>((obj) => OnSaveAndNewCommand());
            _CloseButtonCommand = new DelegateCommand<object>((obj) => { OnClose(obj); });

            //line Command
            Predicate<object> CanSelectedCountry = delegate (object s) { if (((EntityModel.Sequence)s) != null) { return true; } else { return false; } };
            newLineCommand = new DelegateCommand<object>((s) => { OnNewLineCommand(); }, (s) => { return true; });
            saveLineCommand = new DelegateCommand<object>((s) => { OnSaveLineCommand(); }, (s) => { return true; });
            editLineCommand = new DelegateCommand<object>((s) => { OnEditLineCommand(s); }, CanSelectedCountry);
            cancelLineCommand = new DelegateCommand<object>((s) => { OnCancelLineCommand(s); }, (s) => { return true; });
            removeLineCommand = new DelegateCommand<object>((s) => { OnRemoveLineCommand(s); }, CanSelectedCountry);


            SetDefaultValue();
        }



        private void OnRemoveLineCommand(object obj)
        {

            if (!(obj is null))
            {
                if (obj is EntityModel.Sequence)
                {
                    var _obj = obj as EntityModel.Sequence;

                    MessageBoxResult result = MessageBox.Show(MasterMessages.DeleteMessage, MasterMessages.RecordDeleted, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes, MessageBoxOptions.None);
                    if (result == MessageBoxResult.No)
                    {
                        return;
                    }
                    if (_obj.SeqId > 0)
                    {
                        if (SequenceColl.Count(x => x.SeqId == _obj.SeqId) > 0)
                        {
                            var selectedObj = SequenceColl.FirstOrDefault(x => x.SeqId == _obj.SeqId);
                            int indexValue = SequenceColl.IndexOf(selectedObj);
                            SequenceColl.RemoveAt(indexValue);
                        }
                    }
                    else
                    {
                        var selectedObj = SequenceColl.FirstOrDefault(x => x.CreatedDate == _obj.CreatedDate);
                        int indexValue = SequenceColl.IndexOf(selectedObj);
                        SequenceColl.RemoveAt(indexValue);
                    }
                }

                if (SequenceColl.Count == 0)
                {

                    IsEnableSaveLine = false;
                    IsEnableNewLine = true;
                    IsEnableCancelLine = false;
                    IsEnableEditLine = false;
                }
                IsReadOnly = true;
                IsEnableControls = false;
            }
        }

        private void OnCancelLineCommand(object obj)
        {
            if (obj != null)
            {
                if (obj is EntityModel.Sequence)
                {
                    var _obj = obj as EntityModel.Sequence;
                    objSeq.prefix = _obj.prefix;
                    objSeq.IsActive = _obj.IsActive;
                    objSeq.CompanyId = _obj.CompanyId;
                    objSeq.TotalLength = _obj.TotalLength;
                    ModuleName = _obj.SequenceHeader?.ModuleName;
                    objSeq.LastGeneratedNo = _obj.LastGeneratedNo ?? 0;
                    objSeq.SeqHeaderId = _obj.SeqHeaderId;
                    objSeq.StartRangeNo = _obj.StartRangeNo;
                    objSeq.Status = _obj.Status;
                    objSeq.StartDate = _obj.StartDate;
                    objSeq.EndDate = _obj.EndDate;

                }

            }
            OnPropertyChanged(() => objSeq);
            IsEnableNewLine = true;
            IsEnableCancelLine = false;
            IsEnableEditLine = true;
            IsEnableControls = false;
            IsEnableSaveLine = false;


            if (SequenceColl.Count > 0)
                IsEnableRemoveLine = true;
            else
                IsEnableRemoveLine = false;
        }

        private void OnEditLineCommand(object obj)
        {

            var _obj = obj as EntityModel.Sequence;

            objSeq = new SequenceModel();
            objSeq.SeqId = _obj.SeqId;
            objSeq.prefix = _obj.prefix;
            objSeq.IsActive = _obj.IsActive;
            objSeq.CompanyId = Global.Instance.CompanyId;
            objSeq.TotalLength = _obj.TotalLength;
            objSeq.LastGeneratedNo = _obj.LastGeneratedNo ?? 0;
            objSeq.SeqHeaderId = _obj.SeqHeaderId;
            objSeq.StartRangeNo = _obj.StartRangeNo;
            objSeq.Status = _obj.Status;
            objSeq.EndDate = _obj.EndDate;
            objSeq.StartDate = _obj.StartDate;
            objSeq.CreatedDate = _obj.CreatedDate;



            IsEnableNewLine = false;
            IsEnableCancelLine = true;
            IsEnableEditLine = false;
            IsEnableSaveLine = true;
            IsEnableRemoveLine = false;
            IsReadOnly = false;
            IsEnableControls = true;
        }

        private void OnSaveLineCommand()
        {
            IsEnableSaveLine = false;
            IsEnableNewLine = true;
            IsEnableCancelLine = false;
            IsEnableEditLine = true;
            IsReadOnly = true;
            IsEnableControls = false;

            var _obj = new EntityModel.Sequence();
            _obj.SeqId = objSeq.SeqId;
            _obj.prefix = objSeq.prefix;
            _obj.IsActive = objSeq.IsActive;
            _obj.CompanyId = Global.Instance.CompanyId;
            _obj.TotalLength = objSeq.TotalLength;
            _obj.LastGeneratedNo = objSeq.LastGeneratedNo ?? 0;
            _obj.SeqHeaderId = objSeq.SeqHeaderId;
            _obj.StartRangeNo = objSeq.StartRangeNo;
            _obj.Status = objSeq.Status;
            _obj.StartDate = objSeq.StartDate;
            _obj.EndDate = objSeq.EndDate;
            _obj.CreatedDate = objSeq.CreatedDate;
            _obj.CreatedBy = Global.Instance.LoginUserName;
            _obj.Status = 1;


            if (SequenceColl.FirstOrDefault(x => x.SeqId == _obj.SeqId) != null)
            {
                SequenceColl.Remove(SequenceColl.FirstOrDefault(x => x.SeqId == _obj.SeqId));
            }
            else
            {
                if (SequenceColl.FirstOrDefault(x => x.CreatedDate == _obj.CreatedDate) != null)
                {
                    SequenceColl.Remove(SequenceColl.FirstOrDefault(x => x.CreatedDate == _obj.CreatedDate));
                }
            }

            SequenceColl.Add(_obj);

            if (SequenceColl.Count > 0)
                IsEnableRemoveLine = true;
            else
                IsEnableRemoveLine = false;

            OnPropertyChanged(() => SequenceColl);

        }

        private void OnNewLineCommand()
        {

            objSeq = new SequenceModel { CreatedDate = DateTime.Now, IsActive = false, CreatedBy = Global.Instance.LoginUserName, Status = 1, LastGeneratedNo = 0, StartDate = null, EndDate = null, SeqHeaderId = objHdrSeq.SeqHeaderId };

            IsEnableNewLine = false;
            IsEnableCancelLine = true;
            IsEnableEditLine = false;
            IsEnableSaveLine = true;
            IsReadOnly = false;
            IsEnableControls = true;
        }

        private void OnEditCommand()
        {

            IsReadOnly = false;
            IsEditMode = true;
            IsEnableEdit = false;
            IsEnableSave = true;
            IsEnableSaveClose = true;
            IsEnableSaveNew = true;


            if (SequenceColl.Count > 0)
            {
                IsEnableCancelLine = false;
                IsEnableEditLine = true;
                IsEnableSaveLine = false;
                IsEnableRemoveLine = true;
            }
            IsEnableNewLine = true;
        }

        private void OnsaveAndCloseCommand(object obj)
        {
            OnSaveData();
            if (SaveResult)
            {
                ((SequenceView)obj).Close();
            }
            Global.Instance.IsModified = false;
        }

        private void OnClose(object obj)
        {
            if (obj != null)
            {

                if (Global.Instance.IsModified)
                {
                    MessageBoxResult result = MessageBox.Show(GeneralMessages.ChangesHaveMade, "", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.OK);

                    if (result == MessageBoxResult.Yes)
                    {
                        OnSaveData();
                        ((SequenceView)obj).Close();
                    }
                    else
                    {
                        ((SequenceView)obj).Close();
                    }
                }
                else
                {
                    MessageBoxResult result = MessageBox.Show(GeneralMessages.CloseWindow, "", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.OK);
                    if (result == MessageBoxResult.Yes)
                    {
                        ((SequenceView)obj).Close();
                    }

                }
            }
        }

        private void OnSaveAndNewCommand()
        {
            SaveAndNew = true;
            OnSaveData();
            Init();
            if (SaveResult)
            {
                IsEditMode = false;
                IsEnableEdit = false;
                IsEnableSave = true;
                IsEnableSaveClose = true;
                IsEnableSaveNew = true;
                SaveResult = false;
                Global.Instance.IsModified = false;
            }
            else
            {
                SaveAndNew = false;
            }
        }


        private void OnSaveData()
        {

            if (ValidateBeforeSave())
            {
                var sequence = new EntityModel.Sequence();
                var message = new StringBuilder();

                if (SequenceColl.Count > 0)
                    SequenceColl.ToList().ForEach(x => x.SeqHeaderId = objHdrSeq.SeqHeaderId);

                bool isSaved = SequenceManager.SaveData(SequenceColl, out sequence, ref message);
                if (isSaved)
                {
                    SaveResult = true;
                    if (!SaveAndNew)
                    {

                        DisableEditMode();
                    }
                    else
                    {
                        objHdrSeq = new SequenceHeader();
                        SequenceColl = new ObservableCollection<EntityModel.Sequence>();
                        objSeq = new SequenceModel();
                    }

                    MessageBox.Show(message.ToString());
                }
            }

        }

        private bool ValidateBeforeSave()
        {
            objSeq.Validate();
            OnPropertyChanged(() => objSeq);
            int cnt = 1;
            StringBuilder messasge = new StringBuilder();

            foreach (var item in objSeq._errors)
            {
                foreach (var i in item.Value)
                {
                    messasge.Append($"{cnt}. " + i + "\n");
                    cnt++;
                }

            }
            if (objSeq._errors.Count > 0)
            {
                MessageBox.Show(messasge.ToString());
                return false;
            }
            //bool IsDuplicate = CountryMasterManager.CheckDuplicate(objCountry.CountryCode, OldCountryCode);
            //if (IsDuplicate)
            //{
            //    messasge.Append($"{cnt}. " + "Contry Code is Already Exists.." + "\n");
            //    MessageBox.Show(messasge.ToString());
            //    return false;
            //}
            return true;
        }
        private void OnSeqSelectionChanged(object obj)
        {

            if (obj != null)
            {
                if (!IsEnableCancelLine)
                {
                    if (obj is EntityModel.Sequence)
                    {
                        var _obj = obj as EntityModel.Sequence;
                        objSeq.prefix = _obj.prefix;
                        objSeq.SeqId = _obj.SeqId;
                        objSeq.IsActive = _obj.IsActive;
                        objSeq.CompanyId = _obj.CompanyId;
                        objSeq.TotalLength = _obj.TotalLength;
                        objSeq.LastGeneratedNo = _obj.LastGeneratedNo ?? 0;
                        objSeq.SeqHeaderId = _obj.SeqHeaderId;
                        objSeq.StartRangeNo = _obj.StartRangeNo;
                        objSeq.Status = _obj.Status;
                        objSeq.StartDate = _obj.StartDate;
                        objSeq.EndDate = _obj.EndDate;
                    }
                }
            }
            if (!string.IsNullOrEmpty(objSeq.prefix))
                SeqPreview = SequenceGenerator.GetSequence(objSeq.LastGeneratedNo ?? 0, objSeq.TotalLength ?? 0, objSeq.prefix, objSeq.SeqId, objSeq.SeqHeaderId ?? 0);
            else
                SeqPreview = null;


            OnPropertyChanged(() => ModuleName);
            OnPropertyChanged(() => objSeq);
            Global.Instance.IsModified = false;
        }

        private void OnSeqHdrSelectionChanged(object obj)
        {
            if (obj != null)
            {
                if (obj is SequenceHeader)
                {
                    var _obj = obj as SequenceHeader;
                    ModuleName = _obj.ModuleName;
                    objHdrSeq.SeqHeaderId = _obj.SeqHeaderId;
                    OnPropertyChanged(() => SeqHeaderId);
                    SequenceColl = new ObservableCollection<EntityModel.Sequence>(_obj.Sequences);
                    OnSeqSelectionChanged(_obj.Sequences.FirstOrDefault((x => x.SeqHeaderId == _obj.SeqHeaderId)));
                }
                else
                {
                    ModuleName = null;
                    SequenceColl = new ObservableCollection<EntityModel.Sequence>();
                }
                OnPropertyChanged(() => SequenceColl);
                OnPropertyChanged(() => objHdrSeq);

                if (SequenceColl.Count > 0)
                {
                    IsEnableCancelLine = false;
                    IsEnableEditLine = true;
                    IsEnableNewLine = true;
                    IsEnableSaveLine = false;
                    IsEnableRemoveLine = true;
                }
                else
                {
                    IsEnableRemoveLine = false;
                    IsEnableEditLine = false;
                    IsEnableNewLine = true;
                }
            }

        }

        private void SetDefaultValue()
        {
            SequenceHdrColl = SequenceManager.GetSeqHeader();
        }
        #endregion

        #region Properties

        private bool _IsEnableSequence;

        public bool IsEnableSequence
        {
            get { return _IsEnableSequence; }
            set { _IsEnableSequence = value; OnPropertyChanged(() => IsEnableSequence); }
        }


        private int? _SeqHeaderId;

        public int? SeqHeaderId
        {
            get { return _SeqHeaderId; }
            set { _SeqHeaderId = value; OnPropertyChanged(() => SeqHeaderId); }
        }


        private string _ModuleName;
        public string ModuleName
        {
            get { return _ModuleName; }
            set { _ModuleName = value; OnPropertyChanged(() => ModuleName); }
        }

        private string _SeqPreview;
        public string SeqPreview
        {
            get { return _SeqPreview; }
            set { _SeqPreview = value; OnPropertyChanged(() => SeqPreview); }
        }


        private ObservableCollection<SequenceHeader> _sequenceHdrColl;
        public ObservableCollection<SequenceHeader> SequenceHdrColl
        {
            get { return _sequenceHdrColl; }
            set { _sequenceHdrColl = value; OnPropertyChanged(() => SequenceHdrColl); }
        }


        private ObservableCollection<EntityModel.Sequence> _sequenceColl;

        public ObservableCollection<EntityModel.Sequence> SequenceColl
        {
            get { return _sequenceColl; }
            set { _sequenceColl = value; OnPropertyChanged(() => SequenceColl); }
        }

        private bool _IsEnableNewLine;

        public bool IsEnableNewLine
        {
            get { return _IsEnableNewLine; }
            set { _IsEnableNewLine = value; OnPropertyChanged(() => IsEnableNewLine); }
        }

        private bool _IsEnableEditLine;

        public bool IsEnableEditLine
        {
            get { return _IsEnableEditLine; }
            set { _IsEnableEditLine = value; OnPropertyChanged(() => IsEnableEditLine); }
        }

        private bool _IsEnableSaveLine;

        public bool IsEnableSaveLine
        {
            get { return _IsEnableSaveLine; }
            set { _IsEnableSaveLine = value; OnPropertyChanged(() => IsEnableSaveLine); }
        }

        private bool _IsEnableRemoveLine;

        public bool IsEnableRemoveLine
        {
            get { return _IsEnableRemoveLine; }
            set { _IsEnableRemoveLine = value; OnPropertyChanged(() => IsEnableRemoveLine); }
        }

        private bool _IsEnableCancelLine;
        public bool IsEnableCancelLine
        {
            get { return _IsEnableCancelLine; }
            set { _IsEnableCancelLine = value; OnPropertyChanged(() => IsEnableCancelLine); }
        }
        private bool _IsReadOnly;
        public bool IsReadOnly
        {
            get { return _IsReadOnly; }
            set { _IsReadOnly = value; OnPropertyChanged(() => IsReadOnly); }
        }

        private bool _IsEnableControls;

        public bool IsEnableControls
        {
            get { return _IsEnableControls; }
            set { _IsEnableControls = value; OnPropertyChanged(() => IsEnableControls); }
        }


        private bool _SaveResult;
        public bool SaveResult
        {
            get { return _SaveResult; }
            set { _SaveResult = value; OnPropertyChanged(() => SaveResult); }
        }
        private bool _SaveAndNew;
        public bool SaveAndNew
        {
            get { return _SaveAndNew; }
            set { _SaveAndNew = value; OnPropertyChanged(() => SaveAndNew); }
        }

        private string _OldCountryCode;
        public string OldCountryCode
        {
            get { return _OldCountryCode; }
            set { _OldCountryCode = value; OnPropertyChanged(() => OldCountryCode); }
        }

        private bool _IsEditMode;
        public bool IsEditMode
        {
            get { return _IsEditMode; }
            set { _IsEditMode = value; OnPropertyChanged(() => IsEditMode); }
        }

        private bool _IsEnableEdit;
        public bool IsEnableEdit
        {
            get { return _IsEnableEdit; }
            set { _IsEnableEdit = value; OnPropertyChanged(() => IsEnableEdit); }
        }

        private bool _IsEnableSaveNew;
        public bool IsEnableSaveNew
        {
            get { return _IsEnableSaveNew; }
            set { _IsEnableSaveNew = value; OnPropertyChanged(() => IsEnableSaveNew); }
        }

        private bool _IsEnableSaveClose;
        public bool IsEnableSaveClose
        {
            get { return _IsEnableSaveClose; }
            set { _IsEnableSaveClose = value; OnPropertyChanged(() => IsEnableSaveClose); }
        }


        private bool _IsEnableSave;
        public bool IsEnableSave
        {
            get { return _IsEnableSave; }
            set { _IsEnableSave = value; OnPropertyChanged(() => IsEnableSave); }
        }

        private Model.SequenceModel _objSeq;
        public Model.SequenceModel objSeq
        {
            get { return _objSeq; }
            set { _objSeq = value; OnPropertyChanged(() => objSeq); }
        }
        private EntityModel.SequenceHeader _objHdrSeq;
        public EntityModel.SequenceHeader objHdrSeq
        {
            get { return _objHdrSeq; }
            set { _objHdrSeq = value; OnPropertyChanged(() => objHdrSeq); }
        }

        #endregion
    }
}
