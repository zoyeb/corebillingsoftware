﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using BillingSoftware.Command;
using BillingSoftware.EntityModel;
using BillingSoftware.HelperFunction;
using BillingSoftware.Manager;
using BillingSoftware.Model;
using BillingSoftware.UI.Masters;
using BillingSoftware.UI.Masters.Country;
using BillingSoftware.UI.Masters.ProductCategory;
using BillingSoftware.UI.Masters.Sequence;
using BillingSoftware.ViewModel.Masters.ProductCategory;
using ResourceFiles.Properties;

namespace BillingSoftware.ViewModel.Masters.Sequence
{
    class SequenceCollectionViewModel : ViewModelBase
    {

        #region Command
        private readonly DelegateCommand<object> newButtonCommand;
        public DelegateCommand<object> NewButtonCommand
        {
            get { return newButtonCommand; }
        }

        private DelegateCommand<object> closeButtonCommand;
        public DelegateCommand<object> CloseButtonCommand
        {
            get { return closeButtonCommand; }
        }

        private DelegateCommand<object> selectedSequenceHdr;
        public DelegateCommand<object> SelectedSequenceHdr
        {
            get { return selectedSequenceHdr; }
        }
        private DelegateCommand<object> deleteButtonCommand;
        public DelegateCommand<object> DeleteButtonCommand
        {
            get { return deleteButtonCommand; }
        }
        private DelegateCommand<object> _ViewButtonCommand;
        public DelegateCommand<object> ViewButtonCommand
        {
            get { return _ViewButtonCommand; }
        }

        #endregion Command

        #region Constructor
        public SequenceCollectionViewModel()
        {
            SequenceHdrColl = SequenceManager.GetSeqHeader();
            newButtonCommand = new DelegateCommand<object>((e) => { OnAddNewCountryDetails(); }, (e) => { return true; });
            closeButtonCommand = new DelegateCommand<object>((obj) => { OncloseButtonCommand(obj); });
            Predicate<object> CanSelectedCountry = delegate (object s) { if (((SequenceHeader)s) != null) { return true; } else { return false; } };
            selectedSequenceHdr = new DelegateCommand<object>((obj) => { OnselectedSeqHdrChanged(obj); }, CanSelectedCountry);
            _ViewButtonCommand = new DelegateCommand<object>((obj) => { OnselectedSeqHdrChanged(obj); }, CanSelectedCountry);
            deleteButtonCommand = new DelegateCommand<object>((obj) => OnDeleteButtonCommand(obj), CanSelectedCountry);
        }


        #endregion Constructor

        #region Functions

        private void OnDeleteButtonCommand(object obj)
        {
            if (obj is EntityModel.ProductType)
            {
                var _obj = obj as EntityModel.ProductType;
                MessageBoxResult result = MessageBox.Show(MasterMessages.DeleteMessage, MasterMessages.RecordDeleted, MessageBoxButton.YesNoCancel, MessageBoxImage.Question, MessageBoxResult.Yes, MessageBoxOptions.None);
                if (result == MessageBoxResult.Yes)
                {
                    try
                    {
                        if (_obj.ProductTypeId > 0)
                        {
                            bool IsDeleted = ProductCategoryManager.DeleteRecord(_obj);
                            if (IsDeleted)
                            {
                                RefreshGridOnClosed(null, null);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }

                }
            }
        }

        private void OnselectedSeqHdrChanged(object obj)
        {
            if (!Helpers.IsWindowOpen<CountryView>())
            {
                if (obj != null)
                {
                    var _obj = obj as SequenceHeader;
                    var view = new SequenceView();
                    var viewModel = new SequenceViewModel(_obj.SeqHeaderId, IsEditMode: true);
                    view.DataContext = viewModel;
                    view.Closed += RefreshGridOnClosed; ;
                    view.Show();
                }
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        private void OncloseButtonCommand(object obj)
        {
            MessageBoxResult result = MessageBox.Show(GeneralMessages.CloseWindow, "Country Collection", MessageBoxButton.YesNo, MessageBoxImage.Asterisk);
            if (result == MessageBoxResult.Yes)
            {
                ((SequenceCollectionView)obj).Close();
            }
        }
        private void OnAddNewCountryDetails()
        {
            if (!Helpers.IsWindowOpen<CountryView>())
            {
                var view = new SequenceView();
                var viewModel = new SequenceViewModel();
                view.DataContext = viewModel;
                view.Closed += RefreshGridOnClosed; ;
                view.Show();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        private void RefreshGridOnClosed(object sender, EventArgs e)
        {
            SequenceHdrColl = SequenceHdrColl = SequenceManager.GetSeqHeader();
            OnPropertyChanged(nameof(SequenceHdrColl));
        }

        #endregion Constructor

        #region Properties
        private ObservableCollection<SequenceHeader> _sequenceHdrColl;
        public ObservableCollection<SequenceHeader> SequenceHdrColl
        {
            get { return _sequenceHdrColl; }
            set { _sequenceHdrColl = value; OnPropertyChanged(nameof(SequenceHdrColl)); }
        }


        #endregion Properties
    }
}
