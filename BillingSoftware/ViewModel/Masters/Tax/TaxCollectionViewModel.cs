﻿using BillingSoftware.Command;
using BillingSoftware.HelperFunction;
using BillingSoftware.Manager;
using BillingSoftware.UI.Masters.Tax;
using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BillingSoftware.ViewModel.Masters.Tax
{
    public class TaxCollectionViewModel : NotifyPropertyChangedBase
    {

        #region Command
        private readonly DelegateCommand<object> newButtonCommand;
        public DelegateCommand<object> NewButtonCommand
        {
            get { return newButtonCommand; }
        }

        private DelegateCommand<object> closeButtonCommand;
        public DelegateCommand<object> CloseButtonCommand
        {
            get { return closeButtonCommand; }
        }

        private DelegateCommand<object> selectedTaxHeader;
        public DelegateCommand<object> SelectedTaxHeader
        {
            get { return selectedTaxHeader; }
        }
        private DelegateCommand<object> deleteButtonCommand;
        public DelegateCommand<object> DeleteButtonCommand
        {
            get { return deleteButtonCommand; }
        }
        #endregion Command

        #region Constructor
        public TaxCollectionViewModel()
        {
            newButtonCommand = new DelegateCommand<object>((e) => { OnNewTaxDetails(); }, (e) => { return true; });
            selectedTaxHeader = new DelegateCommand<object>((e) => { OnViewTaxDetails(e); }, (e) => { return true; });
            Init();
        }


        #endregion

        #region Methods

        private void OnNewTaxDetails()
        {
            if (!Helpers.IsWindowOpen<TaxView>())
            {
                var view = new TaxView();
                var viewModel = new TaxViewModel();
                view.DataContext = viewModel;
                view.Closed += RefreshGridOnClosed; ;
                view.Show();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }
        private void OnViewTaxDetails(object obj)
        {
            if (obj != null)
            {
                var _obj = obj as EntityModel.TaxHeader;
                if (_obj != null)
                {
                    if (!Helpers.IsWindowOpen<TaxView>())
                    {
                        var view = new TaxView();
                        var viewModel = new TaxViewModel(_obj.TaxHeaderId, true);
                        view.DataContext = viewModel;
                        view.Closed += RefreshGridOnClosed; ;
                        view.Show();
                    }
                    else
                    {
                        MessageBox.Show(GeneralMessages.WindowIsOpened);
                    }
                }
            }

        }

        private void RefreshGridOnClosed(object sender, EventArgs e)
        {
            TaxHeaders = TaxDetailsManager.GetTaxHeaders();
        }

        private void Init()
        {
            TaxHeaders = TaxDetailsManager.GetTaxHeaders();
            OnPropertyChanged(() => TaxHeaders);
        }
        #endregion

        #region Properties

        private ObservableCollection<EntityModel.TaxHeader> _TaxHeaders;
        public ObservableCollection<EntityModel.TaxHeader> TaxHeaders
        {
            get { return _TaxHeaders; }
            set { _TaxHeaders = value; OnPropertyChanged(() => TaxHeaders); }
        }
        #endregion

    }
}
