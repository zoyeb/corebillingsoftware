﻿using BillingSoftware.Command;
using BillingSoftware.Manager;
using BillingSoftware.Model;
using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BillingSoftware.ViewModel.Masters.Tax
{
    public class TaxViewModel : NotifyPropertyChangedBase
    {
        #region Declaration


        #region  LineCommand

        private DelegateCommand<object> newLineCommand;
        public DelegateCommand<object> NewLineCommand
        {
            get { return newLineCommand; }
        }

        private DelegateCommand<object> saveLineCommand;
        public DelegateCommand<object> SaveLineCommand
        {
            get { return saveLineCommand; }
        }

        private DelegateCommand<object> editLineCommand;
        public DelegateCommand<object> EditLineCommand
        {
            get { return editLineCommand; }
        }

        private DelegateCommand<object> cancelLineCommand;
        public DelegateCommand<object> CancelLineCommand
        {
            get { return cancelLineCommand; }
        }

        private DelegateCommand<object> removeLineCommand;
        public DelegateCommand<object> RemoveLineCommand
        {
            get { return removeLineCommand; }
        }
        #endregion


        private DelegateCommand<object> saveAndCloseCommand;
        public DelegateCommand<object> SaveAndCloseCommand
        {
            get { return saveAndCloseCommand; }
        }

        private DelegateCommand<object> _SaveCommand;
        public DelegateCommand<object> SaveCommand
        {
            get { return _SaveCommand; }
        }

        private DelegateCommand<object> _SaveAndNewCommand;
        public DelegateCommand<object> SaveAndNewCommand
        {
            get { return _SaveAndNewCommand; }
        }
        private DelegateCommand<object> _EditCommand;
        public DelegateCommand<object> EditCommand
        {
            get { return _EditCommand; }
        }

        private DelegateCommand<object> _CloseButtonCommand;
        public DelegateCommand<object> CloseButtonCommand
        {
            get { return _CloseButtonCommand; }
        }

        private DelegateCommand<object> _SelectionChanged;
        public DelegateCommand<object> SelectionChanged
        {
            get { return _SelectionChanged; }
        }



        #endregion

        #region Constructor
        public TaxViewModel()
        {
            Init();
            DisableEditMode();
        }

        public TaxViewModel(int taxHeaderId, bool IsEditMode)
        {
            Init();
            this.IsEditMode = IsEditMode;
            GetTaxDetails(taxHeaderId);
            EnableEditMode();
        }
        #endregion


        #region Methods
        private void SetDefaultValues()
        {
            Taxtypes = TaxDetailsManager.GetTaxTypes();
            TaxRates = TaxDetailsManager.GetTaxRates();
        }


        private void GetTaxDetails(int TaxHeaderId)
        {
            TaxDetailsColl = new ObservableCollection<Model.TaxDetailModel>();
            SaveTaxDetails = new ObservableCollection<Model.TaxDetailModel>();
            objTaxHeader = TaxDetailsManager.GetTaxHeaderById(TaxHeaderId);
            foreach (var taxDtl in objTaxHeader.TaxDetails)
            {
                SaveTaxDetails.Add(taxDtl);
                TaxDetailsColl.Add(taxDtl);
            }
            if (TaxDetailsColl.Count > 0)
            {
                OnSelectionChanged(TaxDetailsColl.FirstOrDefault(x => x.TaxHeaderId == objTaxHeader.TaxHeaderId));                 
            }
        }
        private void Init()
        {
            objTaxHeader = new Model.TaxHeaderModel { CreatedBy = Global.Instance.LoginUserName, CreatedDate = DateTime.Now, CompanyId = Global.Instance.CompanyId, Status = 1 };
            objTaxDetails = new Model.TaxDetailModel { CreatedBy = Global.Instance.LoginUserName, CreatedDate = DateTime.Now, CompanyId = Global.Instance.CompanyId, Status = 1 };
            TaxDetailsColl = new ObservableCollection<Model.TaxDetailModel>();
            SaveTaxDetails = new ObservableCollection<TaxDetailModel>();
            _SelectionChanged = new DelegateCommand<object>((s) => { OnSelectionChanged(s); }, (s) => { return true; });


            saveAndCloseCommand = new DelegateCommand<object>((obj) => { OnsaveAndCloseCommand(obj); }, (s) => true);
            _EditCommand = new DelegateCommand<object>((s) => { OnEditCommand(); }, (s) => { return true; });
            _SaveCommand = new DelegateCommand<object>((a) => { OnSaveData(); });
            _SaveAndNewCommand = new DelegateCommand<object>((obj) => OnSaveAndNewCommand());
            _CloseButtonCommand = new DelegateCommand<object>((obj) => { OnClose(obj); });

            //line Command
            Predicate<object> CanSelectedCountry = delegate (object s) { if (((Model.TaxDetailModel)s) != null) { return true; } else { return false; } };
            newLineCommand = new DelegateCommand<object>((s) => { OnNewLineCommand(); }, (s) => { return true; });
            saveLineCommand = new DelegateCommand<object>((s) => { OnSaveLineCommand(); }, (s) => { return true; });
            editLineCommand = new DelegateCommand<object>((s) => { OnEditLineCommand(s); }, CanSelectedCountry);
            cancelLineCommand = new DelegateCommand<object>((s) => { OnCancelLineCommand(s); }, (s) => { return true; });
            removeLineCommand = new DelegateCommand<object>((s) => { OnRemoveLineCommand(s); }, CanSelectedCountry);
            SetDefaultValues();
        }
        private bool ValidateBeforeSave()
        {
            objTaxHeader.Validate();
            OnPropertyChanged(() => objTaxHeader);
            int cnt = 1;
            StringBuilder messasge = new StringBuilder();

            foreach (var item in objTaxHeader._errors)
            {
                foreach (var i in item.Value)
                {
                    messasge.Append($"{cnt}. " + i + "\n");
                    cnt++;
                }

            }
            if (objTaxHeader._errors.Count > 0)
            {
                MessageBox.Show(messasge.ToString());
                return false;
            }
            //bool IsDuplicate = CountryMasterManager.CheckDuplicate(objCountry.CountryCode, OldCountryCode);
            //if (IsDuplicate)
            //{
            //    messasge.Append($"{cnt}. " + "Contry Code is Already Exists.." + "\n");
            //    MessageBox.Show(messasge.ToString());
            //    return false;
            //}
            return true;
        }
        private void OnSaveData()
        {
            foreach (var item in TaxDetailsColl)
            {
                if (SaveTaxDetails.Any(x => x.TaxDetailsId == item.TaxDetailsId))
                    SaveTaxDetails.Remove(item);

                SaveTaxDetails.Add(item);

            }

            if (ValidateBeforeSave())
            {
                var taxHeader = new EntityModel.TaxHeader();
                var message = new StringBuilder();

                bool isSaved = TaxDetailsManager.SaveData(TaxDetailsManager.ParseTaxHeader(objTaxHeader), SaveTaxDetails, out taxHeader, ref message);
                if (isSaved)
                {
                    SaveResult = true;
                    if (!SaveAndNew)
                    {
                        DisableEditMode();
                        TaxDetailsColl.Clear();
                        GetTaxDetails(taxHeader.TaxHeaderId);
                    }
                    MessageBox.Show(message.ToString());
                }
            }
        }

        private void OnsaveAndCloseCommand(object obj)
        {
            if (ValidateBeforeSave())
            {
                OnSaveData();
                if (SaveResult)
                {
                    ((Window)obj).Close();
                }
                Global.Instance.IsModified = false;
            }
            
        }

        private void OnSaveAndNewCommand()
        {
            SaveResult = false;
            OnSaveData();
            if (SaveResult)
            {
                Init();
                DisableEditMode();
            }
            else
            {
                SaveResult = false;
            }
        }

        private void OnClose(object obj)
        {
            if (obj != null)
            {
                if (Global.Instance.IsModified)
                {
                    MessageBoxResult result = MessageBox.Show(GeneralMessages.ChangesHaveMade, "", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.OK);

                    if (result == MessageBoxResult.Yes)
                    {
                        OnSaveData();
                        ((Window)obj).Close();
                    }
                    else
                    {
                        ((Window)obj).Close();
                    }
                }
                else
                {
                    MessageBoxResult result = MessageBox.Show(GeneralMessages.CloseWindow, "", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.OK);
                    if (result == MessageBoxResult.Yes)
                    {
                        ((Window)obj).Close();
                    }

                }
            }
        }

        private void OnSelectionChanged(object obj)
        {
            if (obj != null)
            {
                var _obj = obj as Model.TaxDetailModel;
                if (_obj != null)
                {
                    objTaxDetails = new Model.TaxDetailModel
                    {
                        TaxDetailsId = _obj.TaxDetailsId,
                        CreatedBy = _obj.CreatedBy,
                        CreatedDate = _obj.CreatedDate,
                        CompanyId = _obj.CompanyId,
                        TaxHeaderId = objTaxHeader.TaxHeaderId,
                        TaxTypeId = _obj.TaxTypeId,
                        TaxRateId = _obj.TaxRateId,
                        ModifiedBy = _obj.ModifiedBy,
                        ModifiedDate = _obj.ModifiedDate,
                        GstType = _obj.GstType,
                        TaxRate = _obj.TaxRate,
                        Status = 1
                    };
                }
            }
            OnPropertyChanged(() => objTaxDetails);
            OnPropertyChanged(() => TaxDetailsColl);
        }
        private void DisableEditMode()
        {
            IsEditMode = false;
            IsEnableEdit = false;
            IsEnableSave = true;
            IsEnableSaveClose = true;
            IsEnableSaveNew = true;
            IsEnableControls = false;
            IsEnableNewLine = true;
        }
        private void EnableEditMode()
        {
            IsReadOnly = true;
            IsEnableControls = false;
            IsEditMode = true;
            IsEnableEdit = true;
            IsEnableSave = false;
            IsEnableSaveClose = false;
            IsEnableSaveNew = false;

            IsEnableSaveLine = false;
            IsEnableNewLine = false;
            IsEnableCancelLine = false;
            IsEnableEditLine = false;
            IsEnableRemoveLine = false;
            Global.Instance.IsModified = false;
        }
        private void OnEditCommand()
        {

            IsReadOnly = false;
            IsEditMode = true;
            IsEnableEdit = false;
            IsEnableSave = true;
            IsEnableSaveClose = true;
            IsEnableSaveNew = true;


            if (TaxDetailsColl.Count > 0)
            {
                IsEnableCancelLine = false;
                IsEnableEditLine = true;
                IsEnableSaveLine = false;
                IsEnableRemoveLine = true;
            }
            IsEnableNewLine = true;
        }

        private void OnNewLineCommand()
        {
            objTaxDetails = new Model.TaxDetailModel
            {
                CreatedBy = Global.Instance.LoginUserName,
                CreatedDate = DateTime.Now,
                CompanyId = Global.Instance.CompanyId,
                Status = 1
            };
            IsEnableControls = true;
            IsEnableSaveLine = true;
            IsEnableCancelLine = true;
            IsEnableEditLine = false;
            IsEnableNewLine = false;
            IsEnableControls = true;
            OnPropertyChanged(() => objTaxDetails);
        }
        

        private void OnSaveLineCommand()
        {
            SetVitalDetails();
            if (objTaxDetails.TaxDetailsId > 0)
            {
                TaxDetailsColl.Remove(TaxDetailsColl.FirstOrDefault(x => x.TaxDetailsId == objTaxDetails.TaxDetailsId));
                TaxDetailsColl.Add(objTaxDetails);
            }
            else
            {
                TaxDetailsColl.Remove(TaxDetailsColl.FirstOrDefault(x => x.CreatedDate == objTaxDetails.CreatedDate));
                TaxDetailsColl.Add(objTaxDetails);
            }
            if (TaxDetailsColl.Count > 0)
            {
                IsEnableEditLine = true;
                IsEnableRemoveLine = true;
                IsEnableSaveLine = false;
                IsEnableCancelLine = false;
                IsEnableNewLine = true;
                IsEnableControls = false;
            }
        }

        private void SetVitalDetails()
        {
            if (objTaxDetails.TaxTypeId != null)
            {
                var taxTypeinfo = Taxtypes.Select(x => new { x.GstType, x.TaxTypeId }).FirstOrDefault(x => x.TaxTypeId == objTaxDetails.TaxTypeId);
                objTaxDetails.GstType = taxTypeinfo.GstType.ToString();
            }
            if (objTaxDetails.TaxRateId != null)
            {
                var taxRateInfo = TaxRates.Select(x => new { x.TaxRateId, x.TaxRate }).FirstOrDefault(x => x.TaxRateId == objTaxDetails.TaxRateId);
                objTaxDetails.TaxRate = Convert.ToDouble(taxRateInfo.TaxRate);
            }
        }

        private void OnEditLineCommand(object obj)
        {
            if (obj != null)
            {
                var _obj = obj as Model.TaxDetailModel;
                if (_obj != null)
                {
                    objTaxDetails = new Model.TaxDetailModel
                    {
                        TaxDetailsId = _obj.TaxDetailsId,
                        CreatedBy = _obj.CreatedBy,
                        CreatedDate = _obj.CreatedDate,
                        CompanyId = _obj.CompanyId,
                        TaxHeaderId = objTaxHeader.TaxHeaderId,
                        TaxTypeId = _obj.TaxTypeId,
                        TaxRateId = _obj.TaxRateId,
                        ModifiedBy = _obj.ModifiedBy,
                        GstType = _obj.GstType,
                        TaxRate = _obj.TaxRate,
                        ModifiedDate = _obj.ModifiedDate,
                        Status = 1
                    };
                }

                IsEnableEditLine = false;
                IsEnableRemoveLine = false;
                IsEnableSaveLine = true;
                IsEnableCancelLine = true;
                IsEnableNewLine = false;
                IsEnableControls = true;
            }

        }

        private void OnCancelLineCommand(object obj)
        {
            if (obj != null)
            {
                var _obj = obj as Model.TaxDetailModel;
                if (_obj != null)
                {
                    objTaxDetails = new Model.TaxDetailModel
                    {
                        CreatedBy = _obj.CreatedBy,
                        CreatedDate = _obj.CreatedDate,
                        CompanyId = _obj.CompanyId,
                        TaxHeaderId = objTaxHeader.TaxHeaderId,
                        TaxTypeId = _obj.TaxTypeId,
                        TaxRateId = _obj.TaxRateId,
                        ModifiedBy = _obj.ModifiedBy,
                        GstType = _obj.GstType,
                        TaxRate = _obj.TaxRate,
                        ModifiedDate = _obj.ModifiedDate,
                        Status = 1
                    };
                }
                IsEnableEditLine = true;
                IsEnableRemoveLine = true;
                IsEnableSaveLine = false;
                IsEnableCancelLine = false;
                IsEnableNewLine = true;
                IsEnableControls = false;
            }
        }

        private void OnRemoveLineCommand(object s)
        {
            if (objTaxDetails.TaxDetailsId > 0)
            {
                var taxDetail = TaxDetailsColl.FirstOrDefault(x => x.TaxDetailsId == objTaxDetails.TaxDetailsId);
                int index = TaxDetailsColl.IndexOf(taxDetail);
                TaxDetailsColl.RemoveAt(index);
                
                if (index != -1)
                {
                    index = SaveTaxDetails.IndexOf(taxDetail);
                    SaveTaxDetails.RemoveAt(index);
                    taxDetail.Status = 0;
                    SaveTaxDetails.Add(taxDetail);
                }                
            }
            else
            {
                TaxDetailsColl.Remove(TaxDetailsColl.FirstOrDefault(x => x.CreatedDate == objTaxDetails.CreatedDate));
                SaveTaxDetails.Remove(TaxDetailsColl.FirstOrDefault(x => x.CreatedDate == objTaxDetails.CreatedDate));
            }
            IsEnableControls = false;
        }

        #endregion

        #region Properties
        private ObservableCollection<EntityModel.TaxTypeMaster> _Taxtypes;
        public ObservableCollection<EntityModel.TaxTypeMaster> Taxtypes
        {
            get { return _Taxtypes; }
            set { _Taxtypes = value; OnPropertyChanged(() => Taxtypes); }
        }

        private ObservableCollection<EntityModel.TaxRateMaster> _TaxRates;
        public ObservableCollection<EntityModel.TaxRateMaster> TaxRates
        {
            get { return _TaxRates; }
            set { _TaxRates = value; OnPropertyChanged(() => TaxRates); }
        }

        private Model.TaxDetailModel _objTaxdetails;
        public Model.TaxDetailModel objTaxDetails
        {
            get { return _objTaxdetails; }
            set { _objTaxdetails = value; OnPropertyChanged(() => objTaxDetails); }
        }

        private Model.TaxHeaderModel _objTaxHeader;
        public Model.TaxHeaderModel objTaxHeader
        {
            get { return _objTaxHeader; }
            set { _objTaxHeader = value; OnPropertyChanged(() => objTaxHeader); }
        }

        private ObservableCollection<Model.TaxDetailModel> _TaxDetailsColl;
        public ObservableCollection<Model.TaxDetailModel> TaxDetailsColl
        {
            get { return _TaxDetailsColl; }
            set { _TaxDetailsColl = value; OnPropertyChanged(() => TaxDetailsColl); }
        }


        private ObservableCollection<Model.TaxDetailModel> _SaveTaxDetails;
        public ObservableCollection<Model.TaxDetailModel> SaveTaxDetails
        {
            get { return _SaveTaxDetails; }
            set { _SaveTaxDetails = value; OnPropertyChanged(() => SaveTaxDetails); }
        }
        private bool _IsEnableNewLine;

        public bool IsEnableNewLine
        {
            get { return _IsEnableNewLine; }
            set { _IsEnableNewLine = value; OnPropertyChanged(() => IsEnableNewLine); }
        }

        private bool _IsEnableEditLine;

        public bool IsEnableEditLine
        {
            get { return _IsEnableEditLine; }
            set { _IsEnableEditLine = value; OnPropertyChanged(() => IsEnableEditLine); }
        }

        private bool _IsEnableSaveLine;

        public bool IsEnableSaveLine
        {
            get { return _IsEnableSaveLine; }
            set { _IsEnableSaveLine = value; OnPropertyChanged(() => IsEnableSaveLine); }
        }

        private bool _IsEnableRemoveLine;

        public bool IsEnableRemoveLine
        {
            get { return _IsEnableRemoveLine; }
            set { _IsEnableRemoveLine = value; OnPropertyChanged(() => IsEnableRemoveLine); }
        }

        private bool _IsEnableCancelLine;
        public bool IsEnableCancelLine
        {
            get { return _IsEnableCancelLine; }
            set { _IsEnableCancelLine = value; OnPropertyChanged(() => IsEnableCancelLine); }
        }
        private bool _IsReadOnly;
        public bool IsReadOnly
        {
            get { return _IsReadOnly; }
            set { _IsReadOnly = value; OnPropertyChanged(() => IsReadOnly); }
        }
        private bool _IsEnableControls;

        public bool IsEnableControls
        {
            get { return _IsEnableControls; }
            set { _IsEnableControls = value; OnPropertyChanged(() => IsEnableControls); }
        }


        private bool _SaveResult;
        public bool SaveResult
        {
            get { return _SaveResult; }
            set { _SaveResult = value; OnPropertyChanged(() => SaveResult); }
        }
        private bool _SaveAndNew;
        public bool SaveAndNew
        {
            get { return _SaveAndNew; }
            set { _SaveAndNew = value; OnPropertyChanged(() => SaveAndNew); }
        }



        private bool _IsEditMode;
        public bool IsEditMode
        {
            get { return _IsEditMode; }
            set { _IsEditMode = value; OnPropertyChanged(() => IsEditMode); }
        }

        private bool _IsEnableEdit;
        public bool IsEnableEdit
        {
            get { return _IsEnableEdit; }
            set { _IsEnableEdit = value; OnPropertyChanged(() => IsEnableEdit); }
        }

        private bool _IsEnableSaveNew;
        public bool IsEnableSaveNew
        {
            get { return _IsEnableSaveNew; }
            set { _IsEnableSaveNew = value; OnPropertyChanged(() => IsEnableSaveNew); }
        }

        private bool _IsEnableSaveClose;
        public bool IsEnableSaveClose
        {
            get { return _IsEnableSaveClose; }
            set { _IsEnableSaveClose = value; OnPropertyChanged(() => IsEnableSaveClose); }
        }

        private bool _IsEnableSave;
        private int taxHeaderId;
        private bool v;

        public bool IsEnableSave
        {
            get { return _IsEnableSave; }
            set { _IsEnableSave = value; OnPropertyChanged(() => IsEnableSave); }
        }
        #endregion


    }
}
