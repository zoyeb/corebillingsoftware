﻿using BillingSoftware.Command;
using BillingSoftware.EntityModel;
using BillingSoftware.HelperFunction;
using BillingSoftware.Manager;
using BillingSoftware.Model;
using BillingSoftware.UI.Masters;
using BillingSoftware.UI.Masters.Country;
using ResourceFiles.Properties;
using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace BillingSoftware.ViewModel.Masters.Country
{
    public class CountryCollectionViewModel : ViewModelBase
    {
        #region Command
        private readonly DelegateCommand<object> newButtonCommand;
        public DelegateCommand<object> NewButtonCommand
        {
            get { return newButtonCommand; }
        }

        private DelegateCommand<object> closeButtonCommand;
        public DelegateCommand<object> CloseButtonCommand
        {
            get { return closeButtonCommand; }
        }

        private DelegateCommand<object> selectedCountry;
        public DelegateCommand<object> SelectedCountry
        {
            get { return selectedCountry; }
        }
        private DelegateCommand<object> deleteButtonCommand;
        public DelegateCommand<object> DeleteButtonCommand
        {
            get { return deleteButtonCommand; }
        }
        #endregion Command

        #region Constructor
        public CountryCollectionViewModel()
        {
            Countries = CountryMasterManager.GetCountryCollection(start, itemCount, true, out totalItems);
            newButtonCommand = new DelegateCommand<object>((e) => { OnAddNewCountryDetails(); }, (e) => { return true; });
            closeButtonCommand = new DelegateCommand<object>((obj) => { OncloseButtonCommand(obj); });
            Predicate<object> CanSelectedCountry = delegate (object s) { if (((EntityModel.Country)s) != null) { return true; } else { return false; } };
            selectedCountry = new DelegateCommand<object>((obj) => { OnselectedCountry(obj); }, CanSelectedCountry);
            deleteButtonCommand = new DelegateCommand<object>((obj) => OnDeleteButtonCommand(obj), CanSelectedCountry);
        }


        #endregion Constructor

        #region Functions

        private void OnDeleteButtonCommand(object obj)
        {
            if (obj is EntityModel.Country)
            {
                var _obj = obj as EntityModel.Country;
                MessageBoxResult result = MessageBox.Show(MasterMessages.DeleteMessage, MasterMessages.RecordDeleted, MessageBoxButton.YesNoCancel, MessageBoxImage.Question, MessageBoxResult.Yes, MessageBoxOptions.None);
                if (result == MessageBoxResult.Yes)
                {
                    try
                    {
                        if (_obj.CountryId > 0)
                        {
                            bool IsDeleted = CountryMasterManager.DeleteRecord(_obj);
                            if (IsDeleted)
                            {
                                RefreshGridOnClosed(null, null);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }

                }
            }
        }

        private void OnselectedCountry(object obj)
        {
            if (!Helpers.IsWindowOpen<CountryView>())
            {
                if (obj != null)
                {
                    EntityModel.Country _obj = obj as EntityModel.Country;
                    var view = new CountryView();
                    var viewModel = new CountryMasterViewModel(_obj.CountryId, IsEditMode: true);
                    view.DataContext = viewModel;
                    view.Closed += RefreshGridOnClosed; ;
                    view.Show();
                }
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        private void OncloseButtonCommand(object obj)
        {
            MessageBoxResult result = MessageBox.Show(GeneralMessages.CloseWindow, "Country Collection", MessageBoxButton.YesNo, MessageBoxImage.Asterisk);
            if (result == MessageBoxResult.Yes)
            {
                ((CountryCollectionView)obj).Close();
            }
        }
        private void OnAddNewCountryDetails()
        {
            if (!Helpers.IsWindowOpen<CountryView>())
            {
                var view = new CountryView();
                var viewModel = new CountryMasterViewModel();
                view.DataContext = viewModel;
                view.Closed += RefreshGridOnClosed; ;
                view.Show();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        private void RefreshGridOnClosed(object sender, EventArgs e)
        {
            Countries = CountryMasterManager.GetCountryCollection(start, itemCount, true, out totalItems);
            OnPropertyChanged(nameof(countries));
        }

        #endregion Constructor

        #region Properties
        private ObservableCollection<EntityModel.Country> countries;
        public ObservableCollection<EntityModel.Country> Countries
        {
            get { return countries; }
            set { countries = value; OnErrorsChanged(nameof(Countries)); }
        }

        #endregion Properties

        #region paging Data Grid

        #region Private Fields

        private int start = 0;

        private int itemCount = 2;

        private bool ascending = true;

        private int totalItems = 0;

        private DelegateCommand<object> firstCommand;
        private DelegateCommand<object> previousCommand;
        private DelegateCommand<object> nextCommand;
        private DelegateCommand<object> lastCommand;
        #endregion

        #region binding properites
        public int Start { get { return start + 1; } }
        public int End { get { return start + itemCount < totalItems ? start + itemCount : totalItems; } }
        public int TotalItems { get { return totalItems; } }       
        #endregion

        #region Commands For Paging
        public DelegateCommand<object> FirstCommand
        {
            get
            {
                if (firstCommand == null)
                {
                    firstCommand = new DelegateCommand<object>
                    (
                        param =>
                        {
                            start = 0;
                            Countries = CountryMasterManager.GetCountryCollection(start, itemCount, ascending, out totalItems);
                            OnPropertyChanged(nameof(Start));
                            OnPropertyChanged(nameof(End));
                            OnPropertyChanged(nameof(TotalItems));
                            OnPropertyChanged(nameof(countries));
                        },
                        param =>
                        {
                            return start - itemCount >= 0 ? true : false;
                        }
                    );
                }

                return firstCommand;
            }
        }
        public DelegateCommand<object> NextCommand
        {
            get
            {
                if (nextCommand == null)
                {
                    nextCommand = new DelegateCommand<object>
                    (
                        param =>
                        {
                            start += itemCount;
                            Countries = CountryMasterManager.GetCountryCollection(start, itemCount, ascending, out totalItems);
                            OnPropertyChanged(nameof(Start));
                            OnPropertyChanged(nameof(End));
                            OnPropertyChanged(nameof(TotalItems));
                            OnPropertyChanged(nameof(countries));
                        },
                        param =>
                        {
                            return start + itemCount < totalItems ? true : false;
                        }
                    );
                }

                return nextCommand;
            }
        }
        public DelegateCommand<object> PreviousCommand
        {
            get
            {
                if (previousCommand == null)
                {
                    previousCommand = new DelegateCommand<object>
                    (
                        param =>
                        {
                            start -= itemCount;
                            Countries = CountryMasterManager.GetCountryCollection(start, itemCount, ascending, out totalItems);
                            OnPropertyChanged(nameof(Start));
                            OnPropertyChanged(nameof(End));
                            OnPropertyChanged(nameof(TotalItems));
                            OnPropertyChanged(nameof(countries));
                        },
                        param =>
                        {
                            return start - itemCount >= 0 ? true : false;
                        }
                    );
                }

                return previousCommand;
            }
        }
        public DelegateCommand<object> LastCommand
        {
            get
            {
                if (lastCommand == null)
                {
                    lastCommand = new DelegateCommand<object>
                    (
                        param =>
                        {
                            start = (totalItems / itemCount - 1) * itemCount;
                            start += totalItems % itemCount == 0 ? 0 : itemCount;
                            Countries = CountryMasterManager.GetCountryCollection(start, itemCount, ascending, out totalItems);
                            OnPropertyChanged(nameof(Start));
                            OnPropertyChanged(nameof(End));
                            OnPropertyChanged(nameof(TotalItems));
                            OnPropertyChanged(nameof(countries));
                        },
                        param =>
                        {
                            return start + itemCount < totalItems ? true : false;
                        }
                    );
                }

                return lastCommand;
            }
        }
        #endregion

        #endregion
    }
}
