﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using BillingSoftware.Command;
using BillingSoftware.EntityModel;
using BillingSoftware.HelperFunction;
using BillingSoftware.Manager;
using BillingSoftware.Model;
using BillingSoftware.UI.Masters;
using BillingSoftware.UI.Masters.Country;
using BillingSoftware.UI.Masters.ProductCategory;
using ResourceFiles.Properties;

namespace BillingSoftware.ViewModel.Masters.ProductCategory
{
    public class ProductCategoryCollectionViewModel : ViewModelBase
    {
        #region Command
        private readonly DelegateCommand<object> newButtonCommand;
        public DelegateCommand<object> NewButtonCommand
        {
            get { return newButtonCommand; }
        }

        private DelegateCommand<object> closeButtonCommand;
        public DelegateCommand<object> CloseButtonCommand
        {
            get { return closeButtonCommand; }
        }

        private DelegateCommand<object> selectedProductCategory;
        public DelegateCommand<object> SelectedProductCategory
        {
            get { return selectedProductCategory; }
        }
        private DelegateCommand<object> deleteButtonCommand;
        public DelegateCommand<object> DeleteButtonCommand
        {
            get { return deleteButtonCommand; }
        }
        #endregion Command

        #region Constructor
        public ProductCategoryCollectionViewModel()
        {
            ProductCategories = ProductCategoryManager.GetProductCatCollection();
            newButtonCommand = new DelegateCommand<object>((e) => { OnAddNewCountryDetails(); }, (e) => { return true; });
            closeButtonCommand = new DelegateCommand<object>((obj) => { OncloseButtonCommand(obj); });
            Predicate<object> CanSelectedCountry = delegate (object s) { if (((ProductType)s) != null) { return true; } else { return false; } };
            selectedProductCategory = new DelegateCommand<object>((obj) => { OnselectedProductCategory(obj); }, CanSelectedCountry);
            deleteButtonCommand = new DelegateCommand<object>((obj) => OnDeleteButtonCommand(obj), CanSelectedCountry);
        }


        #endregion Constructor

        #region Functions

        private void OnDeleteButtonCommand(object obj)
        {
            if (obj is EntityModel.ProductType)
            {
                var _obj = obj as EntityModel.ProductType;
                MessageBoxResult result = MessageBox.Show(MasterMessages.DeleteMessage, MasterMessages.RecordDeleted, MessageBoxButton.YesNoCancel, MessageBoxImage.Question, MessageBoxResult.Yes, MessageBoxOptions.None);
                if (result == MessageBoxResult.Yes)
                {
                    try
                    {
                        if (_obj.ProductTypeId > 0)
                        {
                            bool IsDeleted = ProductCategoryManager.DeleteRecord(_obj);
                            if (IsDeleted)
                            {
                                RefreshGridOnClosed(null, null);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }

                }
            }
        }

        private void OnselectedProductCategory(object obj)
        {
            if (!Helpers.IsWindowOpen<CountryView>())
            {
                if (obj != null)
                {
                    var _obj = obj as ProductType;
                    var view = new ProductCategoryView();
                    var viewModel = new ProductCategoryViewModel(_obj.ProductTypeId, IsEditMode: true);
                    view.DataContext = viewModel;
                    view.Closed += RefreshGridOnClosed; ;
                    view.Show();
                }
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        private void OncloseButtonCommand(object obj)
        {
            MessageBoxResult result = MessageBox.Show(GeneralMessages.CloseWindow, "Country Collection", MessageBoxButton.YesNo, MessageBoxImage.Asterisk);
            if (result == MessageBoxResult.Yes)
            {
                ((ProductCategoryCollectionView)obj).Close();
            }
        }
        private void OnAddNewCountryDetails()
        {
            if (!Helpers.IsWindowOpen<CountryView>())
            {
                var view = new ProductCategoryView();
                var viewModel = new ProductCategoryViewModel();
                view.DataContext = viewModel;
                view.Closed += RefreshGridOnClosed; ;
                view.Show();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        private void RefreshGridOnClosed(object sender, EventArgs e)
        {
            ProductCategories = ProductCategoryManager.GetProductCatCollection();
            OnPropertyChanged(nameof(ProductCategories));
        }

        #endregion Constructor

        #region Properties
        private ObservableCollection<ProductType> productCategories;
        public ObservableCollection<ProductType> ProductCategories
        {
            get { return productCategories; }
            set { productCategories = value; OnErrorsChanged(nameof(ProductCategories)); }
        }

        #endregion Properties
    }
}
