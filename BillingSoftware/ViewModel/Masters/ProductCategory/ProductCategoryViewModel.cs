﻿using BillingSoftware.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using BillingSoftware.EntityModel;
using BillingSoftware.Manager;
using BillingSoftware.Model;
using BillingSoftware.UI.Masters;
using ResourceFiles.Properties;
using BillingSoftware.UI.Masters.ProductCategory;

namespace BillingSoftware.ViewModel.Masters.ProductCategory
{
    public class ProductCategoryViewModel : NotifyPropertyChangedBase
    {

        #region Declaration 


        private DelegateCommand<object> saveAndCloseCommand;
        public DelegateCommand<object> SaveAndCloseCommand
        {
            get { return saveAndCloseCommand; }
        }

        private DelegateCommand<object> _SaveCommand;
        public DelegateCommand<object> SaveCommand
        {
            get { return _SaveCommand; }
        }

        private DelegateCommand<object> _SaveAndNewCommand;
        public DelegateCommand<object> SaveAndNewCommand
        {
            get { return _SaveAndNewCommand; }
        }
        private DelegateCommand<object> _EditCommand;
        public DelegateCommand<object> EditCommand
        {
            get { return _EditCommand; }
        }

        private DelegateCommand<object> _CloseButtonCommand;
        public DelegateCommand<object> CloseButtonCommand
        {
            get { return _CloseButtonCommand; }
        }

        #endregion

        #region Constructor
        public ProductCategoryViewModel(int ProductTypeId, bool IsEditMode)
        {
            this.IsEditMode = IsEditMode;
            Init(editMode: IsEditMode);
            ObjProductType = ProductCategoryManager.GetProductCatById(ProductTypeId);
            OldProductTypeName = ObjProductType.ParentProductTypeName;
            EnableEditMode();
        }
        public ProductCategoryViewModel()
        {
            Init();
            DisableEditMode();
            Global.Instance.IsModified = false;
        }
        #endregion Constructor

        #region Functions
        private void DisableEditMode()
        {

            IsEditMode = false;
            IsEnableEdit = false;
            IsEnableSave = true;
            IsEnableSaveClose = true;
            IsEnableSaveNew = true;
        }
        private void EnableEditMode()
        {
            IsReadOnly = true;
            IsEditMode = true;
            IsEnableEdit = true;
            IsEnableSave = false;
            IsEnableSaveClose = false;
            IsEnableSaveNew = false;
            Global.Instance.IsModified = false;
        }

        private void Init(bool editMode = false)
        {
            ObjProductType = new ProductTypeModel { CreatedBy = Global.Instance.LoginUserName, CreatedDate = DateTime.Now, Status = 1, CompanyId = Global.Instance.CompanyId };
            _EditCommand = new DelegateCommand<object>((s) => { OnEditCommand(); }, (s) => { return true; });
            _SaveCommand = new DelegateCommand<object>((a) => { OnSaveData(); });
            _SaveAndNewCommand = new DelegateCommand<object>((obj) => OnSaveAndNewCommand());
            _CloseButtonCommand = new DelegateCommand<object>((obj) => { OnClose(obj); });
            saveAndCloseCommand = new DelegateCommand<object>((obj) => { OnsaveAndCloseCommand(obj); });
        }
        private void OnsaveAndCloseCommand(object obj)
        {
            OnSaveData();
            if (SaveResult)
            {
                ((ProductCategoryView)obj).Close();
            }
            Global.Instance.IsModified = false;
        }

        private void OnClose(object obj)
        {
            if (obj != null)
            {

                if (Global.Instance.IsModified)
                {
                    MessageBoxResult result = MessageBox.Show(GeneralMessages.ChangesHaveMade, "", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.OK);

                    if (result == MessageBoxResult.Yes)
                    {
                        OnSaveData();
                        ((ProductCategoryView)obj).Close();
                    }
                    else
                    {
                        ((ProductCategoryView)obj).Close();
                    }
                }
                else
                {
                    MessageBoxResult result = MessageBox.Show(GeneralMessages.CloseWindow, "", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.OK);
                    if (result == MessageBoxResult.Yes)
                    {
                        ((ProductCategoryView)obj).Close();
                    }

                }
            }
        }

        private void OnSaveAndNewCommand()
        {
            SaveAndNew = true;
            OnSaveData();
            Init();
            if (SaveResult)
            {
                IsEditMode = false;
                IsEnableEdit = false;
                IsEnableSave = true;
                IsEnableSaveClose = true;
                IsEnableSaveNew = true;
                SaveResult = false;
                Global.Instance.IsModified = false;
            }
            else
            {
                SaveAndNew = false;
            }
        }


        private void OnSaveData()
        {
            if (ValidateBeforeSave())
            {
                var productType = new ProductType();
                var message = new StringBuilder();
                bool isSaved = ProductCategoryManager.SaveData(ObjProductType, out productType, ref message);
                if (isSaved)
                {
                    SaveResult = true;
                    if (!SaveAndNew)
                    {
                        ObjProductType = new ProductTypeModel();
                        ObjProductType.CompanyId = productType.CompanyId;
                        ObjProductType.ParentProductTypeName = productType.ParentProductTypeName;
                        ObjProductType.ProductTypeDescription = productType.ProductTypeDescription;
                        OldProductTypeName = productType.ParentProductTypeName;
                        EnableEditMode();
                    }
                    MessageBox.Show(message.ToString());
                }
            }

        }

        private bool ValidateBeforeSave()
        {
            ObjProductType.Validate();
            OnPropertyChanged(() => ObjProductType);
            int cnt = 1;
            StringBuilder messasge = new StringBuilder();

            foreach (var item in ObjProductType._errors)
            {
                foreach (var i in item.Value)
                {
                    messasge.Append($"{cnt}. " + i + "\n");
                    cnt++;
                }

            }
            if (ObjProductType._errors.Count > 0)
            {
                MessageBox.Show(messasge.ToString());
                return false;
            }
            bool IsDuplicate = ProductCategoryManager.CheckDuplicate(_objProductType.ParentProductTypeName, OldProductTypeName);
            if (IsDuplicate)
            {
                messasge.Append($"{cnt}. " + "Product Name is Already Exists.." + "\n");
                MessageBox.Show(messasge.ToString());
                return false;
            }
            return true;
        }


        #endregion Functions

        #region Command

        private void OnEditCommand()
        {
            IsReadOnly = false;
            IsEditMode = true;
            IsEnableEdit = false;
            IsEnableSave = true;
            IsEnableSaveClose = true;
            IsEnableSaveNew = true;
        }


        #endregion Command  

        #region Properties


        private bool _IsReadOnly;
        public bool IsReadOnly
        {
            get { return _IsReadOnly; }
            set { _IsReadOnly = value; OnPropertyChanged(() => IsReadOnly); }
        }

        private bool _SaveResult;
        public bool SaveResult
        {
            get { return _SaveResult; }
            set { _SaveResult = value; OnPropertyChanged(() => SaveResult); }
        }
        private bool _SaveAndNew;
        public bool SaveAndNew
        {
            get { return _SaveAndNew; }
            set { _SaveAndNew = value; OnPropertyChanged(() => SaveAndNew); }
        }

        private string _OldProductTypeName;
        public string OldProductTypeName
        {
            get { return _OldProductTypeName; }
            set { _OldProductTypeName = value; OnPropertyChanged(() => OldProductTypeName); }
        }

        private bool _IsEditMode;
        public bool IsEditMode
        {
            get { return _IsEditMode; }
            set { _IsEditMode = value; OnPropertyChanged(() => IsEditMode); }
        }

        private bool _IsEnableEdit;
        public bool IsEnableEdit
        {
            get { return _IsEnableEdit; }
            set { _IsEnableEdit = value; OnPropertyChanged(() => IsEnableEdit); }
        }

        private bool _IsEnableSaveNew;
        public bool IsEnableSaveNew
        {
            get { return _IsEnableSaveNew; }
            set { _IsEnableSaveNew = value; OnPropertyChanged(() => IsEnableSaveNew); }
        }

        private bool _IsEnableSaveClose;
        public bool IsEnableSaveClose
        {
            get { return _IsEnableSaveClose; }
            set { _IsEnableSaveClose = value; OnPropertyChanged(() => IsEnableSaveClose); }
        }


        private bool _IsEnableSave;
        public bool IsEnableSave
        {
            get { return _IsEnableSave; }
            set { _IsEnableSave = value; OnPropertyChanged(() => IsEnableSave); }
        }


        private ProductTypeModel _objProductType;
        public ProductTypeModel ObjProductType
        {
            get { return _objProductType; }
            set { _objProductType = value; OnPropertyChanged(() => ObjProductType); }
        }

        #endregion

    }
}
