﻿using BillingSoftware.Command;
using BillingSoftware.HelperFunction;
using BillingSoftware.Manager;
using BillingSoftware.UI.Masters.City;
using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using BillingSoftware.Model;
using BillingSoftware.EntityModel;
using BillingSoftware.ViewModel.Masters.City;
using System.Collections.ObjectModel;
using BillingSoftware.UI.Masters.ProductStock;

namespace BillingSoftware.ViewModel.Masters.ProductStock
{
    class ProductStockCollectionViewModel : ViewModelBase
    {
        #region Declaration
        public DelegateCommand<object> NewButtonCommand { get; }
        public DelegateCommand<object> StockDetailSelectionChanged { get; }
        public DelegateCommand<object> DeleteButtonCommand { get; }
        public DelegateCommand<object> CloseButtonCommand { get; }
        #endregion

        #region Constructor
        public ProductStockCollectionViewModel()
        {
            NewButtonCommand = new DelegateCommand<object>((e) => { OnAddNewStockDetails(); }, (e) => { return true; });
            Predicate<object> predicate = new Predicate<object>(delegate (object obj) { if (((EntityModel.ProductStock)obj) != null) { return true; } else { return false; } });
            StockDetailSelectionChanged = new DelegateCommand<object>((obj) => OnStockDetailSelectionChanged(obj), predicate);
            DeleteButtonCommand = new DelegateCommand<object>((obj) => OnDeleteButtonCommand(obj), predicate);
            CloseButtonCommand = new DelegateCommand<object>((obj) => { OncloseButtonCommand(obj); });
            SetDefaultValues();
        }


        #endregion

        #region Methods
        private void SetDefaultValues()
        {
            ProductStocks = ProductStockManager.GetProductStockCollection();
        }
        private void OncloseButtonCommand(object obj)
        {
            MessageBoxResult result = MessageBox.Show(GeneralMessages.CloseWindow, "City Collection", MessageBoxButton.YesNo, MessageBoxImage.Asterisk);
            if (result == MessageBoxResult.Yes)
            {
                ((CityCollectionView)obj).Close();
            }
        }
        private void OnDeleteButtonCommand(object obj)
        {
            //if (obj is EntityModel.ProductStock _obj)
            //{                
            //    MessageBoxResult result = MessageBox.Show(MasterMessages.DeleteMessage, MasterMessages.RecordDeleted, MessageBoxButton.YesNoCancel, MessageBoxImage.Question, MessageBoxResult.Yes, MessageBoxOptions.None);
            //    if (result == MessageBoxResult.Yes)
            //    {
            //        try
            //        {
            //            if (_obj.ProductStockId > 0)
            //            {
            //                bool IsDeleted = CityMasterManager.DeleteRecord(_obj);
            //                if (IsDeleted)
            //                {
            //                    RefreshGridOnClosed(null, null);
            //                }

            //            }
            //        }
            //        catch (Exception)
            //        {

            //            throw;
            //        }
            //    }
            //}
        }

        private void OnStockDetailSelectionChanged(object obj)
        {
            if (obj != null)
            {
                if (obj is EntityModel.ProductStock _obj)
                {

                    var view = new ProductStockView();
                    var viewModel = new ProductStockViewModel(_obj.ProductStockId, IsEditMode: true);
                    view.DataContext = viewModel;
                    view.Closed += RefreshGridOnClosed;
                    view.Show();
                }
            }
        }

        private void OnAddNewStockDetails()
        {
            if (Helpers.IsWindowOpen<CityView>() == false)
            {
                var view = new ProductStockView();
                var viewModel = new ProductStockViewModel();
                view.DataContext = viewModel;
                view.Closed += RefreshGridOnClosed;
                view.Show();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        private void RefreshGridOnClosed(object sender, EventArgs e)
        {
            ProductStocks = ProductStockManager.GetProductStockCollection();
            OnPropertyChanged(nameof(ProductStocks));
        }

        #endregion

        #region properties

        private ObservableCollection<EntityModel.ProductStock> _ProductStocks;
        public ObservableCollection<EntityModel.ProductStock> ProductStocks
        {
            get { return _ProductStocks; }
            set { _ProductStocks = value; OnPropertyChanged(nameof(ProductStocks)); }
        }

        #endregion
    }
}
