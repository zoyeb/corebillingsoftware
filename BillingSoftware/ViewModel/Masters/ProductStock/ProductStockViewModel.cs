﻿using BillingSoftware.Command;
using BillingSoftware.Manager;
using BillingSoftware.Model;
using ResourceFiles.Properties;
using System;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using BillingSoftware.UI.Masters.City;
using BillingSoftware.UI.Masters.ProductStock;

namespace BillingSoftware.ViewModel.Masters.ProductStock
{
    public class ProductStockViewModel : NotifyPropertyChangedBase
    {
        #region Declartion

        public DelegateCommand<object> SaveCommand { get; private set; }
        public DelegateCommand<object> EditCommand { get; private set; }
        public DelegateCommand<object> SaveAndNewCommand { get; private set; }
        public DelegateCommand<object> CloseButtonCommand { get; private set; }
        public DelegateCommand<object> SaveAndCloseCommand { get; private set; }
        public DelegateCommand<object> SearchChanged { get; private set; }
        public DelegateCommand<object> ProductSelectionChanged { get; private set; }
        #endregion

        #region Constructor
        public ProductStockViewModel()
        {
            Init();
            DisableEditMode();
        }

        public ProductStockViewModel(int ProductStockId, bool IsEditMode)
        {
            this.IsEditMode = IsEditMode;
            Init();
            objStock = ProductStockManager.GetProductStockById(ProductStockId);
            if (objStock.ProductId > 0)
                InStockQty = ProductStockManager.GetProductTotalStockAvailable(objStock.ProductId);
            EnableEditMode();
        }

        #endregion

        #region Methods
        private void Init()
        {
            InStockQty = null;
            objStock = new ProductStockModel { CreatedBy = Global.Instance.LoginUserName, CreatedDate = DateTime.Now, Status = 1, CompanyId = Global.Instance.CompanyId };
            EditCommand = new DelegateCommand<object>((s) => { OnEditCommand(); }, (s) => { return true; });
            SaveCommand = new DelegateCommand<object>((a) => { OnSaveData(); });
            SaveAndNewCommand = new DelegateCommand<object>((a) => { OnSaveAndNewCommand(); });
            CloseButtonCommand = new DelegateCommand<object>((obj) => { OnClose(obj); });
            SaveAndCloseCommand = new DelegateCommand<object>((obj) => { OnsaveAndCloseCommand(obj); });
            ProductSelectionChanged = new DelegateCommand<object>((obj) => { OnProductSelectionChanged(obj); });

            SetDefaultValues();
        }

        private void OnProductSelectionChanged(object obj)
        {
            InStockQty = null;
            if (obj is EntityModel.Product _obj)
            {
                InStockQty = ProductStockManager.GetProductTotalStockAvailable(_obj.ProductId);
            }

        }

        private void OnsaveAndCloseCommand(object obj)
        {
            OnSaveData();
            if (SaveResult)
            {
                ((ProductStockView)obj).Close();
            }
            Global.Instance.IsModified = false;
        }
        private bool ValidateBeforeSave()
        {
            objStock.Validate();
            OnPropertyChanged(() => objStock);
            int cnt = 1;
            StringBuilder messasge = new StringBuilder();

            foreach (var item in objStock._errors)
            {
                foreach (var i in item.Value)
                {
                    messasge.Append($"{cnt}. " + i + "\n");
                    cnt++;
                }

            }
            if (objStock._errors.Count > 0)
            {
                MessageBox.Show(messasge.ToString());
                return false;
            }

            return true;
        }

        private void OnSaveData()
        {
            if (ValidateBeforeSave())
            {
                var message = new StringBuilder();
                bool isSave = ProductStockManager.SaveData(objStock, ref message, out var productStock);

                if (isSave)
                {
                    if (!SaveAndNew)
                    {
                        objStock = new ProductStockModel();
                        objStock.ProductStockId = productStock.ProductStockId;
                        objStock.ProductId = productStock.ProductId;
                        objStock.StockQty = productStock.StockQty;
                        objStock.StockDate = productStock.StockDate;
                        objStock.CreatedBy = productStock.CreatedBy;
                        objStock.ModifiedBy = Global.Instance.LoginUserName;
                        objStock.ModifiedDate = productStock.ModifiedDate;
                        objStock.Status = productStock.Status;
                        objStock.CompanyId = productStock.CompanyId;
                        OnPropertyChanged(() => objStock);
                        EnableEditMode();
                        if (objStock.ProductId > 0)
                            InStockQty = ProductStockManager.GetProductTotalStockAvailable(objStock.ProductId);

                    }
                    MessageBox.Show(message.ToString());
                }
            }
        }

        private void OnClose(object obj)
        {
            if (obj != null)
            {
                if (Global.Instance.IsModified)
                {
                    MessageBoxResult result = MessageBox.Show(GeneralMessages.ChangesHaveMade, "", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.OK);

                    if (result == MessageBoxResult.Yes)
                    {
                        OnSaveData();
                        ((ProductStockView)obj).Close();
                    }
                    else
                    {
                        ((ProductStockView)obj).Close();
                    }
                }
                else
                {
                    MessageBoxResult result = MessageBox.Show(GeneralMessages.CloseWindow, "", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.OK);
                    if (result == MessageBoxResult.Yes)
                    {
                        ((ProductStockView)obj).Close();
                    }
                }
            }
        }

        private void OnSaveAndNewCommand()
        {
            SaveAndNew = true;
            OnSaveData();
            Init();
            if (SaveResult)
            {
                IsEditMode = false;
                IsEnableEdit = false;
                IsEnableSave = true;
                IsEnableSaveClose = true;
                IsEnableSaveNew = true;
                SaveResult = false;
            }
        }

        private void SetDefaultValues()
        {
            ProductColl = ProductStockManager.GetProducts();

        }

        private void OnEditCommand()
        {
            IsReadOnly = false;
            IsEditMode = true;
            IsEnableEdit = false;
            IsEnableSave = true;
            IsEnableSaveClose = true;
            IsEnableSaveNew = true;
        }
        private void DisableEditMode()
        {

            IsEditMode = false;
            IsEnableEdit = false;
            IsEnableSave = true;
            IsEnableSaveClose = true;
            IsEnableSaveNew = true;
        }
        private void EnableEditMode()
        {
            IsReadOnly = true;
            IsEditMode = true;
            IsEnableEdit = true;
            IsEnableSave = false;
            IsEnableSaveClose = false;
            IsEnableSaveNew = false;
            Global.Instance.IsModified = false;
        }
        #endregion

        #region Properties
        private bool _isReadOnly;
        public bool IsReadOnly
        {
            get => _isReadOnly;
            set { _isReadOnly = value; OnPropertyChanged(() => IsReadOnly); }
        }

        private bool _saveResult;
        public bool SaveResult
        {
            get => _saveResult;
            set { _saveResult = value; OnPropertyChanged(() => SaveResult); }
        }
        private bool _saveAndNew;
        public bool SaveAndNew
        {
            get => _saveAndNew;
            set { _saveAndNew = value; OnPropertyChanged(() => SaveAndNew); }
        }

        private string _oldCityName;
        public string OldCityName
        {
            get => _oldCityName;
            set { _oldCityName = value; OnPropertyChanged(() => OldCityName); }
        }

        private bool _isEditMode;
        public bool IsEditMode
        {
            get => _isEditMode;
            set { _isEditMode = value; OnPropertyChanged(() => IsEditMode); }
        }

        private bool _isEnableEdit;
        public bool IsEnableEdit
        {
            get => _isEnableEdit;
            set { _isEnableEdit = value; OnPropertyChanged(() => IsEnableEdit); }
        }

        private bool _isEnableSaveNew;
        public bool IsEnableSaveNew
        {
            get => _isEnableSaveNew;
            set { _isEnableSaveNew = value; OnPropertyChanged(() => IsEnableSaveNew); }
        }

        private bool _isEnableSaveClose;
        public bool IsEnableSaveClose
        {
            get => _isEnableSaveClose;
            set { _isEnableSaveClose = value; OnPropertyChanged(() => IsEnableSaveClose); }
        }


        private bool _isEnableSave;
        public bool IsEnableSave
        {
            get => _isEnableSave;
            set { _isEnableSave = value; OnPropertyChanged(() => IsEnableSave); }
        }

        private ProductStockModel _objStock;
        public ProductStockModel objStock
        {
            get => _objStock;
            set { _objStock = value; OnPropertyChanged(() => objStock); }
        }

        private ObservableCollection<EntityModel.Product> _ProductColl;
        public ObservableCollection<EntityModel.Product> ProductColl
        {
            get => _ProductColl;
            set { _ProductColl = value; OnPropertyChanged(() => ProductColl); }
        }



        private string _countryId;
        public string CountryId
        {
            get => _countryId;
            set { _countryId = value; OnPropertyChanged(() => CountryId); }
        }

        private string _SearchState;

        public string SearchState
        {
            get => _SearchState;
            set { _SearchState = value; OnPropertyChanged(() => SearchState); }
        }

        private decimal? _InStockQty;
        public decimal? InStockQty
        {
            get => _InStockQty;
            set { _InStockQty = value; OnPropertyChanged(() => InStockQty); }
        }

        #endregion
    }
}
