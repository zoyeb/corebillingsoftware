﻿using BillingSoftware.Command;
using BillingSoftware.Manager;
using BillingSoftware.Model;
using ResourceFiles.Properties;
using System;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using BillingSoftware.UI.Masters.City;

namespace BillingSoftware.ViewModel.Masters.City
{
    class CityMasterViewModel : NotifyPropertyChangedBase
    {

        #region Declartion

        public DelegateCommand<object> SaveCommand { get; private set; }
        public DelegateCommand<object> EditCommand { get; private set; }
        public DelegateCommand<object> SaveAndNewCommand { get; private set; }
        public DelegateCommand<object> CloseButtonCommand { get; private set; }
        public DelegateCommand<object> SaveAndCloseCommand { get; private set; }

        public DelegateCommand<object> SearchChanged { get; private set; }
        #endregion

        #region Constructor
        public CityMasterViewModel()
        {
            Init();
            DisableEditMode();
        }

        public CityMasterViewModel(int CityId, bool EditMode)
        {
            this.IsEditMode = EditMode;
            Init();
            objCity = CityMasterManager.GetCityById(CityId);
            OldCityName = objCity.CityName;
            EnableEditMode();
        }

        #endregion

        #region Methods
        private void Init()
        {
            objCity = new CityModel { CreatedBy = Global.Instance.LoginUserName, CreatedDate = DateTime.Now, Status = 1, CompanyId = Global.Instance.CompanyId };
            EditCommand = new DelegateCommand<object>((s) => { OnEditCommand(); }, (s) => { return true; });
            SaveCommand = new DelegateCommand<object>((a) => { OnSaveData(); });
            SaveAndNewCommand = new DelegateCommand<object>((a) => { OnSaveAndNewCommand(); });
            CloseButtonCommand = new DelegateCommand<object>((obj) => { OnClose(obj); });
            SaveAndCloseCommand = new DelegateCommand<object>((obj) => { OnsaveAndCloseCommand(obj); });

            SearchChanged = new DelegateCommand<object>((obj) => { OnSearchChanged(obj); });
            SetDefaultValues();
        }

        private void OnSearchChanged(object obj)
        {
            if (obj is string _obj)
            {
                SearchState = _obj;
            }
            else
            {
                SearchState = string.Empty;
            }

        }

        private void OnsaveAndCloseCommand(object obj)
        {
            OnSaveData();
            if (SaveResult)
            {
                ((CityView)obj).Close();
            }
            Global.Instance.IsModified = false;
        }
        private bool ValidateBeforeSave()
        {
            objCity.Validate();
            OnPropertyChanged(() => objCity);
            int cnt = 1;
            StringBuilder messasge = new StringBuilder();

            foreach (var item in objCity._errors)
            {
                foreach (var i in item.Value)
                {
                    messasge.Append($"{cnt}. " + i + "\n");
                    cnt++;
                }

            }
            if (objCity._errors.Count > 0)
            {
                MessageBox.Show(messasge.ToString());
                return false;
            }
            bool isDuplicate = CityMasterManager.CheckDuplicate(objCity.CityName, OldCityName);
            if (isDuplicate)
            {
                messasge.Append($"{cnt}. " + MasterMessages.StateNameRequired + "\n");
                MessageBox.Show(messasge.ToString());
                return false;
            }
            return true;
        }

        private void OnSaveData()
        {
            if (ValidateBeforeSave())
            {
                var message = new StringBuilder();
                bool isSave = CityMasterManager.SaveData(objCity, ref message, out var tempCity);

                if (isSave)
                {
                    if (!SaveAndNew)
                    {
                        objCity = new CityModel();
                        objCity = CityMasterManager.GetCityById(tempCity.CityId);
                        OldCityName = objCity.CityName;
                        EnableEditMode();
                    }
                    MessageBox.Show(message.ToString());
                }
            }
        }

        private void OnClose(object obj)
        {
            if (obj != null)
            {
                if (Global.Instance.IsModified)
                {
                    MessageBoxResult result = MessageBox.Show(GeneralMessages.ChangesHaveMade, "", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.OK);

                    if (result == MessageBoxResult.Yes)
                    {
                        OnSaveData();
                        ((CityView)obj).Close();
                    }
                    else
                    {
                        ((CityView)obj).Close();
                    }
                }
                else
                {
                    MessageBoxResult result = MessageBox.Show(GeneralMessages.CloseWindow, "", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.OK);
                    if (result == MessageBoxResult.Yes)
                    {
                        ((CityView)obj).Close();
                    }
                }
            }
        }

        private void OnSaveAndNewCommand()
        {
            SaveAndNew = true;
            OnSaveData();
            Init();
            if (SaveResult)
            {
                IsEditMode = false;
                IsEnableEdit = false;
                IsEnableSave = true;
                IsEnableSaveClose = true;
                IsEnableSaveNew = true;
                SaveResult = false;
            }
        }

        private void SetDefaultValues()
        {
            StateColl = CityMasterManager.GetStates();
            CountryColl = StateMasterManager.GetCountires();
        }

        private void OnEditCommand()
        {
            IsReadOnly = false;
            IsEditMode = true;
            IsEnableEdit = false;
            IsEnableSave = true;
            IsEnableSaveClose = true;
            IsEnableSaveNew = true;
        }
        private void DisableEditMode()
        {

            IsEditMode = false;
            IsEnableEdit = false;
            IsEnableSave = true;
            IsEnableSaveClose = true;
            IsEnableSaveNew = true;
        }
        private void EnableEditMode()
        {
            IsReadOnly = true;
            IsEditMode = true;
            IsEnableEdit = true;
            IsEnableSave = false;
            IsEnableSaveClose = false;
            IsEnableSaveNew = false;
            Global.Instance.IsModified = false;
        }
        #endregion

        #region Properties
        private bool _isReadOnly;
        public bool IsReadOnly
        {
            get => _isReadOnly;
            set { _isReadOnly = value; OnPropertyChanged(() => IsReadOnly); }
        }

        private bool _saveResult;
        public bool SaveResult
        {
            get => _saveResult;
            set { _saveResult = value; OnPropertyChanged(() => SaveResult); }
        }
        private bool _saveAndNew;
        public bool SaveAndNew
        {
            get { return _saveAndNew; }
            set { _saveAndNew = value; OnPropertyChanged(() => SaveAndNew); }
        }

        private string _oldCityName;
        public string OldCityName
        {
            get { return _oldCityName; }
            set { _oldCityName = value; OnPropertyChanged(() => OldCityName); }
        }

        private bool _isEditMode;
        public bool IsEditMode
        {
            get { return _isEditMode; }
            set { _isEditMode = value; OnPropertyChanged(() => IsEditMode); }
        }

        private bool _isEnableEdit;
        public bool IsEnableEdit
        {
            get => _isEnableEdit;
            set { _isEnableEdit = value; OnPropertyChanged(() => IsEnableEdit); }
        }

        private bool _isEnableSaveNew;
        public bool IsEnableSaveNew
        {
            get => _isEnableSaveNew;
            set { _isEnableSaveNew = value; OnPropertyChanged(() => IsEnableSaveNew); }
        }

        private bool _isEnableSaveClose;
        public bool IsEnableSaveClose
        {
            get => _isEnableSaveClose;
            set { _isEnableSaveClose = value; OnPropertyChanged(() => IsEnableSaveClose); }
        }


        private bool _isEnableSave;
        public bool IsEnableSave
        {
            get => _isEnableSave;
            set { _isEnableSave = value; OnPropertyChanged(() => IsEnableSave); }
        }

        private CityModel _objCity;
        public CityModel objCity
        {
            get => _objCity;
            set { _objCity = value; OnPropertyChanged(() => objCity); }
        }


        private StateModel _objState;
        public StateModel objState
        {
            get => _objState;
            set { _objState = value; OnPropertyChanged(() => objState); }
        }

        private ObservableCollection<EntityModel.State> _StateColl;
        public ObservableCollection<EntityModel.State> StateColl
        {
            get => _StateColl;
            set { _StateColl = value; OnPropertyChanged(() => StateColl); }
        }

        private ObservableCollection<EntityModel.Country> _countryColl;
        public ObservableCollection<EntityModel.Country> CountryColl
        {
            get => _countryColl;
            set { _countryColl = value; OnPropertyChanged(() => CountryColl); }
        }

        private string _countryId;
        public string CountryId
        {
            get { return _countryId; }
            set { _countryId = value; OnPropertyChanged(() => CountryId); }
        }

        private string _SearchState;

        public string SearchState
        {
            get { return _SearchState; }
            set { _SearchState = value; OnPropertyChanged(() => SearchState); }
        }


        #endregion
    }
}
