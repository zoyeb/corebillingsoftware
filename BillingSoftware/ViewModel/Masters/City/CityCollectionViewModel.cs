﻿using BillingSoftware.Command;
using BillingSoftware.HelperFunction;
using BillingSoftware.Manager;
using BillingSoftware.UI.Masters.City;
using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using BillingSoftware.Model;
using BillingSoftware.EntityModel;

namespace BillingSoftware.ViewModel.Masters.City
{
    class CityCollectionViewModel : ViewModelBase
    {
        #region Declaration
        private readonly DelegateCommand<object> newButtonCommand;
        public DelegateCommand<object> NewButtonCommand
        {
            get { return newButtonCommand; }
        }

        private DelegateCommand<object> citySelectionChanged;
        public DelegateCommand<object> CitySelectionChanged
        {
            get { return citySelectionChanged; }
        }

        private DelegateCommand<object> deleteButtonCommand;
        public DelegateCommand<object> DeleteButtonCommand
        {
            get { return deleteButtonCommand; }
        }
        private DelegateCommand<object> closeButtonCommand;
        public DelegateCommand<object> CloseButtonCommand
        {
            get { return closeButtonCommand; }
        }
        #endregion

        #region Constructor
        public CityCollectionViewModel()
        {            
            newButtonCommand = new DelegateCommand<object>((e) => { OnAddNewCityDetails(); }, (e) => { return true; });
            Predicate<object> predicate = new Predicate<object>(delegate (object obj) { if (((EntityModel.City)obj) != null) { return true; } else { return false; } });
            citySelectionChanged = new DelegateCommand<object>((obj) => OnCitySelectionChanged(obj), predicate);
            deleteButtonCommand = new DelegateCommand<object>((obj) => OnDeleteButtonCommand(obj), predicate);
            closeButtonCommand = new DelegateCommand<object>((obj) => { OncloseButtonCommand(obj); });
            SetDefaultValues();
        }

         
        #endregion

        #region Methods
        private void SetDefaultValues()
        {
            Cities = CityMasterManager.GetCities();

        }
        private void OncloseButtonCommand(object obj)
        {
            MessageBoxResult result = MessageBox.Show(GeneralMessages.CloseWindow, "City Collection", MessageBoxButton.YesNo, MessageBoxImage.Asterisk);
            if (result == MessageBoxResult.Yes)
            {
                ((CityCollectionView)obj).Close();
            }
        }
        private void OnDeleteButtonCommand(object obj)
        {
            if (obj is EntityModel.City)
            {
                var _obj = obj as EntityModel.City;
                MessageBoxResult result = MessageBox.Show(MasterMessages.DeleteMessage, MasterMessages.RecordDeleted, MessageBoxButton.YesNoCancel, MessageBoxImage.Question, MessageBoxResult.Yes, MessageBoxOptions.None);
                if (result == MessageBoxResult.Yes)
                {
                    try
                    {
                        if (_obj.CityId > 0)
                        {
                            bool IsDeleted = CityMasterManager.DeleteRecord(_obj);
                            if (IsDeleted)
                            {
                                RefreshGridOnClosed(null, null);
                            }
                                 
                        }
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }
            }
        }

        private void OnCitySelectionChanged(object obj)
        {
            if (obj != null)
            {
                if (obj is EntityModel.City)
                {
                    var _obj = obj as EntityModel.City;
                    var view = new CityView();
                    var viewModel = new CityMasterViewModel(_obj.CityId, EditMode: true);
                    view.DataContext = viewModel;
                    view.Closed += RefreshGridOnClosed;
                    view.Show();
                }
            }
        }

        private void OnAddNewCityDetails()
        {
            if (Helpers.IsWindowOpen<CityView>() == false)
            {
                var view = new CityView();
                var viewModel = new CityMasterViewModel();
                view.DataContext = viewModel;
                view.Closed += RefreshGridOnClosed;
                view.Show();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        private void RefreshGridOnClosed(object sender, EventArgs e)
        {
            Cities = CityMasterManager.GetCities();
            OnPropertyChanged(nameof(Cities));
        }

        #endregion

        #region properties

        private List<EntityModel.City> _Cities;
        public List<EntityModel.City> Cities
        {
            get { return _Cities; }
            set { _Cities = value; }
        }

        #endregion
    }
}
