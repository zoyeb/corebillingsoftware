﻿using BillingSoftware.Command;
using BillingSoftware.HelperFunction;
using BillingSoftware.Manager;
using BillingSoftware.Model;
using BillingSoftware.UI.Masters.Customer;
using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BillingSoftware.ViewModel.Masters.Customer
{
    public class CustomerCollectionViewModel : ViewModelBase
    {

        #region Command
        private readonly DelegateCommand<object> newButtonCommand;
        public DelegateCommand<object> NewButtonCommand
        {
            get { return newButtonCommand; }
        }

        private DelegateCommand<object> closeButtonCommand;
        public DelegateCommand<object> CloseButtonCommand
        {
            get { return closeButtonCommand; }
        }

        
        private DelegateCommand<object> deleteButtonCommand;
        public DelegateCommand<object> DeleteButtonCommand
        {
            get { return deleteButtonCommand; }
        }
        private DelegateCommand<object> selectedCustomer;
        public DelegateCommand<object> SelectedCustomer
        {
            get { return selectedCustomer; }
        }
        #endregion Command

        #region Constructor
        public CustomerCollectionViewModel()
        {
            Customers = CustomerMasterManager.GetCustomerCollection();
            //new command
            newButtonCommand = new DelegateCommand<object>((e) => { OnAddNewCustomerInfo(); }, (e) => { return true; });
            //close command
            closeButtonCommand = new DelegateCommand<object>((e) => { OnCloseButtonCommand(e); }, (e) => { return true; });            
            //can edit
            Predicate<object> selectedCust = delegate (object obj) { if (((EntityModel.Customer)obj) != null) return true; else return false; };
            //delete command
            deleteButtonCommand = new DelegateCommand<object>((e) => OnDeleteButtonCommand(e), selectedCust);
            //selected customer
            selectedCustomer = new DelegateCommand<object>((e) => OnSelectedCustomer(e), selectedCust);
        }

        #region Methods
        private void OnSelectedCustomer(object e)
        {
            if (e != null)
            {
                if (e is EntityModel.Customer)
                {
                    var obj = e as EntityModel.Customer;
                    if (!Helpers.IsWindowOpen<CustomerView>())
                    {
                        var view = new CustomerView();
                        var viewModel = new CustomerViewModel(obj.CustId, IsEditMode: true);
                        view.DataContext = viewModel;
                        view.Closed += RefreshGridOnClosed; ;
                        view.Show();
                    }
                    else
                    {
                        MessageBox.Show(GeneralMessages.WindowIsOpened);
                    }
                }
            }
        }
        private void OnDeleteButtonCommand(object obj)
        {
            if (obj is EntityModel.Customer)
            {
                var _obj = obj as EntityModel.Customer;
                MessageBoxResult result = MessageBox.Show(MasterMessages.DeleteMessage, MasterMessages.RecordDeleted, MessageBoxButton.YesNoCancel, MessageBoxImage.Question, MessageBoxResult.Yes, MessageBoxOptions.None);
                if (result == MessageBoxResult.Yes)
                {
                    try
                    {
                        if (_obj.CountryId > 0)
                        {
                            bool IsDeleted = CustomerMasterManager.DeleteRecord(_obj);
                            if (IsDeleted)
                            {
                                RefreshGridOnClosed(null, null);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }

                }
            }
        }
        private void OnCloseButtonCommand(object obj)
        {
            MessageBoxResult result = MessageBox.Show(GeneralMessages.CloseWindow, "Customer Collection", MessageBoxButton.YesNo, MessageBoxImage.Asterisk);
            if (result == MessageBoxResult.Yes)
            {
                ((CustomerCollectionView)obj).Close();
            }
        }
        private void OnAddNewCustomerInfo()
        {
            if (!Helpers.IsWindowOpen<CustomerView>())
            {
                var view = new CustomerView();
                var viewModel = new CustomerViewModel();
                view.DataContext = viewModel;
                view.Closed += RefreshGridOnClosed; ;
                view.Show();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        private void RefreshGridOnClosed(object sender, EventArgs e)
        {
            Customers = CustomerMasterManager.GetCustomerCollection();
            OnPropertyChanged(nameof(Customers));
        }
        #endregion

        #endregion

        #region Properties
        private ObservableCollection<EntityModel.Customer> _Customers;
        public ObservableCollection<EntityModel.Customer> Customers
        {
            get { return _Customers; }
            set { _Customers = value; OnErrorsChanged(nameof(Customers)); }
        }

        #endregion



    }
}
