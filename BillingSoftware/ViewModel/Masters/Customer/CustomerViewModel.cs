﻿using BillingSoftware.Command;
using BillingSoftware.Manager;
using BillingSoftware.Model;
using BillingSoftware.UI.Masters.Customer;
using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BillingSoftware.ViewModel.Masters.Customer
{
    public class CustomerViewModel : NotifyPropertyChangedBase
    {


        #region Command

        #region Basic Command Declartion
        private DelegateCommand<object> _SaveCommand;
        public DelegateCommand<object> SaveCommand
        {
            get { return _SaveCommand; }
        }
        private DelegateCommand<object> _EditCommand;
        public DelegateCommand<object> EditCommand
        {
            get { return _EditCommand; }
        }
        private DelegateCommand<object> _CloseButtonCommand;
        public DelegateCommand<object> CloseButtonCommand
        {
            get { return _CloseButtonCommand; }
        }
        private DelegateCommand<object> saveAndCloseCommand;
        public DelegateCommand<object> SaveAndCloseCommand
        {
            get { return saveAndCloseCommand; }
        }
        private DelegateCommand<object> _SaveAndNewCommand;
        public DelegateCommand<object> SaveAndNewCommand
        {
            get { return _SaveAndNewCommand; }
        }

        #endregion


        public DelegateCommand<object> CitySelectionChanged { get; private set; }


        #endregion
        public CustomerViewModel()
        {
            Init(IsEditMode = false);
            DisableEditMode();
        }
        public CustomerViewModel(int CustId, bool IsEditMode)
        {
            this.IsEditMode = IsEditMode;
            Init(IsEditMode);
            EnableEditMode();
            objCustomer = CustomerMasterManager.GetCustomerByCustomerId(CustId);
        }
        public CustomerViewModel(int CustId, bool IsEditMode , bool ViewOnlyMode)
        {
            this.IsEditMode = IsEditMode;
            Init(IsEditMode);
            EnableEditMode();
            objCustomer = CustomerMasterManager.GetCustomerByCustomerId(CustId);
            IsEnableEdit = false;

        }


        #region Functions
        private void Init(bool IsEditMode)
        {
            objCustomer = new CustomerModel { CreatedBy = Global.Instance.LoginUserName, CreatedDate = DateTime.Now, Gender = true, CompanyId = Global.Instance.CompanyId, Status = 1 };
            _SaveCommand = new DelegateCommand<object>((a) => { OnSaveData(); });
            _EditCommand = new DelegateCommand<object>((obj) => { OnEditCommand(); });
            _CloseButtonCommand = new DelegateCommand<object>((obj) => { OnClose(obj); });
            _SaveAndNewCommand = new DelegateCommand<object>((obj) => OnSaveAndNewCommand());
            saveAndCloseCommand = new DelegateCommand<object>((obj) => { OnsaveAndCloseCommand(obj); });
            SetDefaultValues();
            CitySelectionChanged = new DelegateCommand<object>((e) => { OnCitySelectionChanged(e); }, (e) => { return true; });

        }

        private void OnsaveAndCloseCommand(object obj)
        {
            OnSaveData();
            if (SaveResult)
            {
                ((CustomerView)obj).Close();
            }
            Global.Instance.IsModified = false;
        }

        private void OnSaveAndNewCommand()
        {
            SaveAndNew = true;
            OnSaveData();
            Init(false);

            if (SaveResult)
            {
                DisableEditMode();
                SaveResult = false;
            }
            else
            {
                SaveAndNew = false;
            }
        }

        private void OnClose(object obj)
        {
            if (obj != null)
            {
                if (Global.Instance.IsModified)
                {
                    MessageBoxResult result = MessageBox.Show(GeneralMessages.ChangesHaveMade, "", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.OK);

                    if (result == MessageBoxResult.Yes)
                    {
                        OnSaveData();
                        ((CustomerView)obj).Close();
                    }
                    else
                    {
                        ((CustomerView)obj).Close();
                    }
                }
                else
                {
                    MessageBoxResult result = MessageBox.Show(GeneralMessages.CloseWindow, "", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.OK);
                    if (result == MessageBoxResult.Yes)
                    {
                        ((CustomerView)obj).Close();
                    }

                }
            }
        }

        private void OnEditCommand()
        {
            IsReadOnly = false;
            IsEditMode = false;
            IsEnableEdit = false;
            IsEnableSave = true;
            IsEnableSaveClose = true;
            IsEnableSaveNew = true;
        }

        private void OnSaveData()
        {
            if (ValidateBeforeSave())
            {
                var message = new StringBuilder();
                var TempCust = new EntityModel.Customer();
                bool isSave = CustomerMasterManager.SaveData(objCustomer, SelectedSequence, out TempCust, ref message);

                if (isSave)
                {
                    SaveResult = true;
                    if (!SaveAndNew)
                    {
                        objCustomer = new CustomerModel();
                        objCustomer = CustomerMasterManager.GetCustomerByCustomerId(TempCust.CustId);
                        EnableEditMode();
                    }
                    MessageBox.Show(message.ToString());
                }
            }
        }
        private bool ValidateBeforeSave()
        {
            objCustomer.Validate();
            OnPropertyChanged(() => objCustomer);
            int cnt = 1;
            StringBuilder messasge = new StringBuilder();

            foreach (var item in objCustomer._errors)
            {
                foreach (var i in item.Value)
                {
                    messasge.Append($"{cnt}. " + i + "\n");
                    cnt++;
                }

            }
            if (objCustomer._errors.Count > 0)
            {
                MessageBox.Show(messasge.ToString());
                return false;
            }
            //bool IsDuplicate = StateMasterManager.CheckDuplicate(objState.StateName, OldStateName);
            //if (IsDuplicate)
            //{
            //    messasge.Append($"{cnt}. " + MasterMessages.StateNameRequired + "\n");
            //    MessageBox.Show(messasge.ToString());
            //    return false;
            //}
            return true;
        }
        private void OnCitySelectionChanged(object e)
        {
            if (e != null)
            {
                var _obj = e as EntityModel.City;
                if (_obj != null)
                {
                    objCustomer.CityId = _obj.CityId;
                    objCustomer.StateId = _obj.State.StateId;
                    objCustomer.CountryId = _obj.State.Country.CountryId;
                }
                else
                {
                    objCustomer.CityId = null;
                    objCustomer.StateId = null;
                    objCustomer.CountryId = null;
                }
                OnPropertyChanged(() => objCustomer);
            }
        }

        private void SetDefaultValues()
        {
            StateColl = CityMasterManager.GetStates();
            CountryColl = StateMasterManager.GetCountires();
            Cities = CityMasterManager.GetCities();
            SequenceColl = CustomerMasterManager.GetSequence(Global.MasterModule, Global.CustomerSeqForm);
        }
        private void DisableEditMode()
        {
            VisibleCustCode = Visibility.Visible;
            IsEditMode = false;
            IsEnableEdit = false;
            IsEnableSave = true;
            IsEnableSaveClose = true;
            IsEnableSaveNew = true;
        }
        private void EnableEditMode()
        {
            VisibleCustCode = Visibility.Collapsed;
            IsReadOnly = true;
            IsEditMode = true;
            IsEnableEdit = true;
            IsEnableSave = false;
            IsEnableSaveClose = false;
            IsEnableSaveNew = false;
            Global.Instance.IsModified = false;
        }
        #endregion

        #region Properties


        private bool _IsReadOnly;
        public bool IsReadOnly
        {
            get { return _IsReadOnly; }
            set { _IsReadOnly = value; OnPropertyChanged(() => IsReadOnly); }
        }

        private bool _SaveResult;
        public bool SaveResult
        {
            get { return _SaveResult; }
            set { _SaveResult = value; OnPropertyChanged(() => SaveResult); }
        }
        private bool _SaveAndNew;
        public bool SaveAndNew
        {
            get { return _SaveAndNew; }
            set { _SaveAndNew = value; OnPropertyChanged(() => SaveAndNew); }
        }

        private string _OldCountryCode;
        public string OldCountryCode
        {
            get { return _OldCountryCode; }
            set { _OldCountryCode = value; OnPropertyChanged(() => OldCountryCode); }
        }

        private bool _IsEditMode;
        public bool IsEditMode
        {
            get { return _IsEditMode; }
            set { _IsEditMode = value; OnPropertyChanged(() => IsEditMode); }
        }

        private bool _IsEnableEdit;
        public bool IsEnableEdit
        {
            get { return _IsEnableEdit; }
            set { _IsEnableEdit = value; OnPropertyChanged(() => IsEnableEdit); }
        }

        private bool _IsEnableSaveNew;
        public bool IsEnableSaveNew
        {
            get { return _IsEnableSaveNew; }
            set { _IsEnableSaveNew = value; OnPropertyChanged(() => IsEnableSaveNew); }
        }

        private bool _IsEnableSaveClose;
        public bool IsEnableSaveClose
        {
            get { return _IsEnableSaveClose; }
            set { _IsEnableSaveClose = value; OnPropertyChanged(() => IsEnableSaveClose); }
        }


        private bool _IsEnableSave;
        public bool IsEnableSave
        {
            get { return _IsEnableSave; }
            set { _IsEnableSave = value; OnPropertyChanged(() => IsEnableSave); }
        }

        private ObservableCollection<EntityModel.Country> _CountryColl;
        public ObservableCollection<EntityModel.Country> CountryColl
        {
            get { return _CountryColl; }
            set { _CountryColl = value; OnPropertyChanged(() => CountryColl); }
        }

        private ObservableCollection<EntityModel.State> _StateColl;
        public ObservableCollection<EntityModel.State> StateColl
        {
            get => _StateColl;
            set { _StateColl = value; OnPropertyChanged(() => StateColl); }
        }
        private List<EntityModel.City> _Cities;
        public List<EntityModel.City> Cities
        {
            get { return _Cities; }
            set { _Cities = value; OnPropertyChanged(() => Cities); }
        }

        private ObservableCollection<EntityModel.Sequence> _SequenceColl;
        public ObservableCollection<EntityModel.Sequence> SequenceColl
        {
            get { return _SequenceColl; }
            set { _SequenceColl = value; OnPropertyChanged(() => SequenceColl); }
        }

        private CustomerModel _objCustomer;
        public CustomerModel objCustomer
        {
            get { return _objCustomer; }
            set { _objCustomer = value; OnPropertyChanged(() => objCustomer); }
        }

        private EntityModel.Sequence _SelectedSequence;
        public EntityModel.Sequence SelectedSequence
        {
            get { return _SelectedSequence; }
            set { _SelectedSequence = value; OnPropertyChanged(() => SelectedSequence); }
        }

        private Visibility _VisibleCustCode;
        public Visibility VisibleCustCode
        {
            get { return _VisibleCustCode; }
            set { _VisibleCustCode = value; OnPropertyChanged(() => VisibleCustCode); }
        }

        #endregion
    }
}
