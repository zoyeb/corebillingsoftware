﻿using BillingSoftware.Command;
using BillingSoftware.Manager;
using BillingSoftware.Model;
using BillingSoftware.UI.Masters.State;
using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BillingSoftware.ViewModel.Masters.State
{
    public class StateMasterViewModel : NotifyPropertyChangedBase
    {

        #region Declartion
        private DelegateCommand<object> _SaveCommand;
        public DelegateCommand<object> SaveCommand
        {
            get { return _SaveCommand; }
        }
        private DelegateCommand<object> _EditCommand;
        public DelegateCommand<object> EditCommand
        {
            get { return _EditCommand; }
        }
        private DelegateCommand<object> _CloseButtonCommand;
        public DelegateCommand<object> CloseButtonCommand
        {
            get { return _CloseButtonCommand; }
        }
        private DelegateCommand<object> saveAndCloseCommand;
        public DelegateCommand<object> SaveAndCloseCommand
        {
            get { return saveAndCloseCommand; }
        }
        private DelegateCommand<object> _SaveAndNewCommand;
        public DelegateCommand<object> SaveAndNewCommand
        {
            get { return _SaveAndNewCommand; }
        }

        #endregion

        #region Command

        #endregion

        #region Constructor
        public StateMasterViewModel()
        {
            Init();
            DisableEditMode();
        }

        public StateMasterViewModel(int stateId, bool EditMode)
        {
            this.IsEditMode = EditMode;
            Init();
            objState = StateMasterManager.GetStateById(stateId);
            OldStateName = objState.StateName;
            EnableEditMode();
        }

        #endregion

        #region Methods
        private void OnSaveAndNewCommand()
        {
            SaveAndNew = true;
            OnSaveData();
            Init();
            if (SaveResult)
            {
                IsEditMode = false;
                IsEnableEdit = false;
                IsEnableSave = true;
                IsEnableSaveClose = true;
                IsEnableSaveNew = true;
                SaveResult = false;
                Global.Instance.IsModified = false;
            }
            else
            {
                SaveAndNew = false;
            }
        }
        private void Init()
        {
            objState = new StateModel { CreatedBy = Global.Instance.LoginUserName, CreatedDate = DateTime.Now, Status = 1, CompanyId = Global.Instance.CompanyId };
            _SaveCommand = new DelegateCommand<object>((a) => { OnSaveData(); });
            _EditCommand = new DelegateCommand<object>((obj) => { OnEditCommand(); });
            _CloseButtonCommand = new DelegateCommand<object>((obj) => { OnClose(obj); });
            _SaveAndNewCommand = new DelegateCommand<object>((obj) => OnSaveAndNewCommand());
            saveAndCloseCommand = new DelegateCommand<object>((obj) => { OnsaveAndCloseCommand(obj); });
            SetDefaultValues();
        }
        private void OnsaveAndCloseCommand(object obj)
        {
            OnSaveData();
            if (SaveResult)
            {
                ((StateView)obj).Close();
            }
            Global.Instance.IsModified = false;
        }
        private void OnClose(object obj)
        {
            if (obj != null)
            {
                if (Global.Instance.IsModified)
                {
                    MessageBoxResult result = MessageBox.Show(GeneralMessages.ChangesHaveMade, "", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.OK);

                    if (result == MessageBoxResult.Yes)
                    {
                        OnSaveData();
                        ((StateView)obj).Close();
                    }
                    else
                    {
                        ((StateView)obj).Close();
                    }
                }
                else
                {
                    MessageBoxResult result = MessageBox.Show(GeneralMessages.CloseWindow, "", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.OK);
                    if (result == MessageBoxResult.Yes)
                    {
                        ((StateView)obj).Close();
                    }

                }
            }
        }
        private bool ValidateBeforeSave()
        {
            objState.Validate();
            OnPropertyChanged(() => objState);
            int cnt = 1;
            StringBuilder messasge = new StringBuilder();

            foreach (var item in objState._errors)
            {
                foreach (var i in item.Value)
                {
                    messasge.Append($"{cnt}. " + i + "\n");
                    cnt++;
                }

            }
            if (objState._errors.Count > 0)
            {
                MessageBox.Show(messasge.ToString());
                return false;
            }
            bool IsDuplicate = StateMasterManager.CheckDuplicate(objState.StateName, OldStateName);
            if (IsDuplicate)
            {
                messasge.Append($"{cnt}. " + MasterMessages.StateNameRequired + "\n");
                MessageBox.Show(messasge.ToString());
                return false;
            }
            return true;
        }

        private void OnSaveData()
        {
            if (ValidateBeforeSave())
            {
                var message = new StringBuilder();
                var TempState = new EntityModel.State();
                bool isSave = StateMasterManager.SaveData(objState, ref message, out TempState);

                if (isSave)
                {
                    SaveResult = true;
                    if (!SaveAndNew)
                    {
                        objState = new StateModel();
                        objState = StateMasterManager.GetStateById(TempState.StateId);
                        OldStateName = objState.StateName;
                        EnableEditMode();
                        MessageBox.Show(message.ToString());
                    }
                }
            }
        }
        private void SetDefaultValues()
        {
            CountryColl = StateMasterManager.GetCountires();
        }

        private void OnEditCommand()
        {
            IsReadOnly = false;
            IsEditMode = true;
            IsEnableEdit = false;
            IsEnableSave = true;
            IsEnableSaveClose = true;
            IsEnableSaveNew = true;
        }
        private void DisableEditMode()
        {

            IsEditMode = false;
            IsEnableEdit = false;
            IsEnableSave = true;
            IsEnableSaveClose = true;
            IsEnableSaveNew = true;
        }
        private void EnableEditMode()
        {
            IsReadOnly = true;
            IsEditMode = true;
            IsEnableEdit = true;
            IsEnableSave = false;
            IsEnableSaveClose = false;
            IsEnableSaveNew = false;
            Global.Instance.IsModified = false;
        }
        #endregion

        #region Properties
        private bool _IsReadOnly;
        public bool IsReadOnly
        {
            get { return _IsReadOnly; }
            set { _IsReadOnly = value; OnPropertyChanged(() => IsReadOnly); }
        }

        private bool _SaveResult;
        public bool SaveResult
        {
            get { return _SaveResult; }
            set { _SaveResult = value; OnPropertyChanged(() => SaveResult); }
        }
        private bool _SaveAndNew;
        public bool SaveAndNew
        {
            get { return _SaveAndNew; }
            set { _SaveAndNew = value; OnPropertyChanged(() => SaveAndNew); }
        }

        private string _OldStateName;
        public string OldStateName
        {
            get { return _OldStateName; }
            set { _OldStateName = value; OnPropertyChanged(() => OldStateName); }
        }

        private bool _IsEditMode;
        public bool IsEditMode
        {
            get { return _IsEditMode; }
            set { _IsEditMode = value; OnPropertyChanged(() => IsEditMode); }
        }

        private bool _IsEnableEdit;
        public bool IsEnableEdit
        {
            get { return _IsEnableEdit; }
            set { _IsEnableEdit = value; OnPropertyChanged(() => IsEnableEdit); }
        }

        private bool _IsEnableSaveNew;
        public bool IsEnableSaveNew
        {
            get { return _IsEnableSaveNew; }
            set { _IsEnableSaveNew = value; OnPropertyChanged(() => IsEnableSaveNew); }
        }

        private bool _IsEnableSaveClose;
        public bool IsEnableSaveClose
        {
            get { return _IsEnableSaveClose; }
            set { _IsEnableSaveClose = value; OnPropertyChanged(() => IsEnableSaveClose); }
        }


        private bool _IsEnableSave;
        public bool IsEnableSave
        {
            get { return _IsEnableSave; }
            set { _IsEnableSave = value; OnPropertyChanged(() => IsEnableSave); }
        }


        private StateModel _objState;
        public StateModel objState
        {
            get { return _objState; }
            set { _objState = value; OnPropertyChanged(() => objState); }
        }

        private ObservableCollection<EntityModel.Country> _CountryColl;
        public ObservableCollection<EntityModel.Country> CountryColl
        {
            get { return _CountryColl; }
            set { _CountryColl = value; OnPropertyChanged(() => CountryColl); }
        }
        private string _CountryId;
        public string CountryId
        {
            get { return _CountryId; }
            set { _CountryId = value; OnPropertyChanged(() => CountryId); }
        }


        #endregion

    }
}
