﻿using System;
using System.Collections.Generic;
using System.Windows;
using BillingSoftware.Command;
using BillingSoftware.HelperFunction;
using BillingSoftware.Manager;
using BillingSoftware.Model;
using BillingSoftware.UI.Masters.State;
using ResourceFiles.Properties;

namespace BillingSoftware.ViewModel.Masters.State
{
    public class StateCollectionViewModel : ViewModelBase
    {
        #region Declaration

        public DelegateCommand<object> NewButtonCommand { get; }
        public DelegateCommand<object> StateSelectionChanged { get; }
        public DelegateCommand<object> DeleteButtonCommand { get; }
        public DelegateCommand<object> CloseButtonCommand { get; }

        #endregion

        #region Constructor
        public StateCollectionViewModel()
        {
            NewButtonCommand = new DelegateCommand<object>(e => { OnAddNewStateDetails(); }, e => { return true; });
            Predicate<object> CanEdit = delegate (object obj)
            {
                if (((EntityModel.State)obj) != null) { return true; }

                return false;
            };            
            States = StateMasterManager.GetStates();
            StateSelectionChanged = new DelegateCommand<object>(OnStateSelectionChanged, CanEdit);
            DeleteButtonCommand = new DelegateCommand<object>(OnDeleteButtonCommand, CanEdit);
            CloseButtonCommand = new DelegateCommand<object>(OncEnclosebuttoncommand);
        }

        #endregion

        #region Methods
        private void OnDeleteButtonCommand(object obj)
        {
            if (!(obj is EntityModel.State)) return;
            EntityModel.State _obj;
            _obj = (EntityModel.State) obj;
            MessageBoxResult result = MessageBox.Show(MasterMessages.DeleteMessage, MasterMessages.RecordDeleted, MessageBoxButton.YesNoCancel, MessageBoxImage.Question, MessageBoxResult.Yes, MessageBoxOptions.None);
            if (result == MessageBoxResult.Yes)
            {
                if (_obj.StateId > 0)
                {
                    bool isDeleted = StateMasterManager.DeleteRecord(_obj);
                    if (isDeleted)
                    {
                        RefreshGridOnClosed(null, null);
                    }

                }
            }
        }

        private void OnStateSelectionChanged(object obj)
        {
            if (obj != null)
            {
                var _obj = obj as EntityModel.State;

                var view = new StateView();
                if (_obj != null)
                {
                    var viewModel = new StateMasterViewModel(_obj.StateId, EditMode: true);
                    view.DataContext = viewModel;
                }

                view.Closed += RefreshGridOnClosed;
                view.Show();
            }
        }
        private static void OncEnclosebuttoncommand(object obj)
        {
            MessageBoxResult result = MessageBox.Show(GeneralMessages.CloseWindow, "City Collection", MessageBoxButton.YesNo, MessageBoxImage.Asterisk);
            if (result == MessageBoxResult.Yes)
            {
                ((StateCollectionView)obj).Close();
            }
        }
        private void OnAddNewStateDetails()
        {
            if (Helpers.IsWindowOpen<StateView>() == false)
            {
                var view = new StateView();
                var viewModel = new StateMasterViewModel();
                view.DataContext = viewModel;
                view.Closed += RefreshGridOnClosed;
                view.Show();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        private void RefreshGridOnClosed(object sender, EventArgs e)
        {
            States = StateMasterManager.GetStates();
            OnPropertyChanged(nameof(States));
        }
        #endregion

        #region properties

        private List<EntityModel.State> _states;
        public List<EntityModel.State> States
        {
            get => _states;
            set { _states = value; OnPropertyChanged(nameof(States)); }
        }

        #endregion
    }
}
