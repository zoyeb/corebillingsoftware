﻿using BillingSoftware.Command;
using BillingSoftware.HelperFunction;
using BillingSoftware.Manager;
using BillingSoftware.Model;
using BillingSoftware.UI.Masters.Tax;
using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;


namespace BillingSoftware.ViewModel.Masters.TaxRateMaster
{
    public class TaxRateMasterViewModel : NotifyPropertyChangedBase
    {
        #region Declartion

        public DelegateCommand<object> SaveCommand { get; private set; }
        public DelegateCommand<object> EditCommand { get; private set; }
        public DelegateCommand<object> SaveAndNewCommand { get; private set; }
        public DelegateCommand<object> CloseButtonCommand { get; private set; }
        public DelegateCommand<object> SaveAndCloseCommand { get; private set; }
        #endregion

        #region Constructor
        public TaxRateMasterViewModel()
        {
            Init();
            DisableEditMode();
        }

        public TaxRateMasterViewModel(int taxRateId, bool EditMode)
        {
            Init();
            EnableEditMode();
            GetTaxRate(taxRateId);
        }

        private void GetTaxRate(int taxRateId)
        {
           objTaxRate = TaxRateMasterManager.GetTaxRate(taxRateId);
           OldRate = objTaxRate.TaxRate;
            if (objTaxRate.Active == null)
                objTaxRate.Active = false;


        }
        #endregion


        #region Methods
        private void DisableEditMode()
        {
            IsReadOnly = false;
            IsEnableSave = IsEnableSaveClose = IsEnableSaveNew = true;
            IsEnableEdit = false;
        }
        private void OnEditCommand()
        {
            IsReadOnly = false;
            IsEnableSave = IsEnableSaveClose = IsEnableSaveNew = true;
            IsEnableEdit = false;
        }
        private void EnableEditMode()
        {
            IsReadOnly = true;
            IsEnableSave = IsEnableSaveClose = IsEnableSaveNew = false;
            IsEnableEdit = true;
            Global.Instance.IsModified = false;
        }
        private void Init()
        {
            objTaxRate = new TaxRateMasterModel { CreatedBy = Global.Instance.LoginUserName, CompanyId = Global.Instance.CompanyId , Active=true , CreatedDate=DateTime.Now ,Status=1 };
             
            EditCommand = new DelegateCommand<object>((s) => { OnEditCommand(); }, (s) => { return true; });
            SaveCommand = new DelegateCommand<object>((a) => { OnSaveData(); });
            SaveAndNewCommand = new DelegateCommand<object>((a) => { OnSaveAndNewCommand(); });
            CloseButtonCommand = new DelegateCommand<object>((obj) => { OnClose(obj); });
            SaveAndCloseCommand = new DelegateCommand<object>((obj) => { OnsaveAndCloseCommand(obj); });
             
        }

        private void OnsaveAndCloseCommand(object obj)
        {
            OnSaveData();
            if (SaveResult)
            {
                ((BillingSoftware.UI.Masters.TaxRateMaster.TaxRateMasterView)obj).Close();
            }
            Global.Instance.IsModified = false;             
        }

        private void OnClose(object obj)
        {
            if (obj != null)
            {
                if (Global.Instance.IsModified)
                {
                    MessageBoxResult result = MessageBox.Show(GeneralMessages.ChangesHaveMade, "", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.OK);

                    if (result == MessageBoxResult.Yes)
                    {
                        OnSaveData();
                        ((UI.Masters.TaxRateMaster.TaxRateMasterView)obj).Close();
                    }
                    else
                    {
                        ((UI.Masters.TaxRateMaster.TaxRateMasterView)obj).Close();
                    }
                }
                else
                {
                    MessageBoxResult result = MessageBox.Show(GeneralMessages.CloseWindow, "", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.OK);
                    if (result == MessageBoxResult.Yes)
                    {
                        ((BillingSoftware.UI.Masters.TaxRateMaster.TaxRateMasterView)obj).Close();
                    }
                }
            }
        }

        private void OnSaveAndNewCommand()
        {
            SaveAndNew = true;
            OnSaveData();
            Init();
            if (SaveResult)
            {
                IsEditMode = false;
                IsEnableEdit = false;
                IsEnableSave = true;
                IsEnableSaveClose = true;
                IsEnableSaveNew = true;
                SaveResult = false;
            }
        }

        private void OnSaveData()
        {
            if (ValidateBeforeSave())
            {
                StringBuilder msg = new StringBuilder();
                var temp = new EntityModel.TaxRateMaster();

                bool res = TaxRateMasterManager.SaveData(objTaxRate,ref msg, out temp);
                if (res)
                {
                    SaveResult = true;
                    if (!SaveAndNew)
                    {
                        objTaxRate = new TaxRateMasterModel();
                        objTaxRate = TaxRateMasterManager.GetTaxRate(temp.TaxRateId);
                        OldRate = objTaxRate.TaxRate;
                        EnableEditMode();
                    }
                }
                MessageBox.Show(msg.ToString());
            }
             
        }

        private bool ValidateBeforeSave()
        {
            objTaxRate.Validate();
            int cnt = 1;
            StringBuilder message = new StringBuilder();

            foreach (var item in objTaxRate._errors)
            {
                message.Append($"{cnt}. " + item + "\n");
                cnt++;
            }
            if (objTaxRate._errors.Count > 0)
            {
                MessageBox.Show(message.ToString());
                return false;
            }

            bool isDuplicate = TaxRateMasterManager.CheckDuplicate(objTaxRate.TaxRate, OldRate);
            if (isDuplicate)
            {
                message.Append($"{cnt}. " + MasterMessages.TaxRateIsExists + "\n");
                MessageBox.Show(message.ToString());
                return false;
            }
            return true;
            
        }
        #endregion

        #region Properties
        private TaxRateMasterModel _objTaxRate;

        public TaxRateMasterModel objTaxRate
        {
            get { return _objTaxRate; }
            set { _objTaxRate = value; OnPropertyChanged(() => objTaxRate); }
        }


        private bool _isReadOnly;
        public bool IsReadOnly
        {
            get => _isReadOnly;
            set { _isReadOnly = value; OnPropertyChanged(() => IsReadOnly); }
        }

        private bool _saveResult;
        public bool SaveResult
        {
            get => _saveResult;
            set { _saveResult = value; OnPropertyChanged(() => SaveResult); }
        }
        private bool _saveAndNew;
        public bool SaveAndNew
        {
            get { return _saveAndNew; }
            set { _saveAndNew = value; OnPropertyChanged(() => SaveAndNew); }
        }

        private double? _oldRate;
        public double? OldRate
        {
            get { return _oldRate; }
            set { _oldRate = value; OnPropertyChanged(() => OldRate); }
        }

        private bool _isEditMode;
        public bool IsEditMode
        {
            get { return _isEditMode; }
            set { _isEditMode = value; OnPropertyChanged(() => IsEditMode); }
        }

        private bool _isEnableEdit;
        public bool IsEnableEdit
        {
            get => _isEnableEdit;
            set { _isEnableEdit = value; OnPropertyChanged(() => IsEnableEdit); }
        }

        private bool _isEnableSaveNew;
        public bool IsEnableSaveNew
        {
            get => _isEnableSaveNew;
            set { _isEnableSaveNew = value; OnPropertyChanged(() => IsEnableSaveNew); }
        }

        private bool _isEnableSaveClose;
        public bool IsEnableSaveClose
        {
            get => _isEnableSaveClose;
            set { _isEnableSaveClose = value; OnPropertyChanged(() => IsEnableSaveClose); }
        }


        private bool _isEnableSave;
        public bool IsEnableSave
        {
            get => _isEnableSave;
            set { _isEnableSave = value; OnPropertyChanged(() => IsEnableSave); }
        }
 


        
        private ObservableCollection<EntityModel.State> _StateColl;
        public ObservableCollection<EntityModel.State> StateColl
        {
            get => _StateColl;
            set { _StateColl = value; OnPropertyChanged(() => StateColl); }
        }

        private ObservableCollection<EntityModel.Country> _countryColl;
        public ObservableCollection<EntityModel.Country> CountryColl
        {
            get => _countryColl;
            set { _countryColl = value; OnPropertyChanged(() => CountryColl); }
        }

        private string _countryId;
        public string CountryId
        {
            get { return _countryId; }
            set { _countryId = value; OnPropertyChanged(() => CountryId); }
        }


        #endregion
    }
}
