﻿using BillingSoftware.Command;
using BillingSoftware.HelperFunction;
using BillingSoftware.Manager;
using BillingSoftware.Model;
using BillingSoftware.UI.Masters.Tax;
using BillingSoftware.UI.Masters.TaxRateMaster;
using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BillingSoftware.ViewModel.Masters.TaxRateMaster
{
    public class TaxRateMasterCollectionViewModel : NotifyPropertyChangedBase
    {
        #region Declaration
        private readonly DelegateCommand<object> newButtonCommand;
        public DelegateCommand<object> NewButtonCommand
        {
            get { return newButtonCommand; }
        }

        private DelegateCommand<object> taxRateSelectionChanged;
        public DelegateCommand<object> TaxRateSelectionChanged
        {
            get { return taxRateSelectionChanged; }
        }

        private DelegateCommand<object> deleteButtonCommand;
        public DelegateCommand<object> DeleteButtonCommand
        {
            get { return deleteButtonCommand; }
        }
        private DelegateCommand<object> closeButtonCommand;
        public DelegateCommand<object> CloseButtonCommand
        {
            get { return closeButtonCommand; }
        }
        #endregion

        public TaxRateMasterCollectionViewModel()
        {
            newButtonCommand = new DelegateCommand<object>((e) => { OnAddTaxRate(); }, (e) => { return true; });
            Predicate<object> predicate = new Predicate<object>(delegate (object obj) { if (((EntityModel.TaxRateMaster)obj) != null) { return true; } else { return false; } });
            taxRateSelectionChanged = new DelegateCommand<object>((obj) => OnTaxRateSelectionChanged(obj), predicate);
            deleteButtonCommand = new DelegateCommand<object>((obj) => OnDeleteButtonCommand(obj), predicate);
            closeButtonCommand = new DelegateCommand<object>((obj) => { OncloseButtonCommand(obj); });
            SetDefaultValues();
        }

        private void OnTaxRateSelectionChanged(object obj)
        {
            if (obj != null)
            {
                if (obj is EntityModel.TaxRateMaster)
                {
                    var _obj = obj as EntityModel.TaxRateMaster;
                    var view = new TaxRateMasterView();
                    var viewModel = new TaxRateMasterViewModel(_obj.TaxRateId, EditMode: true);
                    view.DataContext = viewModel;
                    view.Closed += RefreshGridOnClosed;
                    view.Show();
                }
            }
        }

        private void OnAddTaxRate()
        {
            if (Helpers.IsWindowOpen<TaxRateMasterView>() == false)
            {
                var view = new TaxRateMasterView();
                var viewModel = new TaxRateMasterViewModel();
                view.DataContext = viewModel;
                view.Closed += RefreshGridOnClosed;
                view.Show();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }
        private void OncloseButtonCommand(object obj)
        {
            MessageBoxResult result = MessageBox.Show(GeneralMessages.CloseWindow, "Tax RateMaster", MessageBoxButton.YesNo, MessageBoxImage.Asterisk);
            if (result == MessageBoxResult.Yes)
            {
                ((TaxRateMasterCollectionView)obj).Close();
            }
        }
        private void OnDeleteButtonCommand(object obj)
        {
            //if (obj is EntityModel.City)
            //{
            //    var _obj = obj as EntityModel.City;
            //    MessageBoxResult result = MessageBox.Show(MasterMessages.DeleteMessage, MasterMessages.RecordDeleted, MessageBoxButton.YesNoCancel, MessageBoxImage.Question, MessageBoxResult.Yes, MessageBoxOptions.None);
            //    if (result == MessageBoxResult.Yes)
            //    {
            //        try
            //        {
            //            if (_obj.CityId > 0)
            //            {
            //                bool IsDeleted = CityMasterManager.DeleteRecord(_obj);
            //                if (IsDeleted)
            //                {
            //                    RefreshGridOnClosed(null, null);
            //                }

            //            }
            //        }
            //        catch (Exception)
            //        {

            //            throw;
            //        }
            //    }
            //}
        }
        private void SetDefaultValues()
        {
            TaxRates = TaxRateMasterManager.GetTaxRates();

        }
        private void RefreshGridOnClosed(object sender, EventArgs e)
        {
            TaxRates = TaxRateMasterManager.GetTaxRates();
            this.OnPropertyChanged(() => TaxRates);
        }

        #region properties

        private ObservableCollection<EntityModel.TaxRateMaster> _TaxRates;
        public ObservableCollection<EntityModel.TaxRateMaster> TaxRates
        {
            get { return _TaxRates; }
            set { _TaxRates = value; this.OnPropertyChanged(() => TaxRates); }
        }

        #endregion
    }
}
