﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using BillingSoftware.Command;
using BillingSoftware.HelperFunction;
using BillingSoftware.Manager;
using BillingSoftware.Model;
using BillingSoftware.UI.Masters.Country;
using BillingSoftware.UI.Masters.Product;
using ResourceFiles.Properties;

namespace BillingSoftware.ViewModel.Masters.Product
{
    class ProductCollectionViewModel : ViewModelBase
    {
        #region Command
        private readonly DelegateCommand<object> newButtonCommand;
        public DelegateCommand<object> NewButtonCommand
        {
            get { return newButtonCommand; }
        }

        private DelegateCommand<object> closeButtonCommand;
        public DelegateCommand<object> CloseButtonCommand
        {
            get { return closeButtonCommand; }
        }

        private DelegateCommand<object> selectedProduct;
        public DelegateCommand<object> SelectedProduct
        {
            get { return selectedProduct; }
        }
        private DelegateCommand<object> deleteButtonCommand;
        public DelegateCommand<object> DeleteButtonCommand
        {
            get { return deleteButtonCommand; }
        }
        #endregion Command

        #region Constructor
        public ProductCollectionViewModel()
        {
            Products = ProductManager.GetProductCollection();
            newButtonCommand = new DelegateCommand<object>((e) => { OnAddNewCountryDetails(); }, (e) => { return true; });
            closeButtonCommand = new DelegateCommand<object>((obj) => { OncloseButtonCommand(obj); });
            Predicate<object> CanSelectedProduct = delegate (object s) { if (((EntityModel.Product)s) != null) { return true; } else { return false; } };
            selectedProduct = new DelegateCommand<object>((obj) => { OnselectedProducts(obj); }, CanSelectedProduct);
            deleteButtonCommand = new DelegateCommand<object>((obj) => OnDeleteButtonCommand(obj), CanSelectedProduct);
        }


        #endregion Constructor

        #region Functions

        private void OnDeleteButtonCommand(object obj)
        {
            if (obj is EntityModel.Product)
            {
                var _obj = obj as EntityModel.Product;
                MessageBoxResult result = MessageBox.Show(MasterMessages.DeleteMessage, MasterMessages.RecordDeleted, MessageBoxButton.YesNoCancel, MessageBoxImage.Question, MessageBoxResult.Yes, MessageBoxOptions.None);
                if (result == MessageBoxResult.Yes)
                {
                    try
                    {
                        if (_obj.ProductId > 0)
                        {
                            bool IsDeleted = ProductManager.DeleteRecord(_obj);
                            if (IsDeleted)
                            {
                                RefreshGridOnClosed(null, null);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }

                }
            }
        }

        private void OnselectedProducts(object obj)
        {
            if (!Helpers.IsWindowOpen<ProductView>())
            {
                if (obj != null)
                {
                    var _obj = obj as EntityModel.Product;
                    var view = new ProductView();
                    var viewModel = new ProductViewModel(_obj.ProductId, isEditMode: true);
                    view.DataContext = viewModel;
                    view.Closed += RefreshGridOnClosed; ;
                    view.Show();
                }
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        private void OncloseButtonCommand(object obj)
        {
            MessageBoxResult result = MessageBox.Show(GeneralMessages.CloseWindow, "Product Collection View", MessageBoxButton.YesNo, MessageBoxImage.Asterisk);
            if (result == MessageBoxResult.Yes)
            {
                ((ProductCollectionView)obj).Close();
            }
        }
        private void OnAddNewCountryDetails()
        {
            if (!Helpers.IsWindowOpen<CountryView>())
            {
                var view = new ProductView();
                var viewModel = new ProductViewModel();
                view.DataContext = viewModel;
                view.Closed += RefreshGridOnClosed; ;
                view.Show();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        private void RefreshGridOnClosed(object sender, EventArgs e)
        {
            Products = ProductManager.GetProductCollection();
            OnPropertyChanged(nameof(Products));
        }

        #endregion Constructor

        #region Properties
        private ObservableCollection<EntityModel.Product> products;
        public ObservableCollection<EntityModel.Product> Products
        {
            get { return products; }
            set { products = value; OnErrorsChanged(nameof(Products)); }
        }

        #endregion Properties
    }
}
