﻿using BillingSoftware.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using BillingSoftware.EntityModel;
using BillingSoftware.Manager;
using BillingSoftware.Model;
using BillingSoftware.UI.Masters;
using BillingSoftware.UI.Masters.Product;
using BillingSoftware.UI.Masters.ProductCategory;
using ResourceFiles.Properties;
 

namespace BillingSoftware.ViewModel.Masters.Product
{
    class ProductViewModel : NotifyPropertyChangedBase
    {
        #region Declaration 


        private DelegateCommand<object> saveAndCloseCommand;
        public DelegateCommand<object> SaveAndCloseCommand
        {
            get { return saveAndCloseCommand; }
        }

        private DelegateCommand<object> _SaveCommand;
        public DelegateCommand<object> SaveCommand
        {
            get { return _SaveCommand; }
        }

        private DelegateCommand<object> _SaveAndNewCommand;
        public DelegateCommand<object> SaveAndNewCommand
        {
            get { return _SaveAndNewCommand; }
        }
        private DelegateCommand<object> _EditCommand;
        public DelegateCommand<object> EditCommand
        {
            get { return _EditCommand; }
        }

        private DelegateCommand<object> _CloseButtonCommand;
        public DelegateCommand<object> CloseButtonCommand
        {
            get { return _CloseButtonCommand; }
        }

        #endregion

        #region Constructor
        public ProductViewModel(int productId, bool isEditMode)
        {
            this.IsEditMode = isEditMode;
            Init(editMode: IsEditMode);
            objProduct = ProductManager.GetProductById(productId);
            OldProductName = objProduct.ProductName;
            EnableEditMode();
        }
        public ProductViewModel()
        {
            Init();
            DisableEditMode();
            Global.Instance.IsModified = false;
        }
        #endregion Constructor

        #region Functions
        private void DisableEditMode()
        {

            IsEditMode = false;
            IsEnableEdit = false;
            IsEnableSave = true;
            IsEnableSaveClose = true;
            IsEnableSaveNew = true;
        }
        private void EnableEditMode()
        {
            IsReadOnly = true;
            IsEditMode = true;
            IsEnableEdit = true;
            IsEnableSave = false;
            IsEnableSaveClose = false;
            IsEnableSaveNew = false;
            Global.Instance.IsModified = false;
        }

        private void Init(bool editMode = false)
        {
            objProduct = new ProductModel { CreatedBy = Global.Instance.LoginUserName, CreatedDate = DateTime.Now, Status = 1, CompanyId = Global.Instance.CompanyId ,IsGstApplicable = false};
            SetDefaultValue();
            _EditCommand = new DelegateCommand<object>((s) => { OnEditCommand(); }, (s) => { return true; });
            _SaveCommand = new DelegateCommand<object>((a) => { OnSaveData(); });
            _SaveAndNewCommand = new DelegateCommand<object>((obj) => OnSaveAndNewCommand());
            _CloseButtonCommand = new DelegateCommand<object>((obj) => { OnClose(obj); });
            saveAndCloseCommand = new DelegateCommand<object>((obj) => { OnsaveAndCloseCommand(obj); });
        }
        private void OnsaveAndCloseCommand(object obj)
        {
            OnSaveData();
            if (SaveResult)
            {
                ((ProductView)obj).Close();
            }
            Global.Instance.IsModified = false;
        }

        private void SetDefaultValue()
        {
            ProductTypesColl = ProductManager.GetProductCategoires((int) Global.Instance.CompanyId);             
        }
        private void OnClose(object obj)
        {
            if (obj != null)
            {

                if (Global.Instance.IsModified)
                {
                    MessageBoxResult result = MessageBox.Show(GeneralMessages.ChangesHaveMade, "", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.OK);

                    if (result == MessageBoxResult.Yes)
                    {
                        OnSaveData();
                        ((ProductView)obj).Close();
                    }
                    else
                    {
                        ((ProductView)obj).Close();
                    }
                }
                else
                {
                    MessageBoxResult result = MessageBox.Show(GeneralMessages.CloseWindow, "", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.OK);
                    if (result == MessageBoxResult.Yes)
                    {
                        ((ProductView)obj).Close();
                    }

                }
            }
        }

        private void OnSaveAndNewCommand()
        {
            SaveAndNew = true;
            OnSaveData();
            Init();
            if (SaveResult)
            {
                IsEditMode = false;
                IsEnableEdit = false;
                IsEnableSave = true;
                IsEnableSaveClose = true;
                IsEnableSaveNew = true;
                SaveResult = false;
                Global.Instance.IsModified = false;
            }
            else
            {
                SaveAndNew = false;
            }
        }


        private void OnSaveData()
        {
            if (ValidateBeforeSave())
            {
                var product = new EntityModel.Product();
                var message = new StringBuilder();
                bool isSaved = ProductManager.SaveData(objProduct, out product, ref message);
                if (isSaved)
                {
                    SaveResult = true;
                    if (!SaveAndNew)
                    {
                        objProduct = new ProductModel
                        {
                            IsGstApplicable = product.IsGstApplicable,
                            ProductName = product.ProductName,
                            ProductId = product.ProductId,
                            ProductionTypeId = product.ProductionTypeId,                           
                            ProductColor = product.ProductColor,
                            ProductSize = product.ProductSize,
                            ProductDetails = product.ProductDetails
                        };
                        EnableEditMode();
                    }
                    MessageBox.Show(message.ToString());
                }
            }

        }

        private bool ValidateBeforeSave()
        {
            objProduct.Validate();
            OnPropertyChanged(() => objProduct);
            int cnt = 1;
            StringBuilder messasge = new StringBuilder();

            foreach (var item in objProduct._errors)
            {
                foreach (var i in item.Value)
                {
                    messasge.Append($"{cnt}. " + i + "\n");
                    cnt++;
                }

            }
            if (objProduct._errors.Count > 0)
            {
                MessageBox.Show(messasge.ToString());
                return false;
            }
            bool isDuplicate = ProductManager.CheckDuplicate(objProduct.ProductName, OldProductName);
            if (isDuplicate)
            {
                messasge.Append($"{cnt}. " + "Product Name is Already Exists.." + "\n");
                MessageBox.Show(messasge.ToString());
                return false;
            }
            return true;
        }


        #endregion Functions

        #region Command

        private void OnEditCommand()
        {
            IsReadOnly = false;
            IsEditMode = true;
            IsEnableEdit = false;
            IsEnableSave = true;
            IsEnableSaveClose = true;
            IsEnableSaveNew = true;
        }


        #endregion Command  

        #region Properties

        private ObservableCollection<ProductType> _productTypesColl;

        public ObservableCollection<ProductType> ProductTypesColl
        {
            get { return _productTypesColl; }
            set { _productTypesColl = value; OnPropertyChanged(() => ProductTypesColl); }
        }


        private bool _IsReadOnly;
        public bool IsReadOnly
        {
            get { return _IsReadOnly; }
            set { _IsReadOnly = value; OnPropertyChanged(() => IsReadOnly); }
        }

        private bool _SaveResult;
        public bool SaveResult
        {
            get { return _SaveResult; }
            set { _SaveResult = value; OnPropertyChanged(() => SaveResult); }
        }
        private bool _SaveAndNew;
        public bool SaveAndNew
        {
            get { return _SaveAndNew; }
            set { _SaveAndNew = value; OnPropertyChanged(() => SaveAndNew); }
        }

        private string _OldProductName;
        public string OldProductName
        {
            get { return _OldProductName; }
            set { _OldProductName = value; OnPropertyChanged(() => _OldProductName); }
        }

        private bool _IsEditMode;
        public bool IsEditMode
        {
            get { return _IsEditMode; }
            set { _IsEditMode = value; OnPropertyChanged(() => IsEditMode); }
        }

        private bool _IsEnableEdit;
        public bool IsEnableEdit
        {
            get { return _IsEnableEdit; }
            set { _IsEnableEdit = value; OnPropertyChanged(() => IsEnableEdit); }
        }

        private bool _IsEnableSaveNew;
        public bool IsEnableSaveNew
        {
            get { return _IsEnableSaveNew; }
            set { _IsEnableSaveNew = value; OnPropertyChanged(() => IsEnableSaveNew); }
        }

        private bool _IsEnableSaveClose;
        public bool IsEnableSaveClose
        {
            get { return _IsEnableSaveClose; }
            set { _IsEnableSaveClose = value; OnPropertyChanged(() => IsEnableSaveClose); }
        }


        private bool _IsEnableSave;
        public bool IsEnableSave
        {
            get { return _IsEnableSave; }
            set { _IsEnableSave = value; OnPropertyChanged(() => IsEnableSave); }
        }


        private ProductModel _objProduct;
        public ProductModel objProduct
        {
            get { return _objProduct; }
            set { _objProduct = value; OnPropertyChanged(() => objProduct); }
        }

        #endregion


    }
}
