﻿using BillingSoftware.Command;
using BillingSoftware.HelperFunction;
using BillingSoftware.UI.Masters.City;
using BillingSoftware.UI.Masters.ProductCategory;
using BillingSoftware.UI.Masters.State;
using BillingSoftware.ViewModel.Masters.City;
using BillingSoftware.ViewModel.Masters.State;
using ResourceFiles.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using BillingSoftware.ViewModel.Masters.ProductCategory;
using BillingSoftware.UI.Masters.Product;
using BillingSoftware.ViewModel.Masters.Country;
using BillingSoftware.ViewModel.Masters.Product;
using BillingSoftware.UI.Masters.Country;
using BillingSoftware.UI.Masters.Sequence;
using BillingSoftware.ViewModel.Masters.Sequence;
using BillingSoftware.UI.Masters.Customer;
using BillingSoftware.ViewModel.Masters.Customer;
using BillingSoftware.UI.Transaction.SalesOrder;
using BillingSoftware.ViewModel.Transaction.SalesOrder;
using BillingSoftware.UI.Masters.Tax;
using BillingSoftware.ViewModel.Masters.Tax;
using BillingSoftware.ViewModel.Transaction.Invoice;
using BillingSoftware.UI.Transaction.Invoice;
using BillingSoftware.UI.Masters.ProductStock;
using BillingSoftware.ViewModel.Masters.ProductStock;

namespace BillingSoftware.ViewModel
{
    public class MainWindowViewModel
    {
        #region Command Menus

        #region Collection View Command

        #endregion Collection View Command

        #region View Command




        public DelegateCommand<object> MainItemSequence { get; }
        public DelegateCommand<object> MainItemCountryColletion { get; }
        public DelegateCommand<object> MainItemStateColletion { get; }
        public DelegateCommand<object> MainItemCityCollection { get; }
        public DelegateCommand<object> MainItemProductCategoryCollection { get; }
        public DelegateCommand<object> MainItemProductCollection { get; }
        public DelegateCommand<object> MainItemCustomerCollection { get; }
        public DelegateCommand<object> MainItemOrderCollection { get; }
        public DelegateCommand<object> MainItemTaxCollection { get; }
        public DelegateCommand<object> MainItemInvoiceCollection { get; }
        public DelegateCommand<object> MainItemTaxRateCollection { get; }
        public DelegateCommand<object> MainItemItemStockCollection { get; }




        #endregion View Command

        #region Constructor
        public MainWindowViewModel()
        {
            //country 
            MainItemCountryColletion = new DelegateCommand<object>((s) => { OpenCountryCollectionView(); }, (s) => { return true; });
            //state
            MainItemStateColletion = new DelegateCommand<object>((s) => { OpenStateCollectionView(); }, (s) => true);
            //City
            MainItemCityCollection = new DelegateCommand<object>((obj) => OpenCityCollectionView(), (s) => true);
            //Product Category
            MainItemProductCategoryCollection = new DelegateCommand<object>((obj) => OnMainItemProductCategoryCollection(), (obj) => true);
            //product
            MainItemProductCollection = new DelegateCommand<object>((obj) => OnMainItemProductCollection(), (s) => true);
            //sequnece 
            MainItemSequence = new DelegateCommand<object>((obj) => OnMainItemSequence(), (s) => true);
            //Customer
            MainItemCustomerCollection = new DelegateCommand<object>((obj) => OnMainItemCustomerCollection(), (s) => true);
            //Order
            MainItemOrderCollection = new DelegateCommand<object>((obj) => OnMainItemOrderCollection(), (s) => true);
            //Tax
            MainItemTaxCollection = new DelegateCommand<object>((obj) => OnMainItemTaxCollection(), (s) => true);
            //Tax Rate
            MainItemTaxRateCollection = new DelegateCommand<object>((obj) => OnMainItemTaxRateCollection(), (s) => true);
            //Sales Invoice
            MainItemInvoiceCollection = new DelegateCommand<object>((obj) => OnMainItemInvoiceCollection(), (s) => true);
            //Item Sotck
            MainItemItemStockCollection = new DelegateCommand<object>((obj) => OnMainItemItemStockCollection(), (s) => true);
        }
         

        #endregion Constructor


        #region menus Function       
        private void OnMainItemItemStockCollection()
        {
            if (!Helpers.IsWindowOpen<ProductStockCollectionView>())
            {
                var view = new ProductStockCollectionView();
                var vm = new ProductStockCollectionViewModel();
                view.DataContext = vm;
                view.ShowDialog();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }
        private void OnMainItemInvoiceCollection()
        {
            if (!Helpers.IsWindowOpen<InvoiceCollectionView>())
            {               
                var view = new InvoiceCollectionView();
                var vm = new InvoiceCollectionViewModel();
                view.DataContext = vm;
                view.ShowDialog();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }


        private void OnMainItemTaxCollection()
        {
            if (!Helpers.IsWindowOpen<TaxCollectionView>())
            {

                var view = new TaxCollectionView();
                var vm = new TaxCollectionViewModel();
                view.DataContext = vm;
                view.ShowDialog();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }
        private void OnMainItemTaxRateCollection()
        {
            if (!Helpers.IsWindowOpen<BillingSoftware.UI.Masters.TaxRateMaster.TaxRateMasterView>())
            {
                var view = new BillingSoftware.UI.Masters.TaxRateMaster.TaxRateMasterCollectionView();
                var vm = new BillingSoftware.ViewModel.Masters.TaxRateMaster.TaxRateMasterCollectionViewModel();
                view.DataContext = vm;
                view.ShowDialog();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        private void OnMainItemOrderCollection()
        {
            if (!Helpers.IsWindowOpen<SalesOrderCollectionView>())
            {
                var view = new SalesOrderCollectionView();
                var vm = new SalesOrderCollectionViewModel();
                view.DataContext = vm;
                view.ShowDialog();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }
        private void OnMainItemCustomerCollection()
        {
            if (!Helpers.IsWindowOpen<CustomerCollectionView>())
            {
                var view = new CustomerCollectionView();
                var vm = new CustomerCollectionViewModel();
                view.DataContext = vm;
                view.ShowDialog();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        private void OnMainItemSequence()
        {
            if (!Helpers.IsWindowOpen<ProductCategoryCollectionView>())
            {
                var view = new SequenceCollectionView();
                var vm = new SequenceCollectionViewModel();
                view.DataContext = vm;
                view.ShowDialog();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        private void OnMainItemProductCategoryCollection()
        {
            if (!Helpers.IsWindowOpen<ProductCategoryCollectionView>())
            {
                var view = new ProductCategoryCollectionView();
                var vm = new ProductCategoryCollectionViewModel();
                view.DataContext = vm;
                view.ShowDialog();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }
        private void OpenCountryCollectionView()
        {

            if (!Helpers.IsWindowOpen<CountryCollectionView>())
            {
                var view = new CountryCollectionView();
                var vm = new CountryCollectionViewModel();
                view.DataContext = vm;
                view.ShowDialog();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        private void OpenStateCollectionView()
        {
            if (!Helpers.IsWindowOpen<StateCollectionView>())
            {
                var view = new StateCollectionView();
                var vm = new StateCollectionViewModel();
                view.DataContext = vm;
                view.ShowDialog();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        private void OpenCityCollectionView()
        {
            if (!Helpers.IsWindowOpen<CityCollectionView>())
            {
                var view = new CityCollectionView();
                var vm = new CityCollectionViewModel();
                view.DataContext = vm;
                view.ShowDialog();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        private void OnMainItemProductCollection()
        {
            if (!Helpers.IsWindowOpen<ProductCollectionView>())
            {
                var view = new ProductCollectionView();
                var vm = new ProductCollectionViewModel();
                view.DataContext = vm;
                view.ShowDialog();
            }
            else
            {
                MessageBox.Show(GeneralMessages.WindowIsOpened);
            }
        }

        #endregion menus Function

        #endregion Command Menus
    }
}
